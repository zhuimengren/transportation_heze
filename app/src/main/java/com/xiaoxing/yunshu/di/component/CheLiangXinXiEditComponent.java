package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.CheLiangXinXiEditModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.CheLiangXinXiEditActivity;

@ActivityScope
@Component(modules = CheLiangXinXiEditModule.class, dependencies = AppComponent.class)
public interface CheLiangXinXiEditComponent {
    void inject(CheLiangXinXiEditActivity activity);
}