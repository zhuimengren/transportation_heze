package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.OrdersContract;
import com.xiaoxing.yunshu.mvp.model.OrdersModel;


@Module
public class OrdersModule {
    private OrdersContract.View view;

    /**
     * 构建OrdersModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public OrdersModule(OrdersContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    OrdersContract.View provideOrdersView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    OrdersContract.Model provideOrdersModel(OrdersModel model) {
        return model;
    }
}