package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.YiFenPeiXiangQingContract;
import com.xiaoxing.yunshu.mvp.model.YiFenPeiXiangQingModel;


@Module
public class YiFenPeiXiangQingModule {
    private YiFenPeiXiangQingContract.View view;

    /**
     * 构建YiFenPeiXiangQingModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public YiFenPeiXiangQingModule(YiFenPeiXiangQingContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    YiFenPeiXiangQingContract.View provideYiFenPeiXiangQingView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    YiFenPeiXiangQingContract.Model provideYiFenPeiXiangQingModel(YiFenPeiXiangQingModel model) {
        return model;
    }
}