package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.QiangDanContract;
import com.xiaoxing.yunshu.mvp.model.QiangDanModel;


@Module
public class QiangDanModule {
    private QiangDanContract.View view;

    /**
     * 构建QiangDanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public QiangDanModule(QiangDanContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    QiangDanContract.View provideQiangDanView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    QiangDanContract.Model provideQiangDanModel(QiangDanModel model) {
        return model;
    }
}