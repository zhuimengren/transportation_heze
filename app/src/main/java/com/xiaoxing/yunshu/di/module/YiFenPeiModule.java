package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.YiFenPeiContract;
import com.xiaoxing.yunshu.mvp.model.YiFenPeiModel;


@Module
public class YiFenPeiModule {
    private YiFenPeiContract.View view;

    /**
     * 构建YiFenPeiModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public YiFenPeiModule(YiFenPeiContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    YiFenPeiContract.View provideYiFenPeiView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    YiFenPeiContract.Model provideYiFenPeiModel(YiFenPeiModel model) {
        return model;
    }
}