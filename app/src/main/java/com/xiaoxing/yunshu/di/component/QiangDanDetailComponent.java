package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.QiangDanDetailModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.QiangDanDetailActivity;

@ActivityScope
@Component(modules = QiangDanDetailModule.class, dependencies = AppComponent.class)
public interface QiangDanDetailComponent {
    void inject(QiangDanDetailActivity activity);
}