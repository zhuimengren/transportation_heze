package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.WoDeModule;

import com.jess.arms.di.scope.FragmentScope;
import com.xiaoxing.yunshu.mvp.ui.fragment.WoDeFragment;

@FragmentScope
@Component(modules = WoDeModule.class, dependencies = AppComponent.class)
public interface WoDeComponent {
    void inject(WoDeFragment fragment);
}