package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.WanChengContract;
import com.xiaoxing.yunshu.mvp.model.WanChengModel;


@Module
public class WanChengModule {
    private WanChengContract.View view;

    /**
     * 构建WanChengModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public WanChengModule(WanChengContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    WanChengContract.View provideWanChengView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    WanChengContract.Model provideWanChengModel(WanChengModel model) {
        return model;
    }
}