package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.QiangDanModule;

import com.jess.arms.di.scope.FragmentScope;
import com.xiaoxing.yunshu.mvp.ui.fragment.QiangDanFragment;

@FragmentScope
@Component(modules = QiangDanModule.class, dependencies = AppComponent.class)
public interface QiangDanComponent {
    void inject(QiangDanFragment fragment);
}