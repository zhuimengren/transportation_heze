package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.YunShuRuKuMainModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.YunShuRuKuMainActivity;

@ActivityScope
@Component(modules = YunShuRuKuMainModule.class, dependencies = AppComponent.class)
public interface YunShuRuKuMainComponent {
    void inject(YunShuRuKuMainActivity activity);
}