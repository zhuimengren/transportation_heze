package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiContract;
import com.xiaoxing.yunshu.mvp.model.CheLiangXinXiModel;


@Module
public class CheLiangXinXiModule {
    private CheLiangXinXiContract.View view;

    /**
     * 构建CheLiangXinXiModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public CheLiangXinXiModule(CheLiangXinXiContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    CheLiangXinXiContract.View provideCheLiangXinXiView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    CheLiangXinXiContract.Model provideCheLiangXinXiModel(CheLiangXinXiModel model) {
        return model;
    }
}