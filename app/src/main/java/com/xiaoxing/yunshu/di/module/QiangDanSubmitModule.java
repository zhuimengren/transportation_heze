package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.QiangDanSubmitContract;
import com.xiaoxing.yunshu.mvp.model.QiangDanSubmitModel;


@Module
public class QiangDanSubmitModule {
    private QiangDanSubmitContract.View view;

    /**
     * 构建QiangDanSubmitModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public QiangDanSubmitModule(QiangDanSubmitContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    QiangDanSubmitContract.View provideQiangDanSubmitView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    QiangDanSubmitContract.Model provideQiangDanSubmitModel(QiangDanSubmitModel model) {
        return model;
    }
}