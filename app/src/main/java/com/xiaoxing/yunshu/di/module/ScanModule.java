package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.ScanContract;
import com.xiaoxing.yunshu.mvp.model.ScanModel;


@Module
public class ScanModule {
    private ScanContract.View view;

    /**
     * 构建ScanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ScanModule(ScanContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    ScanContract.View provideScanView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    ScanContract.Model provideScanModel(ScanModel model) {
        return model;
    }
}