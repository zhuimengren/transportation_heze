package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.GrabOrdersModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.GrabOrdersActivity;

@ActivityScope
@Component(modules = GrabOrdersModule.class, dependencies = AppComponent.class)
public interface GrabOrdersComponent {
    void inject(GrabOrdersActivity activity);
}