package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.SheZhiContract;
import com.xiaoxing.yunshu.mvp.model.SheZhiModel;


@Module
public class SheZhiModule {
    private SheZhiContract.View view;

    /**
     * 构建SheZhiModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public SheZhiModule(SheZhiContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    SheZhiContract.View provideSheZhiView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    SheZhiContract.Model provideSheZhiModel(SheZhiModel model) {
        return model;
    }
}