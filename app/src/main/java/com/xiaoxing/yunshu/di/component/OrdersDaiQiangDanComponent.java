package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.OrdersDaiQiangDanModule;

import com.jess.arms.di.scope.FragmentScope;
import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersDaiQiangDanFragment;

@FragmentScope
@Component(modules = OrdersDaiQiangDanModule.class, dependencies = AppComponent.class)
public interface OrdersDaiQiangDanComponent {
    void inject(OrdersDaiQiangDanFragment fragment);
}