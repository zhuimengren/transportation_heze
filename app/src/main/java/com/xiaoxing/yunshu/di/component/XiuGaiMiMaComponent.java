package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.XiuGaiMiMaModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.XiuGaiMiMaActivity;

@ActivityScope
@Component(modules = XiuGaiMiMaModule.class, dependencies = AppComponent.class)
public interface XiuGaiMiMaComponent {
    void inject(XiuGaiMiMaActivity activity);
}