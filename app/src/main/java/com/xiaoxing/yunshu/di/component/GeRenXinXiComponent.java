package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.GeRenXinXiModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.GeRenXinXiActivity;

@ActivityScope
@Component(modules = GeRenXinXiModule.class, dependencies = AppComponent.class)
public interface GeRenXinXiComponent {
    void inject(GeRenXinXiActivity activity);
}