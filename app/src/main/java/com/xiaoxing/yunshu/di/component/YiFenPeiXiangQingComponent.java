package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.YiFenPeiXiangQingModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.YiFenPeiXiangQingActivity;

@ActivityScope
@Component(modules = YiFenPeiXiangQingModule.class, dependencies = AppComponent.class)
public interface YiFenPeiXiangQingComponent {
    void inject(YiFenPeiXiangQingActivity activity);
}