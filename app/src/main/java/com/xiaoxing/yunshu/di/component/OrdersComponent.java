package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.OrdersModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.OrdersActivity;

@ActivityScope
@Component(modules = OrdersModule.class, dependencies = AppComponent.class)
public interface OrdersComponent {
    void inject(OrdersActivity activity);
}