package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.ForgotPwdContract;
import com.xiaoxing.yunshu.mvp.model.ForgotPwdModel;


@Module
public class ForgotPwdModule {
    private ForgotPwdContract.View view;

    /**
     * 构建ForgotPwdModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ForgotPwdModule(ForgotPwdContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    ForgotPwdContract.View provideForgotPwdView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    ForgotPwdContract.Model provideForgotPwdModel(ForgotPwdModel model) {
        return model;
    }
}