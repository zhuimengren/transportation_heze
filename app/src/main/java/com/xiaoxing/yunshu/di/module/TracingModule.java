package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.TracingContract;
import com.xiaoxing.yunshu.mvp.model.TracingModel;


@Module
public class TracingModule {
    private TracingContract.View view;

    /**
     * 构建TracingModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public TracingModule(TracingContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    TracingContract.View provideTracingView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    TracingContract.Model provideTracingModel(TracingModel model) {
        return model;
    }
}