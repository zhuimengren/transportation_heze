package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.GrabOrdersContract;
import com.xiaoxing.yunshu.mvp.model.GrabOrdersModel;


@Module
public class GrabOrdersModule {
    private GrabOrdersContract.View view;

    /**
     * 构建GrabOrdersModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public GrabOrdersModule(GrabOrdersContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    GrabOrdersContract.View provideGrabOrdersView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    GrabOrdersContract.Model provideGrabOrdersModel(GrabOrdersModel model) {
        return model;
    }
}