package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.ScanModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.ScanActivity;

@ActivityScope
@Component(modules = ScanModule.class, dependencies = AppComponent.class)
public interface ScanComponent {
    void inject(ScanActivity activity);
}