package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.OrdersJinXingZhongContract;
import com.xiaoxing.yunshu.mvp.model.OrdersJinXingZhongModel;


@Module
public class OrdersJinXingZhongModule {
    private OrdersJinXingZhongContract.View view;

    /**
     * 构建OrdersJinXingZhongModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public OrdersJinXingZhongModule(OrdersJinXingZhongContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    OrdersJinXingZhongContract.View provideOrdersJinXingZhongView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    OrdersJinXingZhongContract.Model provideOrdersJinXingZhongModel(OrdersJinXingZhongModel model) {
        return model;
    }
}