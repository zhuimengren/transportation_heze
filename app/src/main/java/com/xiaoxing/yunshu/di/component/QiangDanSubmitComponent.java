package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.QiangDanSubmitModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.QiangDanSubmitActivity;

@ActivityScope
@Component(modules = QiangDanSubmitModule.class, dependencies = AppComponent.class)
public interface QiangDanSubmitComponent {
    void inject(QiangDanSubmitActivity activity);
}