package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.WanChengModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.WanChengActivity;

@ActivityScope
@Component(modules = WanChengModule.class, dependencies = AppComponent.class)
public interface WanChengComponent {
    void inject(WanChengActivity activity);
}