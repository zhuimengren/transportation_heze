package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.YiWanChengXiangQingContract;
import com.xiaoxing.yunshu.mvp.model.YiWanChengXiangQingModel;


@Module
public class YiWanChengXiangQingModule {
    private YiWanChengXiangQingContract.View view;

    /**
     * 构建YiWanChengXiangQingModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public YiWanChengXiangQingModule(YiWanChengXiangQingContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    YiWanChengXiangQingContract.View provideYiWanChengXiangQingView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    YiWanChengXiangQingContract.Model provideYiWanChengXiangQingModel(YiWanChengXiangQingModel model) {
        return model;
    }
}