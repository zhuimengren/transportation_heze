package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.OrdersYiWanChengModule;

import com.jess.arms.di.scope.FragmentScope;
import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersYiWanChengFragment;

@FragmentScope
@Component(modules = OrdersYiWanChengModule.class, dependencies = AppComponent.class)
public interface OrdersYiWanChengComponent {
    void inject(OrdersYiWanChengFragment fragment);
}