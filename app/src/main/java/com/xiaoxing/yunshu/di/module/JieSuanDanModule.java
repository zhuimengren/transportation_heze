package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.JieSuanDanContract;
import com.xiaoxing.yunshu.mvp.model.JieSuanDanModel;


@Module
public class JieSuanDanModule {
    private JieSuanDanContract.View view;

    /**
     * 构建JieSuanDanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public JieSuanDanModule(JieSuanDanContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    JieSuanDanContract.View provideJieSuanDanView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    JieSuanDanContract.Model provideJieSuanDanModel(JieSuanDanModel model) {
        return model;
    }
}