package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.TracingModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.TracingActivity;

@ActivityScope
@Component(modules = TracingModule.class, dependencies = AppComponent.class)
public interface TracingComponent {
    void inject(TracingActivity activity);
}