package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.YunShuMainContract;
import com.xiaoxing.yunshu.mvp.model.YunShuMainModel;


@Module
public class YunShuMainModule {
    private YunShuMainContract.View view;

    /**
     * 构建YunShuMainModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public YunShuMainModule(YunShuMainContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    YunShuMainContract.View provideYunShuMainView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    YunShuMainContract.Model provideYunShuMainModel(YunShuMainModel model) {
        return model;
    }
}