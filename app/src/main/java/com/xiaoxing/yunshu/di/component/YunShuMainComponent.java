package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.YunShuMainModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.YunShuChuKuMainActivity;

@ActivityScope
@Component(modules = YunShuMainModule.class, dependencies = AppComponent.class)
public interface YunShuMainComponent {
    void inject(YunShuChuKuMainActivity activity);
}