package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.XiuGaiMiMaContract;
import com.xiaoxing.yunshu.mvp.model.XiuGaiMiMaModel;


@Module
public class XiuGaiMiMaModule {
    private XiuGaiMiMaContract.View view;

    /**
     * 构建XiuGaiMiMaModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public XiuGaiMiMaModule(XiuGaiMiMaContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    XiuGaiMiMaContract.View provideXiuGaiMiMaView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    XiuGaiMiMaContract.Model provideXiuGaiMiMaModel(XiuGaiMiMaModel model) {
        return model;
    }
}