package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.GuiHuanContract;
import com.xiaoxing.yunshu.mvp.model.GuiHuanModel;


@Module
public class GuiHuanModule {
    private GuiHuanContract.View view;

    /**
     * 构建GuiHuanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public GuiHuanModule(GuiHuanContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    GuiHuanContract.View provideGuiHuanView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    GuiHuanContract.Model provideGuiHuanModel(GuiHuanModel model) {
        return model;
    }
}