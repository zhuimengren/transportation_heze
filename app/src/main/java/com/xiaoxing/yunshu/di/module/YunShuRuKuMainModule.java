package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.YunShuRuKuMainContract;
import com.xiaoxing.yunshu.mvp.model.YunShuRuKuMainModel;


@Module
public class YunShuRuKuMainModule {
    private YunShuRuKuMainContract.View view;

    /**
     * 构建YunShuRuKuMainModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public YunShuRuKuMainModule(YunShuRuKuMainContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    YunShuRuKuMainContract.View provideYunShuRuKuMainView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    YunShuRuKuMainContract.Model provideYunShuRuKuMainModel(YunShuRuKuMainModel model) {
        return model;
    }
}