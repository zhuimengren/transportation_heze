package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.InTransitModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.InTransitActivity;

@ActivityScope
@Component(modules = InTransitModule.class, dependencies = AppComponent.class)
public interface InTransitComponent {
    void inject(InTransitActivity activity);
}