package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.JinXingZhongXiangQingContract;
import com.xiaoxing.yunshu.mvp.model.JinXingZhongXiangQingModel;


@Module
public class JinXingZhongXiangQingModule {
    private JinXingZhongXiangQingContract.View view;

    /**
     * 构建JinXingZhongXiangQingModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public JinXingZhongXiangQingModule(JinXingZhongXiangQingContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    JinXingZhongXiangQingContract.View provideJinXingZhongXiangQingView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    JinXingZhongXiangQingContract.Model provideJinXingZhongXiangQingModel(JinXingZhongXiangQingModel model) {
        return model;
    }
}