package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.QuanXianSheZhiModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.QuanXianSheZhiActivity;

@ActivityScope
@Component(modules = QuanXianSheZhiModule.class, dependencies = AppComponent.class)
public interface QuanXianSheZhiComponent {
    void inject(QuanXianSheZhiActivity activity);
}