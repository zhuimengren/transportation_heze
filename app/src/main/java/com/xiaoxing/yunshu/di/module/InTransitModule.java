package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.InTransitContract;
import com.xiaoxing.yunshu.mvp.model.InTransitModel;


@Module
public class InTransitModule {
    private InTransitContract.View view;

    /**
     * 构建InTransitModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InTransitModule(InTransitContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    InTransitContract.View provideInTransitView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    InTransitContract.Model provideInTransitModel(InTransitModel model) {
        return model;
    }
}