package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.YiFenPeiModule;

import com.jess.arms.di.scope.FragmentScope;
import com.xiaoxing.yunshu.mvp.ui.fragment.YiFenPeiFragment;

@FragmentScope
@Component(modules = YiFenPeiModule.class, dependencies = AppComponent.class)
public interface YiFenPeiComponent {
    void inject(YiFenPeiFragment fragment);
}