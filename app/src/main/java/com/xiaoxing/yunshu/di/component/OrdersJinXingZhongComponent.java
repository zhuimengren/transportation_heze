package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.OrdersJinXingZhongModule;

import com.jess.arms.di.scope.FragmentScope;
import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersJinXingZhongFragment;

@FragmentScope
@Component(modules = OrdersJinXingZhongModule.class, dependencies = AppComponent.class)
public interface OrdersJinXingZhongComponent {
    void inject(OrdersJinXingZhongFragment fragment);
}