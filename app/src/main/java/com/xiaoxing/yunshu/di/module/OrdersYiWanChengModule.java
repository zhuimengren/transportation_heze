package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.OrdersYiWanChengContract;
import com.xiaoxing.yunshu.mvp.model.OrdersYiWanChengModel;


@Module
public class OrdersYiWanChengModule {
    private OrdersYiWanChengContract.View view;

    /**
     * 构建OrdersYiWanChengModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public OrdersYiWanChengModule(OrdersYiWanChengContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    OrdersYiWanChengContract.View provideOrdersYiWanChengView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    OrdersYiWanChengContract.Model provideOrdersYiWanChengModel(OrdersYiWanChengModel model) {
        return model;
    }
}