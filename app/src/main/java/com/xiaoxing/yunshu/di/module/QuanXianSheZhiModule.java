package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.QuanXianSheZhiContract;
import com.xiaoxing.yunshu.mvp.model.QuanXianSheZhiModel;


@Module
public class QuanXianSheZhiModule {
    private QuanXianSheZhiContract.View view;

    /**
     * 构建QuanXianSheZhiModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public QuanXianSheZhiModule(QuanXianSheZhiContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    QuanXianSheZhiContract.View provideQuanXianSheZhiView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    QuanXianSheZhiContract.Model provideQuanXianSheZhiModel(QuanXianSheZhiModel model) {
        return model;
    }
}