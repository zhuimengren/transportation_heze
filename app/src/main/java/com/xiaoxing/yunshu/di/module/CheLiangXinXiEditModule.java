package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiEditContract;
import com.xiaoxing.yunshu.mvp.model.CheLiangXinXiEditModel;


@Module
public class CheLiangXinXiEditModule {
    private CheLiangXinXiEditContract.View view;

    /**
     * 构建CheLiangXinXiEditModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public CheLiangXinXiEditModule(CheLiangXinXiEditContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    CheLiangXinXiEditContract.View provideCheLiangXinXiEditView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    CheLiangXinXiEditContract.Model provideCheLiangXinXiEditModel(CheLiangXinXiEditModel model) {
        return model;
    }
}