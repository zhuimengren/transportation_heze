package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiAddContract;
import com.xiaoxing.yunshu.mvp.model.CheLiangXinXiAddModel;


@Module
public class CheLiangXinXiAddModule {
    private CheLiangXinXiAddContract.View view;

    /**
     * 构建CheLiangXinXiAddModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public CheLiangXinXiAddModule(CheLiangXinXiAddContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    CheLiangXinXiAddContract.View provideCheLiangXinXiAddView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    CheLiangXinXiAddContract.Model provideCheLiangXinXiAddModel(CheLiangXinXiAddModel model) {
        return model;
    }
}