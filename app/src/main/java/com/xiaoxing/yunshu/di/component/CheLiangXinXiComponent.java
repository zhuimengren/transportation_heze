package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.CheLiangXinXiModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.CheLiangXinXiActivity;

@ActivityScope
@Component(modules = CheLiangXinXiModule.class, dependencies = AppComponent.class)
public interface CheLiangXinXiComponent {
    void inject(CheLiangXinXiActivity activity);
}