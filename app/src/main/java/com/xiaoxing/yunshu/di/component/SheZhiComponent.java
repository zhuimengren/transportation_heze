package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.SheZhiModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.SheZhiActivity;

@ActivityScope
@Component(modules = SheZhiModule.class, dependencies = AppComponent.class)
public interface SheZhiComponent {
    void inject(SheZhiActivity activity);
}