package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.YiWanChengXiangQingModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.YiWanChengXiangQingActivity;

@ActivityScope
@Component(modules = YiWanChengXiangQingModule.class, dependencies = AppComponent.class)
public interface YiWanChengXiangQingComponent {
    void inject(YiWanChengXiangQingActivity activity);
}