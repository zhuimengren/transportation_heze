package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.GeRenXinXiContract;
import com.xiaoxing.yunshu.mvp.model.GeRenXinXiModel;


@Module
public class GeRenXinXiModule {
    private GeRenXinXiContract.View view;

    /**
     * 构建GeRenXinXiModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public GeRenXinXiModule(GeRenXinXiContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    GeRenXinXiContract.View provideGeRenXinXiView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    GeRenXinXiContract.Model provideGeRenXinXiModel(GeRenXinXiModel model) {
        return model;
    }
}