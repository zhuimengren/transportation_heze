package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.QiangDanDetailContract;
import com.xiaoxing.yunshu.mvp.model.QiangDanDetailModel;


@Module
public class QiangDanDetailModule {
    private QiangDanDetailContract.View view;

    /**
     * 构建QiangDanDetailModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public QiangDanDetailModule(QiangDanDetailContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    QiangDanDetailContract.View provideQiangDanDetailView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    QiangDanDetailContract.Model provideQiangDanDetailModel(QiangDanDetailModel model) {
        return model;
    }
}