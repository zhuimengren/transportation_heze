package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.LiShiJiLuContract;
import com.xiaoxing.yunshu.mvp.model.LiShiJiLuModel;


@Module
public class LiShiJiLuModule {
    private LiShiJiLuContract.View view;

    /**
     * 构建LiShiJiLuModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public LiShiJiLuModule(LiShiJiLuContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    LiShiJiLuContract.View provideLiShiJiLuView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    LiShiJiLuContract.Model provideLiShiJiLuModel(LiShiJiLuModel model) {
        return model;
    }
}