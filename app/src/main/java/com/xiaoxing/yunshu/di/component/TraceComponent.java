package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.TraceModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.TraceActivity;

@ActivityScope
@Component(modules = TraceModule.class, dependencies = AppComponent.class)
public interface TraceComponent {
    void inject(TraceActivity activity);
}