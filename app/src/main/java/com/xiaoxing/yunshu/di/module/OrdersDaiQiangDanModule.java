package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.OrdersDaiQiangDanContract;
import com.xiaoxing.yunshu.mvp.model.OrdersDaiQiangDanModel;


@Module
public class OrdersDaiQiangDanModule {
    private OrdersDaiQiangDanContract.View view;

    /**
     * 构建OrdersDaiQiangDanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public OrdersDaiQiangDanModule(OrdersDaiQiangDanContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    OrdersDaiQiangDanContract.View provideOrdersDaiQiangDanView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    OrdersDaiQiangDanContract.Model provideOrdersDaiQiangDanModel(OrdersDaiQiangDanModel model) {
        return model;
    }
}