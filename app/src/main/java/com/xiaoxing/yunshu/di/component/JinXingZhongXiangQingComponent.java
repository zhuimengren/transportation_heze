package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.JinXingZhongXiangQingModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.JinXingZhongXiangQingActivity;

@ActivityScope
@Component(modules = JinXingZhongXiangQingModule.class, dependencies = AppComponent.class)
public interface JinXingZhongXiangQingComponent {
    void inject(JinXingZhongXiangQingActivity activity);
}