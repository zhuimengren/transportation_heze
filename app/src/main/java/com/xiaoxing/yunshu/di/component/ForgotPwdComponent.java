package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.ForgotPwdModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.ForgotPwdActivity;

@ActivityScope
@Component(modules = ForgotPwdModule.class, dependencies = AppComponent.class)
public interface ForgotPwdComponent {
    void inject(ForgotPwdActivity activity);
}