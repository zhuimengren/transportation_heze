package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.TraceContract;
import com.xiaoxing.yunshu.mvp.model.TraceModel;


@Module
public class TraceModule {
    private TraceContract.View view;

    /**
     * 构建TraceModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public TraceModule(TraceContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    TraceContract.View provideTraceView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    TraceContract.Model provideTraceModel(TraceModel model) {
        return model;
    }
}