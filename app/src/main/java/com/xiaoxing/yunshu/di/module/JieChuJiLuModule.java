package com.xiaoxing.yunshu.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.xiaoxing.yunshu.mvp.contract.JieChuJiLuContract;
import com.xiaoxing.yunshu.mvp.model.JieChuJiLuModel;


@Module
public class JieChuJiLuModule {
    private JieChuJiLuContract.View view;

    /**
     * 构建JieChuJiLuModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public JieChuJiLuModule(JieChuJiLuContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    JieChuJiLuContract.View provideJieChuJiLuView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    JieChuJiLuContract.Model provideJieChuJiLuModel(JieChuJiLuModel model) {
        return model;
    }
}