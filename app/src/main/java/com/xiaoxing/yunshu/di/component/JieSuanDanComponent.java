package com.xiaoxing.yunshu.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.xiaoxing.yunshu.di.module.JieSuanDanModule;

import com.jess.arms.di.scope.ActivityScope;
import com.xiaoxing.yunshu.mvp.ui.activity.JieSuanDanActivity;

@ActivityScope
@Component(modules = JieSuanDanModule.class, dependencies = AppComponent.class)
public interface JieSuanDanComponent {
    void inject(JieSuanDanActivity activity);
}