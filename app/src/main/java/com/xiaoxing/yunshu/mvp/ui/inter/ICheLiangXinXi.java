package com.xiaoxing.yunshu.mvp.ui.inter;


import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXi;

/**
 * @author 小星
 * @class describe
 * @time 2020/1/2 0002 11:33
 */
public interface ICheLiangXinXi {
    void cheLiangXinXiDel(CheLiangXinXi.DataBean item);

    void cheLiangXinXiEdit(CheLiangXinXi.DataBean item);

    void setDefault(String isDefault, String address_id);
}
