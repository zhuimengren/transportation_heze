package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.QuanXianSheZhiContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.QuanXianSheZhi;

import io.reactivex.Observable;


@ActivityScope
public class QuanXianSheZhiModel extends BaseModel implements QuanXianSheZhiContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public QuanXianSheZhiModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<QuanXianSheZhi> getQuanXianSheZhiData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getQuanXianSheZhiData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}