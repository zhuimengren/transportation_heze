package com.xiaoxing.yunshu.mvp.ui.entity;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2020/4/15 0015 8:46
 */
public class GetDistributingCount {


    /**
     * Data : 0
     * Code : 200
     * Message : 获取配送中配送单个数成功！
     */

    private int Data;
    private int Code;
    private String Message;

    public int getData() {
        return Data;
    }

    public void setData(int Data) {
        this.Data = Data;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}
