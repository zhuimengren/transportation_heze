package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerQiangDanSubmitComponent;
import com.xiaoxing.yunshu.di.module.QiangDanSubmitModule;
import com.xiaoxing.yunshu.mvp.contract.QiangDanSubmitContract;
import com.xiaoxing.yunshu.mvp.presenter.QiangDanSubmitPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanSubmit;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.XEditText;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_QIANG_DAN_SUBMIT)
public class QiangDanSubmitActivity extends BaseActivity<QiangDanSubmitPresenter> implements QiangDanSubmitContract.View {

    @BindView(R.id.tv_qiang_dan_hao)
    TextView tvQiangDanHao;
    @BindView(R.id.xet_price)
    XEditText xetPrice;
    @BindView(R.id.xet_bei_zhu)
    XEditText xetBeiZhu;
    @BindView(R.id.btn_save)
    Button btnSave;
    private String mGrabNo;
    private String mPrice;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerQiangDanSubmitComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .qiangDanSubmitModule(new QiangDanSubmitModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_qiang_dan_submit; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_qiang_dan_submit));
        //getQiangDanSubmitData();

        mGrabNo = getIntent().getExtras().getString("grabNo");
        mPrice = getIntent().getExtras().getString("price");
        tvQiangDanHao.setText(mGrabNo);
        xetPrice.setText(mPrice);

    }

    @Override
    public void getQiangDanSubmitDataSuccess(QiangDanSubmit entityData) {

        showMessage(entityData.getMessage());
        if (entityData.getCode() == 200) {
            killMyself();
        } else if (entityData.getCode() == 300) {
            Utils.navigation(QiangDanSubmitActivity.this, RouterHub.ACTIVITY_CHE_LIANG_XIN_XI);
        }else if (entityData.getCode() == 301) {
            Utils.navigation(QiangDanSubmitActivity.this, RouterHub.ACTIVITY_GE_REN_XIN_XI);
        }

    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick(R.id.btn_save)
    public void onViewClicked() {
        if (TextUtils.isEmpty(getPrice())) {
            showMessage("请输入报价");
            return;
        }
        getQiangDanSubmitData();
    }

    private String getPrice() {
        return xetPrice.getText().toString();
    }

    private void getQiangDanSubmitData() {
        HashMap<String, String> map = new HashMap<>();
//        String jsonData = "{\"DriverId\":\"" + mSharedPreferencesHelper.getString(BaseConstants.UID) + "\",\"GrabNo\":\"" + mGrabNo + "\",\"GivePrice\":" + getPrice() + ",\"Remark\":\"" + getBeiZhu() + "\"}";
//        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonData);
        map.put("grabNo", mGrabNo);
        map.put("givePrice", getPrice());
        map.put("remark", getBeiZhu());
//        map.put("DriverId", mSharedPreferencesHelper.getString(BaseConstants.UID));
        mPresenter.getQiangDanSubmitData(map);
    }

    private String getBeiZhu() {
        return xetBeiZhu.getText().toString();
    }
}
