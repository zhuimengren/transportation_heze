package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerCheLiangXinXiAddComponent;
import com.xiaoxing.yunshu.di.module.CheLiangXinXiAddModule;
import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiAddContract;
import com.xiaoxing.yunshu.mvp.presenter.CheLiangXinXiAddPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXiAdd;
import com.xiaoxing.yunshu.mvp.ui.entity.GetTruckTypeList;
import com.xw.repo.XEditText;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import okhttp3.RequestBody;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_CHE_LIANG_XIN_XI_ADD)
public class CheLiangXinXiAddActivity extends BaseActivity<CheLiangXinXiAddPresenter> implements CheLiangXinXiAddContract.View {


    @BindView(R.id.xet_che_pai_hao)
    XEditText xetChePaiHao;
    @BindView(R.id.nice_spinner)
    NiceSpinner mNiceSpinner;
    @BindView(R.id.xet_zai_zhong)
    XEditText xetZaiZhong;
    @BindView(R.id.xet_che_xiang_chang)
    XEditText xetCheXiangChang;
    @BindView(R.id.xet_che_xiang_kuan)
    XEditText xetCheXiangKuan;
    @BindView(R.id.xet_che_xiang_gao)
    XEditText xetCheXiangGao;
    @BindView(R.id.nice_spinner_che_liang_suo_shu)
    NiceSpinner mNiceSpinnerCheLiangSuoShu;
    @BindView(R.id.xet_suo_shu_qi_ye)
    XEditText xetSuoShuQiYe;
    @BindView(R.id.cb_default)
    CheckBox cbDefault;
    @BindView(R.id.btn_save)
    Button btnSave;
    private String mTypeId;
    private String mTypeName;
    private int mCheLiangSuoShuIndex = 0;
    private String mCheLiangSuoShuName;


    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerCheLiangXinXiAddComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .cheLiangXinXiAddModule(new CheLiangXinXiAddModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_che_liang_xin_xi_add; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_che_liang_xin_xi_add));
        //getCheLiangXinXiAddData();
        initCheLiangSuoShu();
        getTruckTypeList();
    }

    private void initCheLiangSuoShu() {

        ArrayList<String> dataNames = new ArrayList();

        dataNames.add("个人");
        dataNames.add("企业");

        mNiceSpinnerCheLiangSuoShu.attachDataSource(dataNames);
        mCheLiangSuoShuIndex = 0;
        mCheLiangSuoShuName = dataNames.get(0);

        mNiceSpinnerCheLiangSuoShu.addOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCheLiangSuoShuIndex = position;
                mCheLiangSuoShuName = dataNames.get(position);
            }
        });
    }

    private void getTruckTypeList() {

        HashMap<String, String> map = new HashMap<>();
        mPresenter.getTruckTypeList(map);
    }

    @Override
    public void getCheLiangXinXiAddDataSuccess(CheLiangXinXiAdd entityData) {

        showMessage(entityData.getMessage());
        if (entityData.getCode() == 200) {
            killMyself();
        }
    }

    @Override
    public void getTruckTypeListSuccess(GetTruckTypeList entityData) {

        if (entityData.getData() != null && entityData.getData().size() > 0) {

            List<GetTruckTypeList.DataBean> dataBeanList = entityData.getData();

            ArrayList<String> dataNames = new ArrayList();

            for (int i = 0; i < dataBeanList.size(); i++) {
                dataNames.add(dataBeanList.get(i).getBaseInformationContent());
            }

            mNiceSpinner.attachDataSource(dataNames);
            mTypeId = String.valueOf(dataBeanList.get(0).getBaseInformationId());
            mTypeName = dataBeanList.get(0).getBaseInformationContent();

            mNiceSpinner.addOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mTypeId = String.valueOf(dataBeanList.get(position).getBaseInformationId());
                    mTypeName = dataBeanList.get(position).getBaseInformationContent();
                }
            });
        }

    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick(R.id.btn_save)
    public void onViewClicked() {

        if (TextUtils.isEmpty(getChePaiHao())) {
            showMessage("请输入车牌号");
            return;
        }
        if (TextUtils.isEmpty(getZaiZhong())) {
            showMessage("请输入载重");
            return;
        }
        if (TextUtils.isEmpty(getCheXiangChang())) {
            showMessage("请输入车厢长");
            return;
        }
        if (TextUtils.isEmpty(getCheXiangKuan())) {
            showMessage("请输入车厢宽");
            return;
        }
        if (TextUtils.isEmpty(getCheXiangGao())) {
            showMessage("请输入车厢高");
            return;
        }
        if (mCheLiangSuoShuIndex == 1) {
            if (TextUtils.isEmpty(getSuoShuQiYe())) {
                showMessage("请输入所属企业");
                return;
            }
        }
        getCheLiangXinXiAddData();
    }

    private String getChePaiHao() {
        return xetChePaiHao.getText().toString();
    }

    private String getZaiZhong() {
        return xetZaiZhong.getText().toString();
    }

    private String getCheXiangChang() {
        return xetCheXiangChang.getText().toString();
    }

    private String getCheXiangKuan() {
        return xetCheXiangKuan.getText().toString();
    }

    private String getCheXiangGao() {
        return xetCheXiangGao.getText().toString();
    }

    private String getSuoShuQiYe() {
        return xetSuoShuQiYe.getText().toString();
    }

    private void getCheLiangXinXiAddData() {
//        HashMap<String, String> map = new HashMap<>();

        String truckBelongTo = "1";

        if (mCheLiangSuoShuIndex == 0) {
            truckBelongTo = "1";
        } else if (mCheLiangSuoShuIndex == 1) {
            truckBelongTo = "2";
        }

        String isDefault = "false";

        if (cbDefault.isChecked()) {
            isDefault = "true";
        } else {
            isDefault = "false";
        }
        String jsonData = " {\n" +
                "     \"TruckNumber\": \"" + getChePaiHao() + "\",\n" +
                "    \"TruckType\": \"" + mTypeId + "\",\n" +
                "    \"LoadWeight\": \"" + getZaiZhong() + "\",\n" +
                "    \"LoadLength\": \"" + getCheXiangChang() + "\",\n" +
                "    \"LoadWidth\": \"" + getCheXiangKuan() + "\",\n" +
                "    \"LoadHeight\": \"" + getCheXiangGao() + "\",\n" +
                "    \"TruckBelongTo\": \"" + truckBelongTo + "\",\n" +
                "    \"BelongToOrg\": \"" + getSuoShuQiYe() + "\",\n" +
                "    \"IsDefault\": \"" + isDefault + "\"\n" +
                " }";
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonData);
        mPresenter.getCheLiangXinXiAddData(body);
    }
}
