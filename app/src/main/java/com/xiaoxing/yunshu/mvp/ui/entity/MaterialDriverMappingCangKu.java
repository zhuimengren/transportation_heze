package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2018/10/11 0011 16:06
 */
public class MaterialDriverMappingCangKu {
    private String id;
    private String fromstorename;
    private String fromstoreaddress;
    private String out_id;
    private boolean isChoosed;
    private List<MaterialDriverMappingChanPin> tasks;

    public MaterialDriverMappingCangKu() {

    }

    public MaterialDriverMappingCangKu(String id, String fromstorename, String fromstoreaddress) {
        this.id = id;
        this.fromstorename = fromstorename;
        this.fromstoreaddress = fromstoreaddress;
    }

    public String getOut_id() {
        return out_id == null ? "" : out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id == null ? "" : out_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromstorename() {
        return fromstorename;
    }

    public void setFromstorename(String fromstorename) {
        this.fromstorename = fromstorename;
    }

    public String getFromstoreaddress() {
        return fromstoreaddress;
    }

    public void setFromstoreaddress(String fromstoreaddress) {
        this.fromstoreaddress = fromstoreaddress;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }

    public List<MaterialDriverMappingChanPin> getTasks() {
        return tasks;
    }

    public void setTasks(List<MaterialDriverMappingChanPin> tasks) {
        this.tasks = tasks;
    }
}
