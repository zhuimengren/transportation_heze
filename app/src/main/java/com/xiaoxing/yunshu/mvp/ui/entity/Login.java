package com.xiaoxing.yunshu.mvp.ui.entity;

public class Login {


    /**
     * Code : 200
     * Message : 获取成功！
     * Data : {"Driver":{"DriverId":178,"IDCardNo":null,"Birthday":null,"Sex":null,"DrivingAge":null,"InsertUserId":null,"InsertDateTime":null,"ModifyUserId":null,"ModifyDateTime":null},"Role":{"ID":1053,"RoleName":"司机"},"ID":178,"Name":"徐星","LoginName":"xx123456","Password":"c4ca4238a0b923820dcc509a6f75849b","MobilePhone":"15965561796","FixPhone":null,"Email":null,"RoleId":1053,"Rights":{}}
     */

    private int Code;
    private String Message;
    private DataBean Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * Driver : {"DriverId":178,"IDCardNo":null,"Birthday":null,"Sex":null,"DrivingAge":null,"InsertUserId":null,"InsertDateTime":null,"ModifyUserId":null,"ModifyDateTime":null}
         * Role : {"ID":1053,"RoleName":"司机"}
         * ID : 178
         * Name : 徐星
         * LoginName : xx123456
         * Password : c4ca4238a0b923820dcc509a6f75849b
         * MobilePhone : 15965561796
         * FixPhone : null
         * Email : null
         * RoleId : 1053
         * Rights : {}
         */

        private DriverBean Driver;
        private RoleBean Role;
        private int ID;
        private String Name;
        private String LoginName;
        private String Password;
        private String MobilePhone;
        private String FixPhone;
        private String Email;
        private int RoleId;
        private RightsBean Rights;

        public DriverBean getDriver() {
            return Driver;
        }

        public void setDriver(DriverBean Driver) {
            this.Driver = Driver;
        }

        public RoleBean getRole() {
            return Role;
        }

        public void setRole(RoleBean Role) {
            this.Role = Role;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getLoginName() {
            return LoginName;
        }

        public void setLoginName(String LoginName) {
            this.LoginName = LoginName;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getMobilePhone() {
            return MobilePhone;
        }

        public void setMobilePhone(String MobilePhone) {
            this.MobilePhone = MobilePhone;
        }

        public String getFixPhone() {
            return FixPhone;
        }

        public void setFixPhone(String FixPhone) {
            this.FixPhone = FixPhone;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public RightsBean getRights() {
            return Rights;
        }

        public void setRights(RightsBean Rights) {
            this.Rights = Rights;
        }

        public static class DriverBean {
            /**
             * DriverId : 178
             * IDCardNo : null
             * Birthday : null
             * Sex : null
             * DrivingAge : null
             * InsertUserId : null
             * InsertDateTime : null
             * ModifyUserId : null
             * ModifyDateTime : null
             */

            private int DriverId;
            private String IDCardNo;
            private String Birthday;
            private String Sex;
            private String DrivingAge;
            private String InsertUserId;
            private String InsertDateTime;
            private String ModifyUserId;
            private String ModifyDateTime;

            public int getDriverId() {
                return DriverId;
            }

            public void setDriverId(int DriverId) {
                this.DriverId = DriverId;
            }

            public String getIDCardNo() {
                return IDCardNo;
            }

            public void setIDCardNo(String IDCardNo) {
                this.IDCardNo = IDCardNo;
            }

            public String getBirthday() {
                return Birthday;
            }

            public void setBirthday(String Birthday) {
                this.Birthday = Birthday;
            }

            public String getSex() {
                return Sex;
            }

            public void setSex(String Sex) {
                this.Sex = Sex;
            }

            public String getDrivingAge() {
                return DrivingAge;
            }

            public void setDrivingAge(String DrivingAge) {
                this.DrivingAge = DrivingAge;
            }

            public String getInsertUserId() {
                return InsertUserId;
            }

            public void setInsertUserId(String InsertUserId) {
                this.InsertUserId = InsertUserId;
            }

            public String getInsertDateTime() {
                return InsertDateTime;
            }

            public void setInsertDateTime(String InsertDateTime) {
                this.InsertDateTime = InsertDateTime;
            }

            public String getModifyUserId() {
                return ModifyUserId;
            }

            public void setModifyUserId(String ModifyUserId) {
                this.ModifyUserId = ModifyUserId;
            }

            public String getModifyDateTime() {
                return ModifyDateTime;
            }

            public void setModifyDateTime(String ModifyDateTime) {
                this.ModifyDateTime = ModifyDateTime;
            }
        }

        public static class RoleBean {
        }

        public static class RightsBean {
        }
    }
}
