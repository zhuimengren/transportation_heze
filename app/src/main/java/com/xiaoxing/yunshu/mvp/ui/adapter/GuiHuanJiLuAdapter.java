package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.GuiHuanJiLu;

import java.util.List;


public class GuiHuanJiLuAdapter extends BaseQuickAdapter<GuiHuanJiLu, BaseViewHolder> {

    private Context mContext;

    public GuiHuanJiLuAdapter(Context context, @Nullable List<GuiHuanJiLu> data) {
        super(R.layout.item_gui_huan_ji_lu, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, GuiHuanJiLu item) {

        helper.setText(R.id.tv_xing_hao, item.getXingHao());
        helper.setText(R.id.tv_ri_qi, item.getShiJian());
        helper.setText(R.id.tv_jian_shu, "归还" + removeTrim(item.getJianShu()) + "件");
    }

    public static String removeTrim(String str) {
        if (str.indexOf(".") > 0) {
            str = str.replaceAll("0+?$", "");//去掉多余的0
            str = str.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return str;
    }
}
