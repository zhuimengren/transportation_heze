package com.xiaoxing.yunshu.mvp.ui.entity;

public class NotiExtras {

    private String batchId;
    private String address;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
