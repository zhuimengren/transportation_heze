package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerYiWanChengXiangQingComponent;
import com.xiaoxing.yunshu.di.module.YiWanChengXiangQingModule;
import com.xiaoxing.yunshu.mvp.contract.YiWanChengXiangQingContract;
import com.xiaoxing.yunshu.mvp.presenter.YiWanChengXiangQingPresenter;
import com.xiaoxing.yunshu.mvp.ui.adapter.YiWanChengXiangQing1Adapter;
import com.xiaoxing.yunshu.mvp.ui.entity.YiWanChengXiangQing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import me.jessyan.armscomponent.commonres.utils.BottomDialogUtil;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_YI_WAN_CHENG_XIANG_QING)
public class YiWanChengXiangQingActivity extends BaseActivity<YiWanChengXiangQingPresenter> implements YiWanChengXiangQingContract.View, OnRefreshListener {


    private static boolean mIsNeedDemo = true;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.empty_image)
    ImageView empty_image;

    @BindView(R.id.empty)
    View mEmptyLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    RefreshLayout mRefreshLayout;
    private YiWanChengXiangQing1Adapter mAdapter;
    private List<YiWanChengXiangQing.DataBean> mDataBeanList = new ArrayList<>();

    private String mGrabNo;

    //获得结果
    private Map<String, List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean>> children = new HashMap<String, List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean>>();// 子元素数据列表
    private Map<Integer, Integer> mGroupsPosition = new HashMap<>();
    private List<YiWanChengXiangQing.DataBean.TasksBean> groups = new ArrayList<YiWanChengXiangQing.DataBean.TasksBean>();// 组元素数据列表
    private BottomDialogUtil mBottomDialogUtilShare;
    private String mScanTaskNo;
    private String mMaterialName;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerYiWanChengXiangQingComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .yiWanChengXiangQingModule(new YiWanChengXiangQingModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_yi_wan_cheng_xiang_qing; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_yi_wan_cheng_xiang_qing));
        mGrabNo = getIntent().getExtras().getString("grabNo");

        initRefreshLayout();
        initRecyclerView();
        initEmpty();

    }

    private void initRefreshLayout() {
        mRefreshLayout.setRefreshHeader(new
                ClassicsHeader(this).setSpinnerStyle(SpinnerStyle.FixedBehind).setPrimaryColorId(R.color.public_colorPrimary).setAccentColorId(android.R.color.white));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.autoRefresh();
        mRefreshLayout.setEnableLoadMore(false);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(YiWanChengXiangQingActivity.this));
//        mRecyclerView.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, 10, Color.parseColor("#EEEEEE")));
        mRecyclerView.setAdapter(mAdapter = new YiWanChengXiangQing1Adapter(this, groups, children, mGroupsPosition));

        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setAdapter(mAdapter = new YiWanChengXiangQingAdapter(YiWanChengXiangQingActivity.this, mDataBeanList = loadModels()));
//
//        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//            }
//        });
    }

    private void initEmpty() {
        empty_image.setImageResource(R.drawable.ic_empty);
        empty_text.setTextColor(Color.parseColor("#CFCFCF"));
        empty_text.setText("暂无数据下拉刷新");
    }

    private List<YiWanChengXiangQing.DataBean> loadModels() {
        List<YiWanChengXiangQing.DataBean> lists = new ArrayList<>();

        for (int i = 0; i < 5; i++) {

            YiWanChengXiangQing.DataBean dataBean = new YiWanChengXiangQing.DataBean();
            lists.add(dataBean);
        }

        return lists;
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        getYiWanChengXiangQingList();
        mRefreshLayout.finishRefresh();

    }

    @Override
    public void getYiWanChengXiangQingDataSuccess(YiWanChengXiangQing entityList) {

        if (entityList != null && entityList.getData() != null) {
//            mDataBeanList.clear();
//            mDataBeanList.addAll(entityList.getData());
//            mAdapter.notifyDataSetChanged();


            List<YiWanChengXiangQing.DataBean.TasksBean> tasksBeans = entityList.getData().getTasks();

            mAdapter.setDataItems(getSampleItems(tasksBeans));
            mAdapter.notifyDataSetChanged();
            mAdapter.expandAll();

            mEmptyLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        }

    }
    public List<YiWanChengXiangQing1Adapter.OrderListItem> getSampleItems(List<YiWanChengXiangQing.DataBean.TasksBean> tasksBeans) {
        List<YiWanChengXiangQing1Adapter.OrderListItem> items = new ArrayList<>();


        for (int i = 0; i < tasksBeans.size(); i++) {


            int groupPosition = items.size();
            mGroupsPosition.put(groupPosition, i);

            YiWanChengXiangQing.DataBean.TasksBean tasksBean = tasksBeans.get(i);
            tasksBean.setId(String.valueOf(groupPosition));
            groups.add(tasksBeans.get(i));
            items.add(new YiWanChengXiangQing1Adapter.OrderListItem(tasksBean));

            List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean> tasksBeanItems = tasksBean.getItems();

            List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean> products = new ArrayList<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean>();


            for (int i1 = 0; i1 < tasksBeanItems.size(); i1++) {

                YiWanChengXiangQing.DataBean.TasksBean.ItemsBean itemsBean = tasksBeanItems.get(i1);

                if (tasksBeans.get(i).isIsFromOutStorage()) {
                    itemsBean.setFromOutStorage(true);
                } else {
                    itemsBean.setFromOutStorage(false);

                }
                products.add(itemsBean);

                items.add(new YiWanChengXiangQing1Adapter.OrderListItem(groupPosition, itemsBean, i1));
            }

            children.put(String.valueOf(groupPosition), products);// 将组元素的一个唯一值，这里取Id，作为子元素List的Key

            items.add(new YiWanChengXiangQing1Adapter.OrderListItem(tasksBean.getGrabNo(), "", tasksBean, ""));
        }

        return items;
    }
    private void getYiWanChengXiangQingList() {
        HashMap<String, String> map = new HashMap<>();
        map.put("grabNo", mGrabNo);
        mPresenter.getYiWanChengXiangQingList(map);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);

    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }
}
