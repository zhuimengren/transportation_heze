package com.xiaoxing.yunshu.mvp.ui.entity;

import android.content.Context;

import com.xiaoxing.yunshu.mvp.ui.database.SQLiteDBService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InfoByQRCodeMid implements Serializable {

    private String _material;
    private String _unitPrice;
    private String _unit;
    private String _mbm;
    private Integer _mmodel;
    private String _outtime;
    private String _outnum;
    private String _spname;
    private String _DisplayName;
    private String _LoginName;
    private String _uid;
    private String _spid;
    private String _umid;
    private String _Row;

    private SQLiteDBService sqliteDBservice;

    public InfoByQRCodeMid() {

    }

    public InfoByQRCodeMid(Context context) {
        sqliteDBservice = new SQLiteDBService(context);
    }

    // 将JSON对象转化为list
    public List<InfoByQRCodeMid> getListFromJSON(String JsonStr) {
        List<InfoByQRCodeMid> clist = new ArrayList<InfoByQRCodeMid>();

        try {

            JSONArray jay = new JSONArray(JsonStr);// 将字符串转化为JSON数组

            for (int i = 0; i < jay.length(); i++) {
                JSONObject temp = (JSONObject) jay.get(i);

                InfoByQRCodeMid n = new InfoByQRCodeMid();

                n.set_material(temp.getString("material"));
                //n.set_unitPrice(temp.getString("unitPrice"));
                n.set_unit(temp.getString("unit"));
                //n.set_mbm(temp.getString("mbm"));
                //n.set_mmodel(Integer.parseInt(temp.getString("mmodel")));
                n.set_outtime(temp.getString("outtime"));
                n.set_outnum(temp.getString("outnum"));
                //n.set_spname(temp.getString("spname"));
                //n.set_DisplayName(temp.getString("DisplayName"));
                //n.set_LoginName(temp.getString("LoginName"));
                n.set_uid(temp.getString("uid"));
                n.set_spid(temp.getString("spid"));
                //n.set_umid(temp.getString("umid"));
                n.set_Row(temp.getString("Row"));

                clist.add(n);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return clist;

    }

    public String get_material() {
        return _material;
    }

    public void set_material(String _material) {
        this._material = _material;
    }

    public String get_unitPrice() {
        return _unitPrice;
    }

    public void set_unitPrice(String _unitPrice) {
        this._unitPrice = _unitPrice;
    }

    public String get_unit() {
        return _unit;
    }

    public void set_unit(String _unit) {
        this._unit = _unit;
    }

    public String get_mbm() {
        return _mbm;
    }

    public void set_mbm(String _mbm) {
        this._mbm = _mbm;
    }

    public Integer get_mmodel() {
        return _mmodel;
    }

    public void set_mmodel(Integer _mmodel) {
        this._mmodel = _mmodel;
    }

    public String get_outtime() {
        return _outtime;
    }

    public void set_outtime(String _outtime) {
        this._outtime = _outtime;
    }

    public String get_outnum() {
        return _outnum;
    }

    public void set_outnum(String _outnum) {
        this._outnum = _outnum;
    }

    public String get_spname() {
        return _spname;
    }

    public void set_spname(String _spname) {
        this._spname = _spname;
    }

    public String get_DisplayName() {
        return _DisplayName;
    }

    public void set_DisplayName(String _DisplayName) {
        this._DisplayName = _DisplayName;
    }

    public String get_LoginName() {
        return _LoginName;
    }

    public void set_LoginName(String _LoginName) {
        this._LoginName = _LoginName;
    }

    public String get_uid() {
        return _uid;
    }

    public void set_uid(String _uid) {
        this._uid = _uid;
    }

    public String get_spid() {
        return _spid;
    }

    public void set_spid(String _spid) {
        this._spid = _spid;
    }

    public String get_umid() {
        return _umid;
    }

    public void set_umid(String _umid) {
        this._umid = _umid;
    }

    public String get_Row() {
        return _Row;
    }

    public void set_Row(String _Row) {
        this._Row = _Row;
    }

}
