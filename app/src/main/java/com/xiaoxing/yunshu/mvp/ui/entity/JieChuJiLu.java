package com.xiaoxing.yunshu.mvp.ui.entity;

public class JieChuJiLu {
    private String xingHao;
    private String shiJian;
    private String jianShu;

    public String getXingHao() {
        return xingHao;
    }

    public void setXingHao(String xingHao) {
        this.xingHao = xingHao;
    }

    public String getShiJian() {
        return shiJian;
    }

    public void setShiJian(String shiJian) {
        this.shiJian = shiJian;
    }

    public String getJianShu() {
        return jianShu;
    }

    public void setJianShu(String jianShu) {
        this.jianShu = jianShu;
    }

}
