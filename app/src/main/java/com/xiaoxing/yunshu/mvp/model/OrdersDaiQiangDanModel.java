package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.OrdersDaiQiangDanContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersDaiQiangDan;

import io.reactivex.Observable;


@FragmentScope
public class OrdersDaiQiangDanModel extends BaseModel implements OrdersDaiQiangDanContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public OrdersDaiQiangDanModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<OrdersDaiQiangDan> getOrdersDaiQiangDanData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getOrdersDaiQiangDanData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}