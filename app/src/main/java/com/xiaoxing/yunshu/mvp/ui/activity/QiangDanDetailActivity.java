package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerQiangDanDetailComponent;
import com.xiaoxing.yunshu.di.module.QiangDanDetailModule;
import com.xiaoxing.yunshu.mvp.contract.QiangDanDetailContract;
import com.xiaoxing.yunshu.mvp.presenter.QiangDanDetailPresenter;
import com.xiaoxing.yunshu.mvp.ui.adapter.QiangDanDetail1Adapter;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_QIANG_DAN_DETAIL)
public class QiangDanDetailActivity extends BaseActivity<QiangDanDetailPresenter> implements QiangDanDetailContract.View, OnRefreshListener {


    private static boolean mIsNeedDemo = true;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.empty_image)
    ImageView empty_image;

    @BindView(R.id.empty)
    View mEmptyLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    RefreshLayout mRefreshLayout;
    private QiangDanDetail1Adapter mAdapter;
    private List<QiangDanDetail.DataBean> mDataBeanList = new ArrayList<>();
    //获得结果
    private Map<String, List<QiangDanDetail.DataBean.TasksBean.ItemsBean>> children = new HashMap<String, List<QiangDanDetail.DataBean.TasksBean.ItemsBean>>();// 子元素数据列表
    private Map<Integer, Integer> mGroupsPosition = new HashMap<>();
    private List<QiangDanDetail.DataBean.TasksBean> groups = new ArrayList<QiangDanDetail.DataBean.TasksBean>();// 组元素数据列表

    private String mGrabNo;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerQiangDanDetailComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .qiangDanDetailModule(new QiangDanDetailModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_qiang_dan_detail; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_qiang_dan_detail));
        mGrabNo = getIntent().getExtras().getString("grabNo");

        initRefreshLayout();
        initRecyclerView();
        initEmpty();
        getQiangDanDetailList();

    }

    private void initRefreshLayout() {
        mRefreshLayout.setRefreshHeader(new
                ClassicsHeader(this).setSpinnerStyle(SpinnerStyle.FixedBehind).setPrimaryColorId(R.color.public_colorPrimary).setAccentColorId(android.R.color.white));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableRefresh(false);
        mRefreshLayout.autoRefresh();
        mRefreshLayout.setEnableLoadMore(false);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(QiangDanDetailActivity.this));
//        mRecyclerView.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, 10, Color.parseColor("#EEEEEE")));
//        mRecyclerView.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, 10, Color.parseColor("#EEEEEE")));

        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setAdapter(mAdapter = new QiangDanDetailAdapter(QiangDanDetailActivity.this, mDataBeanList = loadModels()));
        mRecyclerView.setAdapter(mAdapter = new QiangDanDetail1Adapter(QiangDanDetailActivity.this, groups, children, mGroupsPosition));

//        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//            }
//        });
        mAdapter.setWanChengInterface(new QiangDanDetail1Adapter.IQiangDanQueDingInterface() {
            @Override
            public void queDing(String grabNo, String price) {
                Bundle bundle = new Bundle();
                bundle.putString("grabNo", grabNo);
                bundle.putString("price", price);
                Utils.navigation(QiangDanDetailActivity.this, RouterHub.ACTIVITY_QIANG_DAN_SUBMIT, bundle);

            }
        });
    }

    private void initEmpty() {
        empty_image.setImageResource(R.drawable.ic_empty);
        empty_text.setTextColor(Color.parseColor("#CFCFCF"));
        empty_text.setText("暂无数据");
    }

    private void getQiangDanDetailList() {
        HashMap<String, String> map = new HashMap<>();
        map.put("grabNo", mGrabNo);
        mPresenter.getQiangDanDetailList(map);
    }


    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        mRefreshLayout.finishRefresh();

    }

    @Override
    public void getQiangDanDetailDataSuccess(QiangDanDetail entityList) {

        if (entityList != null && entityList.getData() != null) {
//            mDataBeanList.clear();
//            mDataBeanList.addAll(entityList.getData());
//            mAdapter.notifyDataSetChanged();


            List<QiangDanDetail.DataBean.TasksBean> tasksBeans = entityList.getData().getTasks();

            mAdapter.setDataItems(getSampleItems(tasksBeans));
            mAdapter.notifyDataSetChanged();
            mAdapter.expandAll();

            mEmptyLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        }

    }

    public List<QiangDanDetail1Adapter.OrderListItem> getSampleItems(List<QiangDanDetail.DataBean.TasksBean> tasksBeans) {
        List<QiangDanDetail1Adapter.OrderListItem> items = new ArrayList<>();


        for (int i = 0; i < tasksBeans.size(); i++) {


            int groupPosition = items.size();
            mGroupsPosition.put(groupPosition, i);

            QiangDanDetail.DataBean.TasksBean tasksBean = tasksBeans.get(i);

            groups.add(tasksBeans.get(i));
            items.add(new QiangDanDetail1Adapter.OrderListItem(tasksBean));

            List<QiangDanDetail.DataBean.TasksBean.ItemsBean> tasksBeanItems = tasksBean.getItems();

            List<QiangDanDetail.DataBean.TasksBean.ItemsBean> products = new ArrayList<QiangDanDetail.DataBean.TasksBean.ItemsBean>();


            for (int i1 = 0; i1 < tasksBeanItems.size(); i1++) {

                QiangDanDetail.DataBean.TasksBean.ItemsBean itemsBean = tasksBeanItems.get(i1);


                products.add(itemsBean);

                items.add(new QiangDanDetail1Adapter.OrderListItem(groupPosition, itemsBean, i1));
            }

            children.put(String.valueOf(groupPosition), products);// 将组元素的一个唯一值，这里取Id，作为子元素List的Key

        }
        items.add(new QiangDanDetail1Adapter.OrderListItem(mGrabNo, "", "", ""));

        return items;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);

    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }
}
