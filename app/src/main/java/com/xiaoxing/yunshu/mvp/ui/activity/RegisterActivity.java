package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerRegisterComponent;
import com.xiaoxing.yunshu.di.module.RegisterModule;
import com.xiaoxing.yunshu.mvp.contract.RegisterContract;
import com.xiaoxing.yunshu.mvp.presenter.RegisterPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.Register;
import com.xiaoxing.yunshu.mvp.ui.entity.RegisterSendValidateCode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.CountDownButton;
import me.jessyan.armscomponent.commonres.view.XEditText;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_REGISTER)
public class RegisterActivity extends BaseActivity<RegisterPresenter> implements RegisterContract.View {


    @BindView(R.id.xet_name)
    XEditText xetName;
    @BindView(R.id.xet_phone)
    XEditText xetPhone;
    @BindView(R.id.xet_pwd)
    XEditText xetPwd;
    @BindView(R.id.xet_pwd_again)
    XEditText xetPwdAgain;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.xet_account)
    XEditText xetAccount;
    @BindView(R.id.xet_verification_code)
    XEditText xetVerificationCode;
    @BindView(R.id.cd_btn)
    CountDownButton cdBtn;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerRegisterComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .registerModule(new RegisterModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_register; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_register));
        //getRegisterData();

        cdBtn.setEnabled(!TextUtils.isEmpty(xetAccount.getText()));

        xetPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cdBtn.setEnabled(!TextUtils.isEmpty(s.toString()));// && !mCountDownButton.isCountDownNow()
                if (!TextUtils.isEmpty(s.toString())) {
                    cdBtn.setBackground(getResources().getDrawable(R.drawable.public_shape_blue_bg_corner_5dp));
                } else {
                    cdBtn.setBackground(getResources().getDrawable(R.drawable.public_shape_gray_bg_corner_5dp));
                }
            }
        });
    }

    @Override
    public void getRegisterDataSuccess(Register entityData) {

        showMessage(entityData.getMessage());
        if (entityData.getCode() == 200) {
            killMyself();
        }

    }

    @Override
    public void registerSendValidateCodeSuccess(RegisterSendValidateCode entityData) {
        try {
            if (entityData.getCode() == 200) {
                showMessage("发送成功");
            } else {
                showMessage(entityData.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick(R.id.btn_register)
    public void onViewClicked() {

        if (TextUtils.isEmpty(getName())) {
            showMessage("请输入姓名");
            return;
        }
        if (TextUtils.isEmpty(getPhone())) {
            showMessage("请输入手机号");
            return;
        }
        if (TextUtils.isEmpty(getVerificationCode())) {
            showMessage("请输入验证码");
            return;
        }
        if (TextUtils.isEmpty(getAccount())) {
            showMessage("请输入登录帐号");
            return;
        }
        if (TextUtils.isEmpty(getPwd())) {
            showMessage("请输入密码");
            return;
        }
        if (TextUtils.isEmpty(getPwdAgain())) {
            showMessage("请输入确认密码");
            return;
        }
        if (!getPwd().equals(getPwdAgain())) {
            showMessage("两次密码输入不一致");
            return;
        }

        getRegisterData();
    }

    private String getName() {
        return xetName.getText().toString();
    }

    private String getPhone() {
        return xetPhone.getText().toString();
    }

    private String getVerificationCode() {
        return xetVerificationCode.getText().toString();
    }

    private String getAccount() {
        return xetAccount.getText().toString();
    }

    private String getPwd() {
        return xetPwd.getText().toString();
    }

    private String getPwdAgain() {
        return xetPwdAgain.getText().toString();
    }

    private void getRegisterData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("UserName", getName());
        map.put("LoginName", getAccount());
        map.put("MobilePhone", getPhone());
        map.put("LoginPwd", getPwd());
        map.put("validateCode", getVerificationCode());
        mPresenter.getRegisterData(map);
    }

    @OnClick({R.id.cd_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cd_btn:
                onSendCodeViewClicked();
                break;

        }
    }

    public void onSendCodeViewClicked() {
        HashMap<String, String> map = new HashMap<>();
        map.put("mobilePhone", getPhone());
        mPresenter.registerSendValidateCode(map);

    }


}
