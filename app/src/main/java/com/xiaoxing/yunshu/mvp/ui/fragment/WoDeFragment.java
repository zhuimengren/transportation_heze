package com.xiaoxing.yunshu.mvp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jess.arms.base.BaseFragment;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerWoDeComponent;
import com.xiaoxing.yunshu.di.module.WoDeModule;
import com.xiaoxing.yunshu.mvp.contract.WoDeContract;
import com.xiaoxing.yunshu.mvp.presenter.WoDePresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.WoDe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class WoDeFragment extends BaseFragment<WoDePresenter> implements WoDeContract.View {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.ll_ge_ren_xin_xi)
    LinearLayout llGeRenXinXi;
    @BindView(R.id.ll_che_liang_xin_xi)
    LinearLayout llCheLiangXinXi;
    @BindView(R.id.ll_xiu_gai_mi_ma)
    LinearLayout llXiuGaiMiMa;

    public static WoDeFragment newInstance() {
        WoDeFragment fragment = new WoDeFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerWoDeComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .woDeModule(new WoDeModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wo_de, container, false);
        ToolbarUtils.initToolbarTitleNoBack(view, this, "我的");

        return view;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        //getWoDeData();
    }

    /**
     * 通过此方法可以使 Fragment 能够与外界做一些交互和通信, 比如说外部的 Activity 想让自己持有的某个 Fragment 对象执行一些方法,
     * 建议在有多个需要与外界交互的方法时, 统一传 {@link Message}, 通过 what 字段来区分不同的方法, 在 {@link #setData(Object)}
     * 方法中就可以 {@code switch} 做不同的操作, 这样就可以用统一的入口方法做多个不同的操作, 可以起到分发的作用
     * <p>
     * 调用此方法时请注意调用时 Fragment 的生命周期, 如果调用 {@link #setData(Object)} 方法时 {@link Fragment#onCreate(Bundle)} 还没执行
     * 但在 {@link #setData(Object)} 里却调用了 Presenter 的方法, 是会报空的, 因为 Dagger 注入是在 {@link Fragment#onCreate(Bundle)}
     * 方法中执行的
     * 然后才创建的 Presenter, 如果要做一些初始化操作,可以不必让外部调用 {@link #setData(Object)}, 在 {@link #initData(Bundle)} 中初始化就可以了
     *
     * <p>
     * Example usage:
     *
     * <pre>
     * public void setData(@Nullable Object data) {
     *     if (data != null && data instanceof Message) {
     *         switch (((Message) data).what) {
     *             case 0:
     *                 loadData(((Message) data).arg1);
     *                 break;
     *             case 1:
     *                 refreshUI();
     *                 break;
     *             default:
     *                 //do something
     *                 break;
     *         }
     *     }
     * }
     *
     * // call setData(Object):
     * Message data = new Message();
     * data.what = 0;
     * data.arg1 = 1;
     * fragment.setData(data);
     * </pre>
     *
     * @param data 当不需要参数时 {@code data} 可以为 {@code null}
     */
    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void getWoDeDataSuccess(WoDe entityData) {


    }

    private void getWoDeData() {
        HashMap<String, String> map = new HashMap<>();
        mPresenter.getWoDeData(map);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {

    }


    @OnClick({R.id.ll_ge_ren_xin_xi, R.id.ll_che_liang_xin_xi, R.id.ll_xiu_gai_mi_ma, R.id.ll_quan_xian_she_zhi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_ge_ren_xin_xi:
                Utils.navigation(getContext(), RouterHub.ACTIVITY_GE_REN_XIN_XI);
                break;
            case R.id.ll_che_liang_xin_xi:
                Utils.navigation(getContext(), RouterHub.ACTIVITY_CHE_LIANG_XIN_XI);
                break;
            case R.id.ll_xiu_gai_mi_ma:
                Utils.navigation(getContext(), RouterHub.ACTIVITY_XIU_GAI_MI_MA);
                break;
            case R.id.ll_quan_xian_she_zhi:
                Utils.navigation(getContext(), RouterHub.ACTIVITY_QUAN_XIAN_SHE_ZHI);
                break;
        }
    }



    @OnClick(R.id.btn_tui_chu_deng_lu)
    public void onViewClicked() {
        Utils.navigation(getContext(), RouterHub.APP_LOGIN_ACTIVITY);
    }
}
