package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerSheZhiComponent;
import com.xiaoxing.yunshu.di.module.SheZhiModule;
import com.xiaoxing.yunshu.mvp.contract.SheZhiContract;
import com.xiaoxing.yunshu.mvp.presenter.SheZhiPresenter;
import com.xw.repo.XEditText;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import com.jess.arms.base.BaseConstants;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.retrofiturlmanager.RetrofitUrlManager;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.APP_SHE_ZHI_ACTIVITY)
public class SheZhiActivity extends BaseActivity<SheZhiPresenter> implements SheZhiContract.View {


    @BindView(R.id.xet_she_zhi_ip)
    XEditText xetSheZhiIp;
    @BindView(R.id.xet_she_zhi_port)
    XEditText xetSheZhiPort;
    @BindView(R.id.btn_bao_cun_she_zhi)
    Button btnBaoCunSheZhi;


    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerSheZhiComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .sheZhiModule(new SheZhiModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_she_zhi; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.she_zhi_ip_port));

        xetSheZhiIp.setText(getSpIP());
        xetSheZhiPort.setText(getSpPort());

    }

    private String getSpPort() {
        return mSharedPreferencesHelper.getString(BaseConstants.SHE_ZHI_PORT);
    }

    private String getSpIP() {
        return mSharedPreferencesHelper.getString(BaseConstants.SHE_ZHI_IP);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }


    @OnClick({R.id.btn_bao_cun_she_zhi})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_bao_cun_she_zhi:
                if (TextUtils.isEmpty(getXetIP())) {
                    ArmsUtils.snackbarText(getString(R.string.ip_bu_neng_wei_kong));
                    return;
                }
                if (TextUtils.isEmpty(getXetPort())) {
                    ArmsUtils.snackbarText(getString(R.string.port_bu_neng_wei_kong));
                    return;
                }

                mSharedPreferencesHelper.putString(BaseConstants.SHE_ZHI_IP, getXetIP());
                mSharedPreferencesHelper.putString(BaseConstants.SHE_ZHI_PORT, getXetPort());
                RetrofitUrlManager.getInstance().setGlobalDomain(getUrl());
                ArmsUtils.snackbarText(getString(R.string.bao_cun_cheng_gong));
                killMyself();

                break;
        }
    }

    @NonNull
    private String getXetPort() {
        return xetSheZhiPort.getText().toString();
    }

    @NonNull
    private String getXetIP() {
        return xetSheZhiIp.getText().toString();
    }
}
