package com.xiaoxing.yunshu.mvp.ui.database;

import android.content.Context;
import android.database.Cursor;

public class SQLiteDBService {

	private SQLiteDBHelper dbHelper;

	public SQLiteDBService(Context context) {
		dbHelper = new SQLiteDBHelper(context);
	}

	// 升级数据库，重新建立数据库
	public void onUpgrade() {
		dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 1, 2);
	}

	// 建立数据库
	public void onCreateDB() {
		dbHelper.onCreate(dbHelper.getWritableDatabase());
	}

	public void dropTables(String tableName) {
		dbHelper.getWritableDatabase().execSQL(
				"Drop table IF EXISTS " + tableName);
	}

	// public void closeDB() {
	// dbHelper.getWritableDatabase().close();
	// }

	public void closeDBHelper() {
		if (null != dbHelper) {
			dbHelper.close();
		}

	}

	// 执行sql语句
	public void execSQL(String sql, Object[] objs) {
		dbHelper.getWritableDatabase().execSQL(sql, objs);
		dbHelper.close();
	}

	// 执行sql语句
	public void execSQLNotClose(String sql, Object[] objs) {
		dbHelper.getWritableDatabase().execSQL(sql, objs);
	}

	// 执行SQL语句
	public void execSQL(String sql) {
		dbHelper.getWritableDatabase().execSQL(sql);
		dbHelper.close();
	}

	// 返回读取器
	public Cursor query(String table, String[] columns, String selection,
                        String[] selectionArg, String groupBy, String having, String orderBy) {
		Cursor cr = dbHelper.getWritableDatabase().query(table, columns,
				selection, selectionArg, groupBy, having, orderBy);

		return cr;
	}

	public Cursor rawQuery(String sql) {

		Cursor cr = dbHelper.getWritableDatabase().rawQuery(sql, null);

		return cr;
	}

}
