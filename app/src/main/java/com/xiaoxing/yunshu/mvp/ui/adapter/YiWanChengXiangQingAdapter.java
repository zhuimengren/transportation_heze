package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.YiWanChengXiangQing;

import java.util.List;


public class YiWanChengXiangQingAdapter extends BaseQuickAdapter<YiWanChengXiangQing.DataBean, BaseViewHolder> {

    private Context mContext;

    public YiWanChengXiangQingAdapter(Context context, @Nullable List<YiWanChengXiangQing.DataBean> data) {
        super(R.layout.item_yi_wan_cheng_xiang_qing, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, YiWanChengXiangQing.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
    }

}
