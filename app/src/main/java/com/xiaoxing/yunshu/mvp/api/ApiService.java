package com.xiaoxing.yunshu.mvp.api;

import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXi;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXiAdd;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXiEdit;
import com.xiaoxing.yunshu.mvp.ui.entity.DeleteTruck;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgetPwdSendValidateCode;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgotPwd;
import com.xiaoxing.yunshu.mvp.ui.entity.GeRenXinXi;
import com.xiaoxing.yunshu.mvp.ui.entity.GetDistributingCount;
import com.xiaoxing.yunshu.mvp.ui.entity.GetTruckTypeList;
import com.xiaoxing.yunshu.mvp.ui.entity.GrabOrders;
import com.xiaoxing.yunshu.mvp.ui.entity.JieSuanDan;
import com.xiaoxing.yunshu.mvp.ui.entity.JinXingZhongXiangQing;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadedVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.Login;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersDaiQiangDan;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersJinXingZhong;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersYiWanCheng;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDan;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanDetail;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanSubmit;
import com.xiaoxing.yunshu.mvp.ui.entity.QuanXianSheZhi;
import com.xiaoxing.yunshu.mvp.ui.entity.Register;
import com.xiaoxing.yunshu.mvp.ui.entity.RegisterSendValidateCode;
import com.xiaoxing.yunshu.mvp.ui.entity.SendFinishVerifyNotice;
import com.xiaoxing.yunshu.mvp.ui.entity.StartDistibute;
import com.xiaoxing.yunshu.mvp.ui.entity.WoDe;
import com.xiaoxing.yunshu.mvp.ui.entity.XiuGaiMiMa;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPei;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPeiXiangQing;
import com.xiaoxing.yunshu.mvp.ui.entity.YiWanChengXiangQing;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface ApiService {

    /**
     * 登录
     *
     * @param user_login 用户名
     * @param user_pass  密码
     * @return
     */
    @GET("Account/Get")
    Observable<Login> login(@Query("loginName") String user_login, @Query("password") String user_pass);

    @Deprecated
    @GET("PSGrab/GetNewGrabList")
    Observable<GrabOrders> getGrabOrdersData(@QueryMap Map<String, String> map);

    /**
     * 抢单列表
     *
     * @param map
     * @return
     */
    @GET("PSGrab/GetNewGrabList")
    Observable<OrdersDaiQiangDan> getOrdersDaiQiangDanData(@QueryMap Map<String, String> map);

    /**
     * 进行中任务列表
     *
     * @param map startIndex 开始下标  endIndex  结束下标
     * @return
     */
    @GET("PSTaskBill/GetDistributingList")
    Observable<OrdersJinXingZhong> getOrdersJinXingZhongData(@QueryMap Map<String, String> map);

    /**
     * 已完成任务列表
     *
     * @param map startIndex 开始下标  endIndex  结束下标
     * @return
     */
    @GET("PSGrab/GetFinishedList")
    Observable<OrdersYiWanCheng> getOrdersYiWanChengData(@QueryMap Map<String, String> map);

    /**
     * @param map UserName：姓名 LoginName：登录帐号 MobilePhone：手机号 LoginPwd：密码 validateCode：验证码
     * @return
     */
    @FormUrlEncoded
    @POST("Account/Register")
    Observable<Register> getRegisterData(@FieldMap Map<String, String> map);

    @Deprecated
    Observable<QiangDan> getQiangDanData(Map<String, String> map);

    @Deprecated
    Observable<WoDe> getWoDeData(Map<String, String> map);

    /**
     * 忘记密码-修改密码
     *
     * @param map password：新密码  mobilePhone：手机号 validateCode：验证码
     * @return
     */
    @GET("Account/ModifyPassword")
    Observable<ForgotPwd> getForgotPwdData(@QueryMap Map<String, String> map);

    /**
     * 个人信息修改
     *
     * @param info {"IDCardNo":"身份证号","Birthday":"生日","Sex":"性别","DrivingAge":"驾龄"}
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("Account/ModifyDriver")
    Observable<GeRenXinXi> getGeRenXinXiData(@Body RequestBody info);

    /**
     * 车辆信息列表
     *
     * @param map
     * @return
     */
    @GET("Account/GetTruckList")
    Observable<CheLiangXinXi> getCheLiangXinXiList(@QueryMap Map<String, String> map);

    /**
     * 删除车辆信息
     *
     * @param map truckId：车辆信息Id
     * @return
     */
    @GET("Account/DeleteTruck")
    Observable<DeleteTruck> deleteTruck(@QueryMap Map<String, String> map);

    /**
     * 修改密码
     *
     * @param map oldPassword：旧密码 newPassword：新密码
     * @return
     */
    @GET("Account/ModifyPassword")
    Observable<XiuGaiMiMa> getXiuGaiMiMaData(@QueryMap Map<String, String> map);

    /**
     * 车辆新增
     *
     * @param info {"TruckNumber":"车牌号","TruckType":"车辆类型","LoadWeight":"载重","LoadLength":"车厢长","LoadWidth":"车厢宽","LoadHeight":"车厢高","TruckBelongTo":"车辆所属（个人或公司）","BelongToOrg":"所属企业信息","IsDefault":"是否默认"}
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("Account/AddTruck")
    Observable<CheLiangXinXiAdd> getCheLiangXinXiAddData(@Body RequestBody info);

    /**
     * 车辆类型列表
     *
     * @param map
     * @return
     */
    @GET("Account/GetTruckTypeList")
    Observable<GetTruckTypeList> getTruckTypeList(@QueryMap Map<String, String> map);

    /**
     * 车辆信息修改
     *
     * @param info {"TruckNumber":"车牌号","TruckType":"车辆类型","LoadWeight":"载重","LoadLength":"车厢长","LoadWidth":"车厢宽","LoadHeight":"车厢高","TruckBelongTo":"车辆所属（个人或公司）","BelongToOrg":"所属企业信息","IsDefault":"是否默认","TruckId":"车辆信息Id"}
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("Account/ModifyTruck")
    Observable<CheLiangXinXiEdit> getCheLiangXinXiEditData(@Body RequestBody info);

    /**
     * 忘记密码-发送验证码
     *
     * @param map mobilePhone  手机号码
     * @return
     */
    @GET("Account/ForgetPwdSendValidateCode")
    Observable<ForgetPwdSendValidateCode> forgetPwdSendValidateCode(@QueryMap Map<String, String> map);

    /**
     * 注册-发送验证码
     *
     * @param map mobilePhone：手机号
     * @return
     */
    @GET("Account/RegisterSendValidateCode")
    Observable<RegisterSendValidateCode> registerSendValidateCode(@QueryMap Map<String, String> map);

    /**
     * 抢单详情
     *
     * @param map grabNo：单号
     * @return
     */
    @GET("PSGrab/GetDetail")
    Observable<QiangDanDetail> getQiangDanDetailList(@QueryMap Map<String, String> map);

    /**
     * 抢单提交
     *
     * @param map grabNo：单号 givePrice：价格 remark：备注
     * @return
     */
    @POST("PSGrab/AddGrabItem")
    Observable<QiangDanSubmit> getQiangDanSubmitData(@QueryMap Map<String, String> map);

    /**
     * 已分配列表
     *
     * @param map startIndex 开始下标  endIndex  结束下标
     * @return
     */
    @GET("PSGrab/GetDistributeList")
    Observable<YiFenPei> getYiFenPeiData(@QueryMap Map<String, String> map);

    /**
     * 已分配详情列表
     *
     * @param map grabNo：单号
     * @return
     */
    @GET("PSGrab/GetDetail")
    Observable<YiFenPeiXiangQing> getYiFenPeiXiangQingList(@QueryMap Map<String, String> map);

    /**
     * 进行中详情
     *
     * @param map TaskNo：任务号
     * @return
     */
    @GET("PSTaskBill/GetDetail")
    Observable<JinXingZhongXiangQing> getJinXingZhongXiangQingList(@QueryMap Map<String, String> map);

    /**
     * 扫描物料二维码核对物料（此方法菏泽用不到，菏泽只要装车拍照就可以，其他核对不要）
     *
     * @param map taskNo：任务号 materialName：物料名称 materialId：物料Id
     * @return
     */
    @GET("PSTaskBill/LoadScanVerify")
    Observable<LoadScanVerify> loadScanVerify(@QueryMap Map<String, String> map);


    /**
     * 每个物料拍照（暂时没用到）
     *
     * @param info {"TaskNo":"单号","TaskItemId":" 任务Id","PotoExtension":"jpg","PotoBase64Code":" 照片Base64编码数据"}
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("PSTaskBill/LoadMaterialPhotoVerify")
    Observable<LoadMaterialPhotoVerify> loadMaterialPhotoVerify(@Body RequestBody info);

    /**
     * 装车照片上传
     *
     * @param info {"TaskNo":"单号","TaskItemId":" 任务Id","PotoExtension":"jpg","PotoBase64Code":" 照片Base64编码数据"}
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("PSTaskBill/LoadPhotoVerify")
    Observable<LoadPhotoVerify> loadPhotoVerify(@Body RequestBody info);

    /**
     * 核对（核对完物料，上传完装车照片之后，在进行一次核对操作。此操作在菏泽用不到手动操作）
     *
     * @param map taskNo：单号 addrLng：地址经度 addrLat：地址纬度
     * @return
     */
    @GET("PSTaskBill/LoadedVerify")
    Observable<LoadedVerify> loadedVerify(@QueryMap Map<String, String> map);

    /**
     * 开始配送
     *
     * @param map grabNo：单号
     * @return
     */
    @GET("PSGrab/StartDistibute")
    Observable<StartDistibute> startDistibute(@QueryMap Map<String, String> map);


    /**
     * 结束配送-扫描物料二维码核对物料 （此方法菏泽用不到，菏泽只要装车拍照就可以，其他核对不要）
     *
     * @param map taskNo：任务号 materialName：物料名称： materialId：物料Id
     * @return
     */
    @GET("PSTaskBill/FinishScanVerify")
    Observable<FinishScanVerify> finishScanVerify(@QueryMap Map<String, String> map);

    /**
     * 结束配送-物料拍照（暂时没用到）
     *
     * @param info
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("PSTaskBill/FinishMaterialPhotoVerify")
    Observable<FinishMaterialPhotoVerify> finishMaterialPhotoVerify(@Body RequestBody info);

    /**
     * 卸车照片上传
     *
     * @param info {"TaskNo":"单号","TaskItemId":" 任务Id 0","PotoExtension":"jpg","PotoBase64Code":" 照片Base64编码数据"}
     * @return
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})//需要添加头
    @POST("PSTaskBill/FinishPhotoVerify")
    Observable<FinishPhotoVerify> finishPhotoVerify(@Body RequestBody info);

    /**
     * 结束配送
     *
     * @param map taskNo：任务号  validateCode：验证码 addrLng：地址经度 addrLat：地址纬度 miles：公里 weight：重量（吨） loadMode：装卸方式（吊车） loadNum：数量 price：价格
     * @return
     */
    @GET("PSTaskBill/FinishVerify")
    Observable<FinishVerify> finishVerify(@QueryMap Map<String, String> map);

    /**
     * 已完成-详情
     *
     * @param map grabNo：单号
     * @return
     */
    @GET("PSGrab/GetDetail")
    Observable<YiWanChengXiangQing> getYiWanChengXiangQingList(@QueryMap Map<String, String> map);

    /**
     * 结束配送-发送验证码（验证码找不到的情况下，可以重新发送验证码）
     *
     * @param map taskNo：任务号
     * @return
     */
    @GET("PSTaskBill/SendFinishVerifyNotice")
    Observable<SendFinishVerifyNotice> sendFinishVerifyNotice(@QueryMap Map<String, String> map);

    @Deprecated
    Observable<QuanXianSheZhi> getQuanXianSheZhiData(Map<String, String> map);

    /**
     * 结算单详情
     *
     * @param map grabNo：单号
     * @return
     */
    @GET("PSStatement/GetDetail")
    Observable<JieSuanDan> getJieSuanDanData(@QueryMap Map<String, String> map);

    /**
     * 查询是否有进行中的任务（有可能APP会退出重启，在进入APP时获取服务器是否有进行中的任务，如果有就开启搜集轨迹服务开始搜集）
     *
     * @param map
     * @return
     */
    @GET("PSTaskBill/GetDistributingCount")
    Observable<GetDistributingCount> getDistributingCount(@QueryMap Map<String, String> map);
}
