package com.xiaoxing.yunshu.mvp.ui.entity;

import java.io.Serializable;

public class UserInfo implements Serializable {


    /**
     * MachineUserId : 0
     * UserID : 3
     * DeptID : 100
     * DisplayName : Test
     * LoginName : Test
     * Password : 000000
     * AddTime : /Date(1312989287000)/
     * AddUserID : 0
     * State : 1
     * Remark : 18561785270
     * RegisterUserID : 2
     * user_money : null
     */

    private int MachineUserId;
    private int UserID;
    private int DeptID;
    private String DisplayName;
    private String LoginName;
    private String Password;
    private String AddTime;
    private int AddUserID;
    private String State;
    private String Remark;
    private int RegisterUserID;
    private Object user_money;

    public int getMachineUserId() {
        return MachineUserId;
    }

    public void setMachineUserId(int MachineUserId) {
        this.MachineUserId = MachineUserId;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    public int getDeptID() {
        return DeptID;
    }

    public void setDeptID(int DeptID) {
        this.DeptID = DeptID;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }

    public String getLoginName() {
        return LoginName;
    }

    public void setLoginName(String LoginName) {
        this.LoginName = LoginName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getAddTime() {
        return AddTime;
    }

    public void setAddTime(String AddTime) {
        this.AddTime = AddTime;
    }

    public int getAddUserID() {
        return AddUserID;
    }

    public void setAddUserID(int AddUserID) {
        this.AddUserID = AddUserID;
    }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String Remark) {
        this.Remark = Remark;
    }

    public int getRegisterUserID() {
        return RegisterUserID;
    }

    public void setRegisterUserID(int RegisterUserID) {
        this.RegisterUserID = RegisterUserID;
    }

    public Object getUser_money() {
        return user_money;
    }

    public void setUser_money(Object user_money) {
        this.user_money = user_money;
    }
}
