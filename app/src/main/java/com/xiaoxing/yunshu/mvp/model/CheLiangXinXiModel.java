package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXi;
import com.xiaoxing.yunshu.mvp.ui.entity.DeleteTruck;

import io.reactivex.Observable;


@ActivityScope
public class CheLiangXinXiModel extends BaseModel implements CheLiangXinXiContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public CheLiangXinXiModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<CheLiangXinXi> getCheLiangXinXiList(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getCheLiangXinXiList(map);

    }

    @Override
    public Observable<DeleteTruck> deleteTruck(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).deleteTruck(map);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}