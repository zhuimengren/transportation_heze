package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Poi;
import com.amap.api.navi.AmapNaviPage;
import com.amap.api.navi.AmapNaviParams;
import com.amap.api.navi.INaviInfoCallback;
import com.amap.api.navi.model.AMapNaviLocation;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hss01248.frescopicker.FrescoIniter;
import com.hss01248.photoouter.PhotoCallback;
import com.hss01248.photoouter.PhotoUtil;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.jess.arms.utils.LogUtils;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerJinXingZhongXiangQingComponent;
import com.xiaoxing.yunshu.di.module.JinXingZhongXiangQingModule;
import com.xiaoxing.yunshu.mvp.contract.JinXingZhongXiangQingContract;
import com.xiaoxing.yunshu.mvp.presenter.JinXingZhongXiangQingPresenter;
import com.xiaoxing.yunshu.mvp.ui.adapter.JinXingZhongXiangQingAdapter;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.JinXingZhongXiangQing;
import com.xiaoxing.yunshu.mvp.ui.entity.SendFinishVerifyNotice;
import com.xiaoxing.yunshu.mvp.ui.track.TrackApplication;
import com.xw.repo.XEditText;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.iwf.photopicker.utils.ProxyTools;
import me.jessyan.armscomponent.commonres.utils.BottomDialogUtil;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.RecycleViewDivider;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;

import static com.jess.arms.base.BaseConstants.SCAN_REQUEST_CODE;
import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_JIN_XING_ZHONG_XIANG_QING)
public class JinXingZhongXiangQingActivity extends BaseActivity<JinXingZhongXiangQingPresenter> implements JinXingZhongXiangQingContract.View, OnRefreshListener, JinXingZhongXiangQingAdapter.IJinXingZHongXiangQingInterface, INaviInfoCallback {


    private static boolean mIsNeedDemo = true;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.empty_image)
    ImageView empty_image;
    @BindView(R.id.empty)
    View mEmptyLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    RefreshLayout mRefreshLayout;
    @BindView(R.id.btn_jie_shu_pei_song)
    Button btnJieShuPeiSong;
    @BindView(R.id.btn_xie_che_pai_zhao)
    Button btnXieChePaiZhao;
    @BindView(R.id.btn_he_dui)
    Button btnHeDui;
    LatLng p1 = new LatLng(36.074670, 120.377450);//深业中心大厦
    LatLng p2 = new LatLng(36.264680, 120.033382);//胶州
    LatLng p3 = new LatLng(36.706860, 119.161760);//潍坊
    LatLng p4 = new LatLng(36.553580, 116.75199);//济南
    LatLng p5 = new LatLng(36.457020, 115.98549);//聊城
    private JinXingZhongXiangQingAdapter mAdapter;
    private List<JinXingZhongXiangQing.DataBean.ItemsBean> mDataBeanList = new ArrayList<>();
    private String mTaskNo;
    private String mScanTaskNo;
    private String mMaterialName;
    private BottomDialogUtil mBottomDialogUtilShare;
    private BottomDialogUtil mBottomDialogUtilFinishVerify;
    private int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;
    private List<Poi> poiList;
    private TrackApplication trackApp = null;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerJinXingZhongXiangQingComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .jinXingZhongXiangQingModule(new JinXingZhongXiangQingModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_jin_xing_zhong_xiang_qing; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_jin_xing_zhong_xiang_qing));
        trackApp = (TrackApplication) getApplicationContext();

        PhotoUtil.init(this, new FrescoIniter());//第二个参数根据具体依赖库而定
        poiList = new ArrayList<>();

        mTaskNo = getIntent().getExtras().getString("TaskNo");
        initRefreshLayout();
        initRecyclerView();
        initEmpty();
    }

    private void initRefreshLayout() {
        mRefreshLayout.setRefreshHeader(new
                ClassicsHeader(this).setSpinnerStyle(SpinnerStyle.FixedBehind).setPrimaryColorId(R.color.public_colorPrimary).setAccentColorId(android.R.color.white));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.autoRefresh();
        mRefreshLayout.setEnableLoadMore(false);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(JinXingZhongXiangQingActivity.this));
        mRecyclerView.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, 5, Color.parseColor("#EEEEEE")));

        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter = new JinXingZhongXiangQingAdapter(JinXingZhongXiangQingActivity.this, mDataBeanList));

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            }
        });
        mAdapter.setCheckInterface(this);
    }

    private void initEmpty() {
        empty_image.setImageResource(R.drawable.ic_empty);
        empty_text.setTextColor(Color.parseColor("#CFCFCF"));
        empty_text.setText("暂无数据下拉刷新");
    }

    private List<JinXingZhongXiangQing.DataBean> loadModels() {
        List<JinXingZhongXiangQing.DataBean> lists = new ArrayList<>();

        for (int i = 0; i < 5; i++) {

            JinXingZhongXiangQing.DataBean dataBean = new JinXingZhongXiangQing.DataBean();
            lists.add(dataBean);
        }

        return lists;
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        getJinXingZhongXiangQingList();
        mRefreshLayout.finishRefresh();

    }

    private void getJinXingZhongXiangQingList() {
        HashMap<String, String> map = new HashMap<>();
        map.put("TaskNo", mTaskNo);
        mPresenter.getJinXingZhongXiangQingList(map);
    }

    @Override
    public void getJinXingZhongXiangQingDataSuccess(JinXingZhongXiangQing entityList) {

        if (entityList != null && entityList.getData() != null && entityList.getData().getItems().size() > 0) {

            if (!TextUtils.isEmpty(entityList.getData().getDeliverAddrLat()) && !TextUtils.isEmpty(entityList.getData().getDeliverAddrLng())) {
                LatLng latLng = new LatLng(Double.parseDouble(entityList.getData().getDeliverAddrLat()), Double.parseDouble(entityList.getData().getDeliverAddrLng()));//聊城
                poiList.add(new Poi(entityList.getData().getDeliverAddress(), latLng, ""));
            }
            boolean isIsFromOutStorage = entityList.getData().isIsFromOutStorage();

            List<JinXingZhongXiangQing.DataBean.ItemsBean> itemsBeans = entityList.getData().getItems();

            for (int i = 0; i < itemsBeans.size(); i++) {
                if (isIsFromOutStorage) {
                    itemsBeans.get(i).setFromOutStorage(true);
                } else {
                    itemsBeans.get(i).setFromOutStorage(false);
                }
            }

            mDataBeanList.clear();
            mDataBeanList.addAll(entityList.getData().getItems());
            mAdapter.notifyDataSetChanged();
            mEmptyLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);


            if (!TextUtils.isEmpty(entityList.getData().getFinishPhotoId()) && (Integer.parseInt(entityList.getData().getFinishPhotoId()) > 0)) {
                btnXieChePaiZhao.setBackground(getResources().getDrawable(R.drawable.public_shape_gray_bg_corner_5dp));
                btnXieChePaiZhao.setText("卸车拍照(已拍照)");
            } else {
                btnXieChePaiZhao.setBackground(getResources().getDrawable(R.drawable.public_shape_blue_bg_corner_5dp));
                btnXieChePaiZhao.setText("卸车拍照");
            }

        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        }

    }

    @Override
    public void finishScanVerifySuccess(FinishScanVerify entityList) {

        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {
            getJinXingZhongXiangQingList();
        }

    }

    @Override
    public void finishMaterialPhotoVerifySuccess(FinishMaterialPhotoVerify entityList) {
        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {
            getJinXingZhongXiangQingList();
        }
    }

    @Override
    public void finishPhotoVerifySuccess(FinishPhotoVerify entityList) {
        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {
            btnXieChePaiZhao.setText("卸车拍照(已拍照)");
            btnXieChePaiZhao.setBackground(getResources().getDrawable(R.drawable.public_shape_gray_bg_corner_5dp));
            getJinXingZhongXiangQingList();
        }
    }

    @Override
    public void finishVerifySuccess(FinishVerify entityList) {
        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) { //有多个单子的时候，200是完成部分单子
            if (mBottomDialogUtilFinishVerify != null)
                mBottomDialogUtilFinishVerify.dismissDiaolog();
            killMyself();
        } else if (entityList.getCode() == 300) { //有多个单子的时候，300是所有的单子任务都完成
            EventBus.getDefault().post("", "endYunShu");
            EventBus.getDefault().post(2, "setPeiSongCurrentItem");
            EventBus.getDefault().post("", "updateOrdersYiWanChengData");
            EventBus.getDefault().post("", "updateOrdersJinXingZhongData");
            if (mBottomDialogUtilFinishVerify != null)
                mBottomDialogUtilFinishVerify.dismissDiaolog();
            killMyself();
        }
    }

    @Override
    public void sendFinishVerifyNoticeSuccess(SendFinishVerifyNotice entity) {

        if (entity.getCode() == 200) {
            showMessage(entity.getMessage());
        }
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);

    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @Override
    public void saoMiaoErWeiMa(JinXingZhongXiangQing.DataBean.ItemsBean itemsBean) {
        mScanTaskNo = itemsBean.getTaskNo();
        mMaterialName = itemsBean.getMaterialName();
        toScanQRCodeActivity();
    }

    @Override
    public void paiZhao(JinXingZhongXiangQing.DataBean.ItemsBean itemsBean) {
        paiZhaoShangChuan(itemsBean);

    }

    private void paiZhaoShangChuan(JinXingZhongXiangQing.DataBean.ItemsBean itemsBean) {
        LinearLayout root = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.sales_client_bottom_dialog_pai_zhao, null);
        Button btn_xiang_ce = root.findViewById(me.iwf.photopicker.R.id.btn_xiang_ce);
        Button ben_xiang_ji = root.findViewById(me.iwf.photopicker.R.id.ben_xiang_ji);
        Button btn_qu_xiao = root.findViewById(me.iwf.photopicker.R.id.btn_qu_xiao);
        btn_qu_xiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();
            }
        });
        btn_xiang_ce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.begin()
                        .setNeedCropWhenOne(true)
                        .setNeedCompress(true)
                        .setMaxSelectCount(1)
                        .setCropMuskOval()
                        .setSelectGif()
                        /*.setFromCamera(false)
                        .setMaxSelectCount(5)
                        .setNeedCropWhenOne(false)
                        .setNeedCompress(true)
                        .setCropRatio(16,9)*/
                        .start(JinXingZhongXiangQingActivity.this, 33, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));
                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);

                                    String jsonData = "{\"TaskNo\":\"" + itemsBean.getTaskNo() + "\",\"TaskItemId\":" + itemsBean.getItemId() + ",\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.finishMaterialPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
//                                refresh1(originalPaths);
                            }

                            @Override
                            public void onCancel(int requestCode) {
                            }
                        }));
            }
        });
        ben_xiang_ji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.cropAvatar(true)
                        .start(JinXingZhongXiangQingActivity.this, 23, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));

                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);

                                    String jsonData = "{\"TaskNo\":\"" + itemsBean.getTaskNo() + "\",\"TaskItemId\":" + itemsBean.getItemId() + ",\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.finishMaterialPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
                            }

                            @Override
                            public void onCancel(int requestCode) {
                            }
                        }));
            }
        });

        mBottomDialogUtilShare = new BottomDialogUtil(JinXingZhongXiangQingActivity.this, root);

        mBottomDialogUtilShare.showDiaolog();


    }

    private void toScanQRCodeActivity() {
        Intent newIntent = new Intent(this, ScanActivity.class);
        mSharedPreferencesHelper.putInt(SCAN_REQUEST_CODE, 101);
        // 开始一个新的 Activity等候返回结果
        startActivityForResult(newIntent, 101);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 101) {
            Bundle bundle = data.getExtras();
            String mid = bundle.getString("scan_result");
//            setIsScan(mid);
            if (!TextUtils.isEmpty(mid)) {

                HashMap<String, String> map = new HashMap<>();
                map.put("taskNo", mScanTaskNo);
                map.put("materialName", mMaterialName);
                map.put("materialId", mid);

                LogUtils.debugEInfo("taskNo=" + mScanTaskNo + "   materialName=" + mMaterialName + " materialId=" + mid);
                mPresenter.finishScanVerify(map);
            }
        } else {
            //onActivityResult里一行代码回调
            if (resultCode != 0)
                PhotoUtil.onActivityResult(this, requestCode, resultCode, data);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

    @OnClick({R.id.btn_xie_che_pai_zhao, R.id.btn_he_dui, R.id.btn_jie_shu_pei_song, R.id.btn_dao_hang, R.id.btn_yan_zheng_ma})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_xie_che_pai_zhao:
                zhuangCheTuPian();
                break;
            case R.id.btn_he_dui:
                showEditTextDialog();
                break;
            case R.id.btn_yan_zheng_ma:
                HashMap<String, String> map = new HashMap<>();
                map.put("taskNo", mTaskNo);
                mPresenter.sendFinishVerifyNotice(map);
                break;
            case R.id.btn_jie_shu_pei_song:
                if (ArmsUtils.isNetworkAvailable(this)) {
//                    showEditTextDialog();
                    showFinishVerify();
                } else {
                    showMessage("当前网络不可用");
                }
                break;
            case R.id.btn_dao_hang:

//                List<Poi> poiList = new ArrayList();
//                poiList.add(new Poi("聊城", p5, ""));
//                poiList.add(new Poi("胶州", p2, ""));
//                poiList.add(new Poi("潍坊", p3, ""));

//                List<Poi> poiList = mSharedPreferencesHelper.getListData("poiList", Poi.class);

                try {
                    if (poiList.size() > 0) {
//                        AmapNaviParams params = new AmapNaviParams(poiList.get(0), poiList, poiList.get(poiList.size() - 1), AmapNaviType.DRIVER);
                        AmapNaviParams params = new AmapNaviParams(poiList.get(0));
                        params.setUseInnerVoice(true);
                        AmapNaviPage.getInstance().showRouteActivity(getApplicationContext(), params, this);
                    } else {
                        showMessage("目的地址不明确");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void zhuangCheTuPian() {
        LinearLayout root = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.sales_client_bottom_dialog_pai_zhao, null);
        Button btn_xiang_ce = root.findViewById(me.iwf.photopicker.R.id.btn_xiang_ce);
        Button ben_xiang_ji = root.findViewById(me.iwf.photopicker.R.id.ben_xiang_ji);
        Button btn_qu_xiao = root.findViewById(me.iwf.photopicker.R.id.btn_qu_xiao);
        btn_qu_xiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();
            }
        });
        btn_xiang_ce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.begin()
                        .setNeedCropWhenOne(true)
                        .setNeedCompress(true)
                        .setMaxSelectCount(1)
                        .setCropMuskOval()
                        .setSelectGif()
                        /*.setFromCamera(false)
                        .setMaxSelectCount(5)
                        .setNeedCropWhenOne(false)
                        .setNeedCompress(true)
                        .setCropRatio(16,9)*/
                        .start(JinXingZhongXiangQingActivity.this, 33, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));
                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);

                                    String jsonData = "{\"TaskNo\":\"" + mTaskNo + "\",\"TaskItemId\":0,\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.finishPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
//                                refresh1(originalPaths);
                            }

                            @Override
                            public void onCancel(int requestCode) {
                            }
                        }));
            }
        });
        ben_xiang_ji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.cropAvatar(true)
                        .start(JinXingZhongXiangQingActivity.this, 23, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));

                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);

                                    String jsonData = "{\"TaskNo\":\"" + mTaskNo + "\",\"TaskItemId\":0,\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.finishPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
                            }

                            @Override
                            public void onCancel(int requestCode) {
                            }
                        }));
            }
        });

        mBottomDialogUtilShare = new BottomDialogUtil(JinXingZhongXiangQingActivity.this, root);

        mBottomDialogUtilShare.showDiaolog();


    }

    private void showEditTextDialog() {
        final QMUIDialog.EditTextDialogBuilder builder = new QMUIDialog.EditTextDialogBuilder(this);
        builder.setTitle("收货验证码")
                .setPlaceholder("在此输入收货验证码")
                .setInputType(InputType.TYPE_CLASS_TEXT)
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        CharSequence text = builder.getEditText().getText();
                        if (text != null && text.length() > 0) {

                            try {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("taskNo", mTaskNo);
                                map.put("validateCode", text.toString());
                                map.put("addrLng", String.valueOf(mSharedPreferencesHelper.getString("longitude")));
                                map.put("addrLat", String.valueOf(mSharedPreferencesHelper.getString("latitude")));
                                mPresenter.finishVerify(map);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        } else {
                            Toast.makeText(JinXingZhongXiangQingActivity.this, "请填入收货验证码", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .create(mCurrentDialogStyle).show();
    }

    private void showFinishVerify() {
        LinearLayout root = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.bottom_dialog_finish_verify, null);
        XEditText xet_shou_huo_yan_zheng_ma = root.findViewById(R.id.xet_shou_huo_yan_zheng_ma);
        XEditText xet_gong_li_shu = root.findViewById(R.id.xet_gong_li_shu);
        XEditText xet_zhong_liang = root.findViewById(R.id.xet_zhong_liang);
        XEditText xet_zhuang_xie_fang_shi = root.findViewById(R.id.xet_zhuang_xie_fang_shi);
        XEditText xet_shu_liang = root.findViewById(R.id.xet_shu_liang);
        XEditText xet_jia_ge = root.findViewById(R.id.xet_jia_ge);
        Button btn_que_ding = root.findViewById(R.id.btn_que_ding);
        Button btn_qu_xiao = root.findViewById(R.id.btn_qu_xiao);
        btn_que_ding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (TextUtils.isEmpty(getShouHuoYanZhengMa())) {
                        showMessage("请输入收货验证码");
                        return;
                    }
                    if (TextUtils.isEmpty(getGongLiShu())) {
                        showMessage("请输入公里数");
                        return;
                    }

                    if (TextUtils.isEmpty(getZhongLiang())) {
                        showMessage("请输入吨数");
                        return;
                    }

                    if (TextUtils.isEmpty(getxetZhuangXieFangShi())) {
                        showMessage("请输入装卸方式(吊车)");
                        return;
                    }
                    if (TextUtils.isEmpty(getxetShuLiang())) {
                        showMessage("请输入数量");
                        return;
                    }
                    if (TextUtils.isEmpty(getxetJiaGe())) {
                        showMessage("请输入价格");
                        return;
                    }

                    HashMap<String, String> map = new HashMap<>();
                    map.put("taskNo", mTaskNo);
                    map.put("validateCode", getShouHuoYanZhengMa());
                    map.put("addrLng", String.valueOf(mSharedPreferencesHelper.getString("longitude")));
                    map.put("addrLat", String.valueOf(mSharedPreferencesHelper.getString("latitude")));
                    map.put("miles", getGongLiShu());
                    map.put("weight", getZhongLiang());
                    map.put("loadMode", getxetZhuangXieFangShi());
                    map.put("loadNum", getxetShuLiang());
                    map.put("price", getxetJiaGe());
                    mPresenter.finishVerify(map);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @NonNull
            private String getShouHuoYanZhengMa() {
                return xet_shou_huo_yan_zheng_ma.getText().toString().trim();
            }

            @NonNull
            private String getGongLiShu() {
                return xet_gong_li_shu.getText().toString().trim();
            }

            @NonNull
            private String getZhongLiang() {
                return xet_zhong_liang.getText().toString().trim();
            }

            @NonNull
            private String getxetZhuangXieFangShi() {
                return xet_zhuang_xie_fang_shi.getText().toString().trim();
            }

            @NonNull
            private String getxetShuLiang() {
                return xet_shu_liang.getText().toString().trim();
            }

            @NonNull
            private String getxetJiaGe() {
                return xet_jia_ge.getText().toString().trim();
            }
        });
        btn_qu_xiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomDialogUtilFinishVerify != null)
                    mBottomDialogUtilFinishVerify.dismissDiaolog();
            }
        });
        mBottomDialogUtilFinishVerify = new BottomDialogUtil(JinXingZhongXiangQingActivity.this, root);

        mBottomDialogUtilFinishVerify.showDiaolog();

    }

    @Override
    public void onInitNaviFailure() {

    }

    @Override
    public void onGetNavigationText(String s) {

    }

    @Override
    public void onLocationChange(AMapNaviLocation aMapNaviLocation) {

    }

    @Override
    public void onArriveDestination(boolean b) {

    }

    @Override
    public void onStartNavi(int i) {

    }

    @Override
    public void onCalculateRouteSuccess(int[] ints) {

    }

    @Override
    public void onCalculateRouteFailure(int i) {

    }

    @Override
    public void onStopSpeaking() {

    }

    @Override
    public void onReCalculateRoute(int i) {

    }

    @Override
    public void onExitPage(int i) {

    }

    @Override
    public void onStrategyChanged(int i) {

    }

    @Override
    public View getCustomNaviBottomView() {
        return null;
    }

    @Override
    public View getCustomNaviView() {
        return null;
    }

    @Override
    public void onArrivedWayPoint(int i) {

    }
}
