package com.xiaoxing.yunshu.mvp.presenter;

import android.app.Application;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;
import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiEditContract;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXiEdit;
import com.xiaoxing.yunshu.mvp.ui.entity.GetTruckTypeList;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import okhttp3.RequestBody;


@ActivityScope
public class CheLiangXinXiEditPresenter extends BasePresenter<CheLiangXinXiEditContract.Model, CheLiangXinXiEditContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public CheLiangXinXiEditPresenter(CheLiangXinXiEditContract.Model model, CheLiangXinXiEditContract.View rootView) {
        super(model, rootView);
    }

    public void getCheLiangXinXiEditData(RequestBody info) {

        mModel.getCheLiangXinXiEditData(info).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<CheLiangXinXiEdit>(mErrorHandler) {
                    @Override
                    public void onNext(CheLiangXinXiEdit entityData) {
                        mRootView.getCheLiangXinXiEditDataSuccess(entityData);
                    }
                });
    }

    public void getTruckTypeList(Map<String, String> map) {

        mModel.getTruckTypeList(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<GetTruckTypeList>(mErrorHandler) {
                    @Override
                    public void onNext(GetTruckTypeList entityData) {
                        mRootView.getTruckTypeListSuccess(entityData);
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
