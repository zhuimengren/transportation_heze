package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class JinXingZhongXiangQing {


    /**
     * Data : {"TaskNo":"PT202002050005","StartDateTime":"2020-02-14 17:06:15","EndDateTime":null,"TruckId":3,"TruckNumber":"1111","DriverId":180,"DriverName":"xx","DriverPhone":"15965561796","DeliverAddress":"1","DeliverAddrLat":null,"DeliverAddrLng":null,"GetAddress":"延吉路175号南京路仓库","GetAddrLat":null,"GetAddrLng":null,"OutStoreId":44,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":6,"DistributeStatusDesc":"配送中","IsFromOutStorage":true,"GrabNo":"PG202002060001","LoadPhotoId":493,"FinishPhotoId":null,"InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-05 12:12:23","ModifyUserId":null,"ModifyUserName":null,"ModifyDateTime":null,"OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null},"Grab":null,"Items":[{"ItemId":14,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":2000,"MaterialUnit":"件","IsStartVerify":true,"StartVerifyPhotoId":null,"IsEndVerify":false,"EndVerifyPhotoId":null,"Main":null},{"ItemId":15,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,50mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":3000,"MaterialUnit":"件","IsStartVerify":true,"StartVerifyPhotoId":null,"IsEndVerify":false,"EndVerifyPhotoId":null,"Main":null}],"MItems":null}
     * Code : 200
     * Message : 获取配送单详细信息成功！
     */

    private DataBean Data;
    private int Code;
    private String Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * TaskNo : PT202002050005
         * StartDateTime : 2020-02-14 17:06:15
         * EndDateTime : null
         * TruckId : 3
         * TruckNumber : 1111
         * DriverId : 180
         * DriverName : xx
         * DriverPhone : 15965561796
         * DeliverAddress : 1
         * DeliverAddrLat : null
         * DeliverAddrLng : null
         * GetAddress : 延吉路175号南京路仓库
         * GetAddrLat : null
         * GetAddrLng : null
         * OutStoreId : 44
         * ConsigneeName :  1
         * ConsigneePhone : 1
         * DistributeStatus : 6
         * DistributeStatusDesc : 配送中
         * IsFromOutStorage : true
         * GrabNo : PG202002060001
         * LoadPhotoId : 493
         * FinishPhotoId : null
         * InsertUserId : 174
         * InsertUserName : 配送管理员
         * InsertDateTime : 2020-02-05 12:12:23
         * ModifyUserId : null
         * ModifyUserName : null
         * ModifyDateTime : null
         * OutStore : {"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null}
         * Grab : null
         * Items : [{"ItemId":14,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":2000,"MaterialUnit":"件","IsStartVerify":true,"StartVerifyPhotoId":null,"IsEndVerify":false,"EndVerifyPhotoId":null,"Main":null},{"ItemId":15,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,50mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":3000,"MaterialUnit":"件","IsStartVerify":true,"StartVerifyPhotoId":null,"IsEndVerify":false,"EndVerifyPhotoId":null,"Main":null}]
         * MItems : null
         */

        private String TaskNo;
        private String StartDateTime;
        private String EndDateTime;
        private String DeliverCity;
        private String GetCity;
        private int TruckId;
        private String TruckNumber;
        private int DriverId;
        private String DriverName;
        private String DriverPhone;
        private String DeliverAddress;
        private String DeliverAddrLat;
        private String DeliverAddrLng;
        private String GetAddress;
        private String GetAddrLat;
        private String GetAddrLng;
        private int OutStoreId;
        private String ConsigneeName;
        private String ConsigneePhone;
        private int DistributeStatus;
        private String DistributeStatusDesc;
        private boolean IsFromOutStorage;
        private String GrabNo;
        private int LoadPhotoId;
        private String FinishPhotoId;
        private int InsertUserId;
        private String InsertUserName;
        private String InsertDateTime;
        private String ModifyUserId;
        private String ModifyUserName;
        private String ModifyDateTime;
        private OutStoreBean OutStore;
        private String Grab;
        private String MItems;
        private List<ItemsBean> Items;

        public String getGetCity() {
            return GetCity == null ? "" : GetCity;
        }

        public void setGetCity(String getCity) {
            GetCity = getCity == null ? "" : getCity;
        }

        public String getDeliverCity() {
            return DeliverCity == null ? "" : DeliverCity;
        }

        public void setDeliverCity(String deliverCity) {
            DeliverCity = deliverCity == null ? "" : deliverCity;
        }

        public String getTaskNo() {
            return TaskNo;
        }

        public void setTaskNo(String TaskNo) {
            this.TaskNo = TaskNo;
        }

        public String getStartDateTime() {
            return StartDateTime;
        }

        public void setStartDateTime(String StartDateTime) {
            this.StartDateTime = StartDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public int getTruckId() {
            return TruckId;
        }

        public void setTruckId(int TruckId) {
            this.TruckId = TruckId;
        }

        public String getTruckNumber() {
            return TruckNumber;
        }

        public void setTruckNumber(String TruckNumber) {
            this.TruckNumber = TruckNumber;
        }

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int DriverId) {
            this.DriverId = DriverId;
        }

        public String getDriverName() {
            return DriverName;
        }

        public void setDriverName(String DriverName) {
            this.DriverName = DriverName;
        }

        public String getDriverPhone() {
            return DriverPhone;
        }

        public void setDriverPhone(String DriverPhone) {
            this.DriverPhone = DriverPhone;
        }

        public String getDeliverAddress() {
            return DeliverAddress;
        }

        public void setDeliverAddress(String DeliverAddress) {
            this.DeliverAddress = DeliverAddress;
        }

        public String getDeliverAddrLat() {
            return DeliverAddrLat;
        }

        public void setDeliverAddrLat(String DeliverAddrLat) {
            this.DeliverAddrLat = DeliverAddrLat;
        }

        public String getDeliverAddrLng() {
            return DeliverAddrLng;
        }

        public void setDeliverAddrLng(String DeliverAddrLng) {
            this.DeliverAddrLng = DeliverAddrLng;
        }

        public String getGetAddress() {
            return GetAddress;
        }

        public void setGetAddress(String GetAddress) {
            this.GetAddress = GetAddress;
        }

        public String getGetAddrLat() {
            return GetAddrLat;
        }

        public void setGetAddrLat(String GetAddrLat) {
            this.GetAddrLat = GetAddrLat;
        }

        public String getGetAddrLng() {
            return GetAddrLng;
        }

        public void setGetAddrLng(String GetAddrLng) {
            this.GetAddrLng = GetAddrLng;
        }

        public int getOutStoreId() {
            return OutStoreId;
        }

        public void setOutStoreId(int OutStoreId) {
            this.OutStoreId = OutStoreId;
        }

        public String getConsigneeName() {
            return ConsigneeName;
        }

        public void setConsigneeName(String ConsigneeName) {
            this.ConsigneeName = ConsigneeName;
        }

        public String getConsigneePhone() {
            return ConsigneePhone;
        }

        public void setConsigneePhone(String ConsigneePhone) {
            this.ConsigneePhone = ConsigneePhone;
        }

        public int getDistributeStatus() {
            return DistributeStatus;
        }

        public void setDistributeStatus(int DistributeStatus) {
            this.DistributeStatus = DistributeStatus;
        }

        public String getDistributeStatusDesc() {
            return DistributeStatusDesc;
        }

        public void setDistributeStatusDesc(String DistributeStatusDesc) {
            this.DistributeStatusDesc = DistributeStatusDesc;
        }

        public boolean isIsFromOutStorage() {
            return IsFromOutStorage;
        }

        public void setIsFromOutStorage(boolean IsFromOutStorage) {
            this.IsFromOutStorage = IsFromOutStorage;
        }

        public String getGrabNo() {
            return GrabNo;
        }

        public void setGrabNo(String GrabNo) {
            this.GrabNo = GrabNo;
        }

        public int getLoadPhotoId() {
            return LoadPhotoId;
        }

        public void setLoadPhotoId(int LoadPhotoId) {
            this.LoadPhotoId = LoadPhotoId;
        }

        public String getFinishPhotoId() {
            return FinishPhotoId == null ? "" : FinishPhotoId;
        }

        public void setFinishPhotoId(String finishPhotoId) {
            FinishPhotoId = finishPhotoId == null ? "" : finishPhotoId;
        }

        public int getInsertUserId() {
            return InsertUserId;
        }

        public void setInsertUserId(int InsertUserId) {
            this.InsertUserId = InsertUserId;
        }

        public String getInsertUserName() {
            return InsertUserName;
        }

        public void setInsertUserName(String InsertUserName) {
            this.InsertUserName = InsertUserName;
        }

        public String getInsertDateTime() {
            return InsertDateTime;
        }

        public void setInsertDateTime(String InsertDateTime) {
            this.InsertDateTime = InsertDateTime;
        }

        public String getModifyUserId() {
            return ModifyUserId;
        }

        public void setModifyUserId(String ModifyUserId) {
            this.ModifyUserId = ModifyUserId;
        }

        public String getModifyUserName() {
            return ModifyUserName;
        }

        public void setModifyUserName(String ModifyUserName) {
            this.ModifyUserName = ModifyUserName;
        }

        public String getModifyDateTime() {
            return ModifyDateTime;
        }

        public void setModifyDateTime(String ModifyDateTime) {
            this.ModifyDateTime = ModifyDateTime;
        }

        public OutStoreBean getOutStore() {
            return OutStore;
        }

        public void setOutStore(OutStoreBean OutStore) {
            this.OutStore = OutStore;
        }

        public String getGrab() {
            return Grab;
        }

        public void setGrab(String Grab) {
            this.Grab = Grab;
        }

        public String getMItems() {
            return MItems;
        }

        public void setMItems(String MItems) {
            this.MItems = MItems;
        }

        public List<ItemsBean> getItems() {
            return Items;
        }

        public void setItems(List<ItemsBean> Items) {
            this.Items = Items;
        }

        public static class OutStoreBean {
            /**
             * ID : 44
             * StoreName : 南京路仓库
             * StoreAddress : 延吉路175号南京路仓库
             * Tasks : null
             */

            private int ID;
            private String StoreName;
            private String StoreAddress;
            private String Tasks;

            public int getID() {
                return ID;
            }

            public void setID(int ID) {
                this.ID = ID;
            }

            public String getStoreName() {
                return StoreName;
            }

            public void setStoreName(String StoreName) {
                this.StoreName = StoreName;
            }

            public String getStoreAddress() {
                return StoreAddress;
            }

            public void setStoreAddress(String StoreAddress) {
                this.StoreAddress = StoreAddress;
            }

            public String getTasks() {
                return Tasks;
            }

            public void setTasks(String Tasks) {
                this.Tasks = Tasks;
            }
        }

        public static class ItemsBean {
            /**
             * ItemId : 14
             * TaskNo : PT202002050005
             * MaterialName : 普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母
             * MaterialNum : 2000.0
             * MaterialUnit : 件
             * IsStartVerify : true
             * StartVerifyPhotoId : null
             * IsEndVerify : false
             * EndVerifyPhotoId : null
             * Main : null
             */

            private int ItemId;
            private String TaskNo;
            private String MaterialName;
            private double MaterialNum;
            private String MaterialUnit;
            private boolean IsStartVerify;
            private String StartVerifyPhotoId;
            private boolean IsEndVerify;
            private boolean IsFromOutStorage;
            private String EndVerifyPhotoId;
            private String Main;

            public boolean isFromOutStorage() {
                return IsFromOutStorage;
            }

            public void setFromOutStorage(boolean fromOutStorage) {
                IsFromOutStorage = fromOutStorage;
            }

            public int getItemId() {
                return ItemId;
            }

            public void setItemId(int ItemId) {
                this.ItemId = ItemId;
            }

            public String getTaskNo() {
                return TaskNo;
            }

            public void setTaskNo(String TaskNo) {
                this.TaskNo = TaskNo;
            }

            public String getMaterialName() {
                return MaterialName;
            }

            public void setMaterialName(String MaterialName) {
                this.MaterialName = MaterialName;
            }

            public double getMaterialNum() {
                return MaterialNum;
            }

            public void setMaterialNum(double MaterialNum) {
                this.MaterialNum = MaterialNum;
            }

            public String getMaterialUnit() {
                return MaterialUnit;
            }

            public void setMaterialUnit(String MaterialUnit) {
                this.MaterialUnit = MaterialUnit;
            }

            public boolean isIsStartVerify() {
                return IsStartVerify;
            }

            public void setIsStartVerify(boolean IsStartVerify) {
                this.IsStartVerify = IsStartVerify;
            }

            public String getStartVerifyPhotoId() {
                return StartVerifyPhotoId;
            }

            public void setStartVerifyPhotoId(String StartVerifyPhotoId) {
                this.StartVerifyPhotoId = StartVerifyPhotoId;
            }

            public boolean isIsEndVerify() {
                return IsEndVerify;
            }

            public void setIsEndVerify(boolean IsEndVerify) {
                this.IsEndVerify = IsEndVerify;
            }

            public String getEndVerifyPhotoId() {
                return EndVerifyPhotoId;
            }

            public void setEndVerifyPhotoId(String EndVerifyPhotoId) {
                this.EndVerifyPhotoId = EndVerifyPhotoId;
            }

            public String getMain() {
                return Main;
            }

            public void setMain(String Main) {
                this.Main = Main;
            }
        }
    }
}
