package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.GeRenXinXiContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.GeRenXinXi;

import io.reactivex.Observable;
import okhttp3.RequestBody;


@ActivityScope
public class GeRenXinXiModel extends BaseModel implements GeRenXinXiContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public GeRenXinXiModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<GeRenXinXi> getGeRenXinXiData(RequestBody info) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getGeRenXinXiData(info);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}