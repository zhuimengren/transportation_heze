package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersJinXingZhong;

import java.util.List;


public class OrdersJinXingZhongAdapter extends BaseQuickAdapter<OrdersJinXingZhong.DataBean, BaseViewHolder> {

    private Context mContext;

    public OrdersJinXingZhongAdapter(Context context, @Nullable List<OrdersJinXingZhong.DataBean> data) {
        super(R.layout.item_orders_jin_xing_zhong, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, OrdersJinXingZhong.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
        helper.setText(R.id.tv_ren_wu_dan_hao, "任务单号：" + item.getTaskNo());
        helper.setText(R.id.tv_cang_ku, "仓库名称：" + item.getOutStoreName());
        helper.setText(R.id.tv_shou_huo_di_zhi, "收货地址：" + item.getGetAddress());
        helper.setText(R.id.tv_shou_huo_ren, "收货人：" + item.getConsigneeName());
        helper.setText(R.id.tv_shou_huo_ren_lian_xi_fang_shi, "收货人电话：" + item.getConsigneePhone());
        helper.setText(R.id.tv_si_ji_xing_ming, "司机姓名：" + item.getDriverName());
        helper.setText(R.id.tv_si_ji_dian_hua, "司机电话：" + item.getDriverPhone());
    }

}
