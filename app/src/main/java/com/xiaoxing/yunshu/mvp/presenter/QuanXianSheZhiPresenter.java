package com.xiaoxing.yunshu.mvp.presenter;

import android.app.Application;

import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.utils.RxLifecycleUtils;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import java.util.Map;

import com.xiaoxing.yunshu.mvp.contract.QuanXianSheZhiContract;


import com.xiaoxing.yunshu.mvp.ui.entity.QuanXianSheZhi;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class QuanXianSheZhiPresenter extends BasePresenter<QuanXianSheZhiContract.Model, QuanXianSheZhiContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public QuanXianSheZhiPresenter(QuanXianSheZhiContract.Model model, QuanXianSheZhiContract.View rootView) {
        super(model, rootView);
    }

    public void getQuanXianSheZhiData(Map<String, String> map) {

        mModel.getQuanXianSheZhiData(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<QuanXianSheZhi>(mErrorHandler) {
                    @Override
                    public void onNext(QuanXianSheZhi entityData) {
                        mRootView.getQuanXianSheZhiDataSuccess(entityData);
                    }
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
