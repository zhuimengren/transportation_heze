package com.xiaoxing.yunshu.mvp.ui.entity;

import java.io.Serializable;
import java.util.List;

public class CheLiangXinXi implements Serializable{

    /**
     * Code : 200
     * Message : 获取车辆列表成功！
     * Data : [{"TruckId":1,"DriverId":180,"TruckNumber":"12312","TruckTypeDesc":"重型货车","LoadWeight":10,"LoadLength":123,"LoadWidth":456,"LoadHeight":123,"TruckBelongToDesc":"个人","BelongToOrg":"","IsDefault":true},{"TruckId":2,"DriverId":180,"TruckNumber":"12312","TruckTypeDesc":"重型货车","LoadWeight":10,"LoadLength":123,"LoadWidth":456,"LoadHeight":123,"TruckBelongToDesc":"企业","BelongToOrg":"企业2222","IsDefault":false}]
     */

    private int Code;
    private String Message;
    private List<DataBean> Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean implements Serializable {
        /**
         * TruckId : 1
         * DriverId : 180
         * TruckNumber : 12312
         * TruckTypeDesc : 重型货车
         * LoadWeight : 10.0
         * LoadLength : 123.0
         * LoadWidth : 456.0
         * LoadHeight : 123.0
         * TruckBelongToDesc : 个人
         * BelongToOrg :
         * IsDefault : true
         */

        private int TruckId;
        private int DriverId;
        private String TruckNumber;
        private String TruckTypeDesc;
        private double LoadWeight;
        private double LoadLength;
        private double LoadWidth;
        private double LoadHeight;
        private String TruckBelongToDesc;
        private String BelongToOrg;
        private boolean IsDefault;

        public int getTruckId() {
            return TruckId;
        }

        public void setTruckId(int TruckId) {
            this.TruckId = TruckId;
        }

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int DriverId) {
            this.DriverId = DriverId;
        }

        public String getTruckNumber() {
            return TruckNumber;
        }

        public void setTruckNumber(String TruckNumber) {
            this.TruckNumber = TruckNumber;
        }

        public String getTruckTypeDesc() {
            return TruckTypeDesc;
        }

        public void setTruckTypeDesc(String TruckTypeDesc) {
            this.TruckTypeDesc = TruckTypeDesc;
        }

        public double getLoadWeight() {
            return LoadWeight;
        }

        public void setLoadWeight(double LoadWeight) {
            this.LoadWeight = LoadWeight;
        }

        public double getLoadLength() {
            return LoadLength;
        }

        public void setLoadLength(double LoadLength) {
            this.LoadLength = LoadLength;
        }

        public double getLoadWidth() {
            return LoadWidth;
        }

        public void setLoadWidth(double LoadWidth) {
            this.LoadWidth = LoadWidth;
        }

        public double getLoadHeight() {
            return LoadHeight;
        }

        public void setLoadHeight(double LoadHeight) {
            this.LoadHeight = LoadHeight;
        }

        public String getTruckBelongToDesc() {
            return TruckBelongToDesc;
        }

        public void setTruckBelongToDesc(String TruckBelongToDesc) {
            this.TruckBelongToDesc = TruckBelongToDesc;
        }

        public String getBelongToOrg() {
            return BelongToOrg;
        }

        public void setBelongToOrg(String BelongToOrg) {
            this.BelongToOrg = BelongToOrg;
        }

        public boolean isIsDefault() {
            return IsDefault;
        }

        public void setIsDefault(boolean IsDefault) {
            this.IsDefault = IsDefault;
        }
    }
}
