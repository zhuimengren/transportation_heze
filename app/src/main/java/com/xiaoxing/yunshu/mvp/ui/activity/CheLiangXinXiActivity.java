package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerCheLiangXinXiComponent;
import com.xiaoxing.yunshu.di.module.CheLiangXinXiModule;
import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiContract;
import com.xiaoxing.yunshu.mvp.presenter.CheLiangXinXiPresenter;
import com.xiaoxing.yunshu.mvp.ui.adapter.CheLiangXinXiAdapter;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXi;
import com.xiaoxing.yunshu.mvp.ui.entity.DeleteTruck;
import com.xiaoxing.yunshu.mvp.ui.inter.ICheLiangXinXi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.RecycleViewDivider;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_CHE_LIANG_XIN_XI)
public class CheLiangXinXiActivity extends BaseActivity<CheLiangXinXiPresenter> implements CheLiangXinXiContract.View, OnRefreshListener, ICheLiangXinXi {


    private static boolean mIsNeedDemo = true;
    @BindView(R.id.btnRight)
    TextView mBtnRight;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.empty_image)
    ImageView empty_image;
    @BindView(R.id.empty)
    View mEmptyLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    RefreshLayout mRefreshLayout;

    private View.OnClickListener mRightListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.navigation(CheLiangXinXiActivity.this, RouterHub.ACTIVITY_CHE_LIANG_XIN_XI_ADD);
        }
    };
    private CheLiangXinXiAdapter mAdapter;
    private List<CheLiangXinXi.DataBean> mDataBeanList = new ArrayList<>();

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerCheLiangXinXiComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .cheLiangXinXiModule(new CheLiangXinXiModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_che_liang_xin_xi; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBackWithRightTextView(this, getString(R.string.activity_che_liang_xin_xi), mRightListener);
        mBtnRight.setText("新增");

        initRefreshLayout();
        initRecyclerView();
        initEmpty();

    }

    private void initRefreshLayout() {
        mRefreshLayout.setRefreshHeader(new
                ClassicsHeader(this).setSpinnerStyle(SpinnerStyle.FixedBehind).setPrimaryColorId(R.color.public_colorPrimary).setAccentColorId(android.R.color.white));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setEnableRefresh(false);
        mRefreshLayout.autoRefresh();
        mRefreshLayout.setEnableLoadMore(false);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(CheLiangXinXiActivity.this));
        mRecyclerView.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, 10, Color.parseColor("#EEEEEE")));

        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter = new CheLiangXinXiAdapter(CheLiangXinXiActivity.this, mDataBeanList , this));

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            }
        });
    }

    private void initEmpty() {
        empty_image.setImageResource(R.drawable.ic_empty);
        empty_text.setTextColor(Color.parseColor("#CFCFCF"));
        empty_text.setText("暂无数据");
    }

    private List<CheLiangXinXi.DataBean> loadModels() {
        List<CheLiangXinXi.DataBean> lists = new ArrayList<>();

        for (int i = 0; i < 5; i++) {

            CheLiangXinXi.DataBean dataBean = new CheLiangXinXi.DataBean();
            lists.add(dataBean);
        }

        return lists;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCheLiangXinXiList();

    }

    private void getCheLiangXinXiList() {
        HashMap<String, String> map = new HashMap<>();

        mPresenter.getCheLiangXinXiList(map);
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        mRefreshLayout.finishRefresh();

    }

    @Override
    public void getCheLiangXinXiDataSuccess(CheLiangXinXi entityList) {

        if (entityList != null && entityList.getData() != null && entityList.getData().size() > 0) {
            mDataBeanList.clear();
            mDataBeanList.addAll(entityList.getData());
            mAdapter.notifyDataSetChanged();
            mEmptyLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        }

    }

    @Override
    public void deleteTruckSuccess(DeleteTruck entityList) {
        getCheLiangXinXiList();
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);

    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @Override
    public void cheLiangXinXiDel(CheLiangXinXi.DataBean item) {
        HashMap<String, String> map = new HashMap<>();
        map.put("truckId", String.valueOf(item.getTruckId()));
        mPresenter.deleteTruck(map);
    }

    @Override
    public void cheLiangXinXiEdit(CheLiangXinXi.DataBean item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", item);

        Utils.navigation(CheLiangXinXiActivity.this, RouterHub.ACTIVITY_CHE_LIANG_XIN_XI_EDIT, bundle);
    }

    @Override
    public void setDefault(String isDefault, String address_id) {

    }
}
