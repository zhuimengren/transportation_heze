package com.xiaoxing.yunshu.mvp.ui.entity;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2020/2/14 0014 10:49
 */
public class StartDistibute {

    /**
     * Data : null
     * Code : 400
     * Message : 编号为“PT202002060001”的配送单还没有核对，不能开始配送。
     */

    private Object Data;
    private int Code;
    private String Message;

    public Object getData() {
        return Data;
    }

    public void setData(Object Data) {
        this.Data = Data;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}
