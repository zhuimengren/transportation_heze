package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.base.BaseConstants;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerGeRenXinXiComponent;
import com.xiaoxing.yunshu.di.module.GeRenXinXiModule;
import com.xiaoxing.yunshu.mvp.contract.GeRenXinXiContract;
import com.xiaoxing.yunshu.mvp.presenter.GeRenXinXiPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.GeRenXinXi;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.addapp.pickers.picker.DatePicker;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.XEditText;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import okhttp3.RequestBody;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_GE_REN_XIN_XI)
public class GeRenXinXiActivity extends BaseActivity<GeRenXinXiPresenter> implements GeRenXinXiContract.View {


    @BindView(R.id.xet_name)
    XEditText xetName;
    @BindView(R.id.xet_card_no)
    XEditText xetCardNo;
    @BindView(R.id.tv_birth)
    TextView tvBirth;
    @BindView(R.id.ll_birth)
    LinearLayout llBirth;
    @BindView(R.id.tv_sex)
    TextView tvSex;
    @BindView(R.id.ll_sex)
    LinearLayout llSex;
    @BindView(R.id.xet_driving_age)
    XEditText xetDrivingAge;
    @BindView(R.id.btn_save)
    Button btnSave;
    private int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerGeRenXinXiComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .geRenXinXiModule(new GeRenXinXiModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_ge_ren_xin_xi; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_ge_ren_xin_xi));
        //getGeRenXinXiData();

        xetName.setText(mSharedPreferencesHelper.getString(BaseConstants.NAME));
        xetCardNo.setText(mSharedPreferencesHelper.getString(BaseConstants.CARD_NO));
        tvBirth.setText(mSharedPreferencesHelper.getString(BaseConstants.BIRTH));
        if (mSharedPreferencesHelper.getString(BaseConstants.SEX).equals("true")) {//Sex是bool型的true为男，false为女
            tvSex.setText("男");
        } else if (mSharedPreferencesHelper.getString(BaseConstants.SEX).equals("false")) {
            tvSex.setText("女");
        }
        xetDrivingAge.setText(mSharedPreferencesHelper.getString(BaseConstants.DRIVING_AGE));
    }

    @Override
    public void getGeRenXinXiDataSuccess(GeRenXinXi entityData) {

        showMessage(entityData.getMessage());
        if (entityData.getCode() == 200) {
            mSharedPreferencesHelper.putString(BaseConstants.NAME, xetName.getText().toString().trim());
            mSharedPreferencesHelper.putString(BaseConstants.CARD_NO, xetCardNo.getText().toString().trim());
            mSharedPreferencesHelper.putString(BaseConstants.BIRTH, tvBirth.getText().toString().trim());
            if (tvSex.getText().toString().trim().equals("男")) {//Sex是bool型的true为男，false为女
                mSharedPreferencesHelper.putString(BaseConstants.SEX, "true");
            } else if (tvSex.getText().toString().trim().equals("女")) {
                mSharedPreferencesHelper.putString(BaseConstants.SEX, "false");
            }
            mSharedPreferencesHelper.putString(BaseConstants.DRIVING_AGE, xetDrivingAge.getText().toString().trim());
            killMyself();
        }
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick({R.id.ll_birth, R.id.ll_sex, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_birth:
                selectBirth();
                break;
            case R.id.ll_sex:
                selectSex();
                break;
            case R.id.btn_save:
                getGeRenXinXiData();
                break;
        }
    }

    /*
     * 年月日选择
     * */
    public void selectBirth() {
        final DatePicker picker = new DatePicker(this);
        Calendar now = Calendar.getInstance();

        picker.setTopPadding(15);
//        picker.setRangeStart(2016, 8, 29);
//        picker.setRangeEnd(2111, 1, 11);
//        picker.setSelectedItem(2050, 10, 14);

        picker.setDateRangeStart(1900, 1, 1);
        picker.setDateRangeEnd(2500, 12, 31);
        picker.setSelectedItem(now.get(Calendar.YEAR), (now.get(Calendar.MONTH) + 1), now.get(Calendar.DAY_OF_MONTH) + 1, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE));
        picker.setWeightEnable(true);
        picker.setLineColor(Color.BLACK);
        picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
            @Override
            public void onDatePicked(String year, String month, String day) {
                tvBirth.setText(year + "-" + month + "-" + day);
            }
        });
        picker.setOnWheelListener(new DatePicker.OnWheelListener() {
            @Override
            public void onYearWheeled(int index, String year) {
                picker.setTitleText(year + "-" + picker.getSelectedMonth() + "-" + picker.getSelectedDay());
            }

            @Override
            public void onMonthWheeled(int index, String month) {
                picker.setTitleText(picker.getSelectedYear() + "-" + month + "-" + picker.getSelectedDay());
            }

            @Override
            public void onDayWheeled(int index, String day) {
                picker.setTitleText(picker.getSelectedYear() + "-" + picker.getSelectedMonth() + "-" + day);
            }
        });
        picker.show();
    }

    private void selectSex() {
        final String[] items = new String[]{"男", "女"};//Sex是bool型的true为男，false为女
        new QMUIDialog.MenuDialogBuilder(this)
                .addItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(getActivity(), "你选择了 " + items[which], Toast.LENGTH_SHORT).show();
                        tvSex.setText(items[which]);
                        dialog.dismiss();
                    }
                })
                .create(mCurrentDialogStyle).show();
    }

    private void getGeRenXinXiData() {
        HashMap<String, String> map = new HashMap<>();
        String sex = tvSex.getText().toString().trim();
        if (tvSex.getText().toString().trim().equals("男")) {//Sex是bool型的true为男，false为女
            sex = "true";
        } else if (tvSex.getText().toString().trim().equals("女")) {
            sex = "false";
        }
        String jsonData = "{\"IDCardNo\":\"" + xetCardNo.getText().toString().trim() + "\",\"Birthday\":\"" + tvBirth.getText().toString().trim() + "\",\"Sex\":" + sex + ",\"DrivingAge\":" + xetDrivingAge.getText().toString().trim() + "} ";
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonData);
        mPresenter.getGeRenXinXiData(body);
    }


}
