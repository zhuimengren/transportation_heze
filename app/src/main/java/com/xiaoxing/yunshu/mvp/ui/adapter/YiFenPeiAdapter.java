package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPei;

import java.util.List;


public class YiFenPeiAdapter extends BaseQuickAdapter<YiFenPei.DataBean, BaseViewHolder> {

    private Context mContext;

    public YiFenPeiAdapter(Context context, @Nullable List<YiFenPei.DataBean> data) {
        super(R.layout.item_yi_fen_pei, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, YiFenPei.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
        helper.setText(R.id.tv_dan_hao, "单号：" + item.getGrabNo());
        helper.setText(R.id.tv_jia_ge, "价格：" + item.getRealPrice());
        helper.setText(R.id.tv_miao_shu, "描述：" + item.getRemark());
        helper.setText(R.id.tv_che_pai_hao, "车牌号：" + item.getTruckNumber());
    }

}
