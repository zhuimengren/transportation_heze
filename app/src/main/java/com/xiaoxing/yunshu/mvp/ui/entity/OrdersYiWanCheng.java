package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class OrdersYiWanCheng {


    /**
     * Data : [{"GrabNo":"PG202002060001","RefPrice":700,"RealPrice":650,"Remark":"装卸信息","TruckNumber":"1111","DriverId":180,"DriverName":"xx","DriverCount":1,"GrabStatus":6,"GrabStatusName":"已完成","PublishUserId":174,"PublishUserName":"配送管理员","PublishDateTime":"2020-02-06 10:37:07","DistributeUserId":174,"DistributeUserName":"配送管理员","DistributeDateTime":"2020-02-13 09:48:07","InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-06 10:35:37","FinishDateTime":"2020-02-13 09:47:11","FinishPSDateTime":"2020-02-15 12:49:00"}]
     * Code : 200
     * Message : 获取成功！
     */

    private int Code;
    private String Message;
    private List<DataBean> Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * GrabNo : PG202002060001
         * RefPrice : 700.0
         * RealPrice : 650.0
         * Remark : 装卸信息
         * TruckNumber : 1111
         * DriverId : 180
         * DriverName : xx
         * DriverCount : 1
         * GrabStatus : 6
         * GrabStatusName : 已完成
         * PublishUserId : 174
         * PublishUserName : 配送管理员
         * PublishDateTime : 2020-02-06 10:37:07
         * DistributeUserId : 174
         * DistributeUserName : 配送管理员
         * DistributeDateTime : 2020-02-13 09:48:07
         * InsertUserId : 174
         * InsertUserName : 配送管理员
         * InsertDateTime : 2020-02-06 10:35:37
         * FinishDateTime : 2020-02-13 09:47:11
         * FinishPSDateTime : 2020-02-15 12:49:00
         */

        private String GrabNo;
        private double RefPrice;
        private double RealPrice;
        private String Remark;
        private String TruckNumber;
        private int DriverId;
        private String DriverName;
        private int DriverCount;
        private int GrabStatus;
        private String GrabStatusName;
        private int PublishUserId;
        private String PublishUserName;
        private String PublishDateTime;
        private int DistributeUserId;
        private String DistributeUserName;
        private String DistributeDateTime;
        private int InsertUserId;
        private String InsertUserName;
        private String InsertDateTime;
        private String FinishDateTime;
        private String FinishPSDateTime;

        public String getGrabNo() {
            return GrabNo == null ? "" : GrabNo;
        }

        public void setGrabNo(String grabNo) {
            GrabNo = grabNo == null ? "" : grabNo;
        }

        public double getRefPrice() {
            return RefPrice;
        }

        public void setRefPrice(double refPrice) {
            RefPrice = refPrice;
        }

        public double getRealPrice() {
            return RealPrice;
        }

        public void setRealPrice(double realPrice) {
            RealPrice = realPrice;
        }

        public String getRemark() {
            return Remark == null ? "" : Remark;
        }

        public void setRemark(String remark) {
            Remark = remark == null ? "" : remark;
        }

        public String getTruckNumber() {
            return TruckNumber == null ? "" : TruckNumber;
        }

        public void setTruckNumber(String truckNumber) {
            TruckNumber = truckNumber == null ? "" : truckNumber;
        }

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int driverId) {
            DriverId = driverId;
        }

        public String getDriverName() {
            return DriverName == null ? "" : DriverName;
        }

        public void setDriverName(String driverName) {
            DriverName = driverName == null ? "" : driverName;
        }

        public int getDriverCount() {
            return DriverCount;
        }

        public void setDriverCount(int driverCount) {
            DriverCount = driverCount;
        }

        public int getGrabStatus() {
            return GrabStatus;
        }

        public void setGrabStatus(int grabStatus) {
            GrabStatus = grabStatus;
        }

        public String getGrabStatusName() {
            return GrabStatusName == null ? "" : GrabStatusName;
        }

        public void setGrabStatusName(String grabStatusName) {
            GrabStatusName = grabStatusName == null ? "" : grabStatusName;
        }

        public int getPublishUserId() {
            return PublishUserId;
        }

        public void setPublishUserId(int publishUserId) {
            PublishUserId = publishUserId;
        }

        public String getPublishUserName() {
            return PublishUserName == null ? "" : PublishUserName;
        }

        public void setPublishUserName(String publishUserName) {
            PublishUserName = publishUserName == null ? "" : publishUserName;
        }

        public String getPublishDateTime() {
            return PublishDateTime == null ? "" : PublishDateTime;
        }

        public void setPublishDateTime(String publishDateTime) {
            PublishDateTime = publishDateTime == null ? "" : publishDateTime;
        }

        public int getDistributeUserId() {
            return DistributeUserId;
        }

        public void setDistributeUserId(int distributeUserId) {
            DistributeUserId = distributeUserId;
        }

        public String getDistributeUserName() {
            return DistributeUserName == null ? "" : DistributeUserName;
        }

        public void setDistributeUserName(String distributeUserName) {
            DistributeUserName = distributeUserName == null ? "" : distributeUserName;
        }

        public String getDistributeDateTime() {
            return DistributeDateTime == null ? "" : DistributeDateTime;
        }

        public void setDistributeDateTime(String distributeDateTime) {
            DistributeDateTime = distributeDateTime == null ? "" : distributeDateTime;
        }

        public int getInsertUserId() {
            return InsertUserId;
        }

        public void setInsertUserId(int insertUserId) {
            InsertUserId = insertUserId;
        }

        public String getInsertUserName() {
            return InsertUserName == null ? "" : InsertUserName;
        }

        public void setInsertUserName(String insertUserName) {
            InsertUserName = insertUserName == null ? "" : insertUserName;
        }

        public String getInsertDateTime() {
            return InsertDateTime == null ? "" : InsertDateTime;
        }

        public void setInsertDateTime(String insertDateTime) {
            InsertDateTime = insertDateTime == null ? "" : insertDateTime;
        }

        public String getFinishDateTime() {
            return FinishDateTime == null ? "" : FinishDateTime;
        }

        public void setFinishDateTime(String finishDateTime) {
            FinishDateTime = finishDateTime == null ? "" : finishDateTime;
        }

        public String getFinishPSDateTime() {
            return FinishPSDateTime == null ? "" : FinishPSDateTime;
        }

        public void setFinishPSDateTime(String finishPSDateTime) {
            FinishPSDateTime = finishPSDateTime == null ? "" : finishPSDateTime;
        }
    }
}
