package com.xiaoxing.yunshu.mvp.ui.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersDaiQiangDanFragment;
import com.xiaoxing.yunshu.mvp.ui.fragment.QiangDanFragment;
import com.xiaoxing.yunshu.mvp.ui.fragment.WoDeFragment;

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    private int size;

    public MyViewPagerAdapter(FragmentManager fm, int size) {
        super(fm);
        this.size = size;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = OrdersDaiQiangDanFragment.newInstance();
                break;
            case 1:
                fragment = QiangDanFragment.newInstance();
                break;
            case 2:
                fragment = WoDeFragment.newInstance();
                break;
//            case 3:
//                fragment = LiShiJiLuFragment.newInstance();
//                break;

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return size;
    }
}
