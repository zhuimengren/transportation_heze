package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.OrdersYiWanChengContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersYiWanCheng;

import io.reactivex.Observable;


@FragmentScope
public class OrdersYiWanChengModel extends BaseModel implements OrdersYiWanChengContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public OrdersYiWanChengModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<OrdersYiWanCheng> getOrdersYiWanChengData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getOrdersYiWanChengData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}