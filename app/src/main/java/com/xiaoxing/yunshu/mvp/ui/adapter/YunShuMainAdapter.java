package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.MaterialDriverMappingCangKu;
import com.xiaoxing.yunshu.mvp.ui.entity.MaterialDriverMappingChanPin;

import java.util.List;
import java.util.Map;


public class YunShuMainAdapter extends ExpandableRecyclerAdapter<YunShuMainAdapter.OrderListItem> {
    public static final int TYPE_CANG_KU_NAME = 1000; //仓库名称
    public static final int TYPE_CANG_KU_PRODUCTS = 1001; //仓库商品名称
    public static final int TYPE_FOOTER = 1002; //商家订单底部统计

    private List<MaterialDriverMappingCangKu> mGroups;
    private Map<String, List<MaterialDriverMappingChanPin>> mChildren;
    private Map<Integer, Integer> mGroupsPosition;

    private CheckInterface checkInterface;
    private WanChengInterface mWanCheng;

    public void setCheckInterface(CheckInterface checkInterface) {
        this.checkInterface = checkInterface;
    }

    public void setWanChengInterface(WanChengInterface wanChengInterface) {
        this.mWanCheng = wanChengInterface;
    }

    public Object getChild(int groupPosition, int childPosition) {
        List<MaterialDriverMappingChanPin> childs = mChildren.get(String.valueOf(groupPosition));
        return childs.get(childPosition);
    }


    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }


    public YunShuMainAdapter(Context context, List<MaterialDriverMappingCangKu> groups, Map<String, List<MaterialDriverMappingChanPin>> children, Map<Integer, Integer> groupsPosition) {
        super(context);
        this.mGroups = groups;
        this.mChildren = children;
        this.mGroupsPosition = groupsPosition;
    }

    public static class OrderListItem extends ExpandableRecyclerAdapter.ListItem {
        public MaterialDriverMappingCangKu mMaterialDriverMappingCangKu;
        public MaterialDriverMappingChanPin mMaterialDriverMappingChanPin;
        public int mGroupPosition;
        public int mChildPosition;

        public OrderListItem(MaterialDriverMappingCangKu materialDriverMappingCangKu) {
            super(TYPE_CANG_KU_NAME);
            mMaterialDriverMappingCangKu = materialDriverMappingCangKu;
        }

        public OrderListItem(int groupPosition, MaterialDriverMappingChanPin materialDriverMappingChanPin, int childPosition) {
            super(TYPE_CANG_KU_PRODUCTS);
            mMaterialDriverMappingChanPin = materialDriverMappingChanPin;
            mGroupPosition = groupPosition;
            mChildPosition = childPosition;
        }

        public OrderListItem(String first, String last, String footer, String type) {
            super(TYPE_FOOTER);
        }
    }

    public class ShangJiaNameViewHolder extends ExpandableRecyclerAdapter.ViewHolder {

        private TextView tv_name;
        private CheckBox cb_cang_ku_name;

        public ShangJiaNameViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_che_pai_hao);
            cb_cang_ku_name = view.findViewById(R.id.cb_cang_ku_name);
        }

        public void bind(int position) {
            MaterialDriverMappingCangKu materialDriverMappingCangKu = visibleItems.get(position).mMaterialDriverMappingCangKu;
            tv_name.setText(materialDriverMappingCangKu.getFromstorename());

            final MaterialDriverMappingCangKu group = (MaterialDriverMappingCangKu) getGroup(mGroupsPosition.get(position));
            cb_cang_ku_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)

                {
                    group.setChoosed(((CheckBox) v).isChecked());
                    checkInterface.checkGroup(mGroupsPosition.get(position), ((CheckBox) v).isChecked());// 暴露组选接口
                }
            });

            cb_cang_ku_name.setChecked(group.isChoosed());

        }
    }

    public class ProductsViewHolder extends ExpandableRecyclerAdapter.ViewHolder {

        private TextView tv_name;
        private TextView tv_num;
        private TextView tv_sao_mao;
        private CheckBox cb_product_name;

        public ProductsViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_che_pai_hao);
            tv_num = view.findViewById(R.id.tv_num);
            cb_product_name = view.findViewById(R.id.cb_product_name);
            tv_sao_mao = view.findViewById(R.id.tv_sao_mao);
        }

        public void bind(int position) {
            MaterialDriverMappingChanPin materialDriverMappingChanPin = visibleItems.get(position).mMaterialDriverMappingChanPin;
            tv_name.setText(materialDriverMappingChanPin.getMaterial());
            tv_num.setText("共" + materialDriverMappingChanPin.getNum() + "件");

            int groupPosition = visibleItems.get(position).mGroupPosition;
            int childPosition = visibleItems.get(position).mChildPosition;

            final MaterialDriverMappingChanPin goodsInfo2 = (MaterialDriverMappingChanPin) getChild(groupPosition, childPosition);

            cb_product_name.setChecked(goodsInfo2.isChoosed());

            if (goodsInfo2.getIsbeginscan().equals("1")) {
                tv_sao_mao.setText("已扫描");
            } else {
                tv_sao_mao.setText("未扫描");
            }

            cb_product_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    goodsInfo2.setChoosed(((CheckBox) v).isChecked());
                    cb_product_name.setChecked(((CheckBox) v).isChecked());
                    checkInterface.checkChild(groupPosition, position, ((CheckBox) v).isChecked());// 暴露子选接口

                }
            });

        }
    }

    public class FooterViewHolder extends ExpandableRecyclerAdapter.ViewHolder {

        private Button btn_que_ding;

        public FooterViewHolder(View view) {
            super(view);
            btn_que_ding = view.findViewById(R.id.btn_que_ding);
        }

        public void bind(int position) {
            btn_que_ding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mWanCheng.wangCheng();
                }
            });
        }
    }

    @Override
    public ExpandableRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CANG_KU_NAME:
                return new ShangJiaNameViewHolder(inflate(R.layout.item_cang_ku_name, parent));
            case TYPE_FOOTER:
                return new FooterViewHolder(inflate(R.layout.item_cang_ku_footer, parent));
            case TYPE_CANG_KU_PRODUCTS:
            default:
                return new ProductsViewHolder(inflate(R.layout.item_cang_ku_products, parent));
        }
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_CANG_KU_NAME:
                ((ShangJiaNameViewHolder) holder).bind(position);
                break;
            case TYPE_FOOTER:
                ((FooterViewHolder) holder).bind(position);
                break;
            case TYPE_CANG_KU_PRODUCTS:
            default:
                ((ProductsViewHolder) holder).bind(position);
                break;
        }
    }


    public void setDataItems(List<OrderListItem> orderListItems) {
        setItems(orderListItems);
    }


    /**
     * 复选框接口
     */
    public interface CheckInterface {
        /**
         * 组选框状态改变触发的事件
         *
         * @param groupPosition 组元素位置
         * @param isChecked     组元素选中与否
         */
        void checkGroup(int groupPosition, boolean isChecked);

        /**
         * 子选框状态改变时触发的事件
         *
         * @param groupPosition 组元素位置
         * @param childPosition 子元素位置
         * @param isChecked     子元素选中与否
         */
        void checkChild(int groupPosition, int childPosition, boolean isChecked);
    }

    public interface WanChengInterface {
        void wangCheng();
    }
}
