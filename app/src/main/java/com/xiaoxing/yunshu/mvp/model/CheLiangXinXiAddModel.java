package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiAddContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXiAdd;
import com.xiaoxing.yunshu.mvp.ui.entity.GetTruckTypeList;

import io.reactivex.Observable;
import okhttp3.RequestBody;


@ActivityScope
public class CheLiangXinXiAddModel extends BaseModel implements CheLiangXinXiAddContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public CheLiangXinXiAddModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<CheLiangXinXiAdd> getCheLiangXinXiAddData(RequestBody info) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getCheLiangXinXiAddData(info);

    }

    @Override
    public Observable<GetTruckTypeList> getTruckTypeList(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getTruckTypeList(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}