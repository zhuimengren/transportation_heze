package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersDaiQiangDan;

import java.util.List;


public class OrdersDaiQiangDanAdapter extends BaseQuickAdapter<OrdersDaiQiangDan.DataBean, BaseViewHolder> {

    private Context mContext;

    public OrdersDaiQiangDanAdapter(Context context, @Nullable List<OrdersDaiQiangDan.DataBean> data) {
        super(R.layout.item_orders_dai_qiang_dan, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, OrdersDaiQiangDan.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
        helper.setText(R.id.tv_dan_hao, "抢单单号：" + item.getGrabNo());
        helper.setText(R.id.tv_jia_ge, "参考价：" + item.getRefPrice());
    }

}
