package com.xiaoxing.yunshu.mvp.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Poi;
import com.amap.api.services.geocoder.GeocodeQuery;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.dds.soap.SoapListener;
import com.dds.soap.SoapParams;
import com.dds.soap.SoapUtil;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.base.BaseConstants;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerYunShuMainComponent;
import com.xiaoxing.yunshu.di.module.YunShuMainModule;
import com.xiaoxing.yunshu.mvp.contract.YunShuMainContract;
import com.xiaoxing.yunshu.mvp.presenter.YunShuMainPresenter;
import com.xiaoxing.yunshu.mvp.ui.adapter.YunShuMainAdapter;
import com.xiaoxing.yunshu.mvp.ui.entity.MaterialDriverMappingCangKu;
import com.xiaoxing.yunshu.mvp.ui.entity.MaterialDriverMappingChanPin;
import com.xiaoxing.yunshu.mvp.ui.entity.YunShuMain;

import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;
import pub.devrel.easypermissions.EasyPermissions;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static com.jess.arms.base.BaseConstants.NAME_SPACE;
import static com.jess.arms.base.BaseConstants.SCAN_REQUEST_CODE;
import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.APP_YUN_SHU_CHU_KU_MAIN_ACTIVITY)
public class YunShuChuKuMainActivity extends BaseActivity<YunShuMainPresenter> implements YunShuMainContract.View, OnRefreshListener, YunShuMainAdapter.CheckInterface, YunShuMainAdapter.WanChengInterface
        , EasyPermissions.PermissionCallbacks, GeocodeSearch.OnGeocodeSearchListener {


    private static boolean mIsNeedDemo = true;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.empty_image)
    ImageView empty_image;

    @BindView(R.id.empty)
    View mEmptyLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    RefreshLayout mRefreshLayout;
    @BindView(R.id.btnRight)
    ImageView mBtnRight;
    @BindView(R.id.nice_spinner)
    Spinner nice_spinner;
    private YunShuMainAdapter mAdapter;
    private List<YunShuMain.DataBean> mDataBeanList = new ArrayList<>();
    private List<MaterialDriverMappingCangKu> groups = new ArrayList<MaterialDriverMappingCangKu>();// 组元素数据列表
    private Map<String, List<MaterialDriverMappingChanPin>> children = new HashMap<String, List<MaterialDriverMappingChanPin>>();// 子元素数据列表
    private Map<Integer, Integer> mGroupsPosition = new HashMap<>();
    private List<Poi> poiList;
    private String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerYunShuMainComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .yunShuMainModule(new YunShuMainModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_yun_shu_main; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleNoBack(this, "出库列表");
        mBtnRight.setImageResource(R.drawable.icon_scan);
        nice_spinner.setVisibility(View.GONE);
        initRefreshLayout();
        initRecyclerView();
        initEmpty();


    }

    private void initRefreshLayout() {
        mRefreshLayout.setRefreshHeader(new
                ClassicsHeader(this).setSpinnerStyle(SpinnerStyle.FixedBehind).setPrimaryColorId(R.color.public_colorPrimary).setAccentColorId(android.R.color.white));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.autoRefresh();
        mRefreshLayout.setEnableLoadMore(false);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(YunShuChuKuMainActivity.this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(YunShuChuKuMainActivity.this, VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter = new YunShuMainAdapter(YunShuChuKuMainActivity.this, groups, children, mGroupsPosition));


        mAdapter.setCheckInterface(this);// 关键步骤1,设置复选框接口
        mAdapter.setWanChengInterface(this);// 设置完成

    }

    private void initEmpty() {
        empty_image.setImageResource(R.drawable.ic_empty);
        empty_text.setTextColor(Color.parseColor("#CFCFCF"));
        empty_text.setText("暂无数据");
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        poiList = new ArrayList<>();

        getDriverTaskMaterial();
        mRefreshLayout.finishRefresh();
    }

    /**
     * 出库列表
     */
    private void getDriverTaskMaterial() {

        showLoading();

        String methodName = "GetDriverTaskMaterial";

        SoapUtil soapUtil = SoapUtil.getInstance(this);
        soapUtil.setDotNet(true);
        SoapParams params = new SoapParams();
        params.put("driverId", mSharedPreferencesHelper.getString(BaseConstants.UID));

        /**
         * 请求
         */
        soapUtil.callEntry(getUrl(), NAME_SPACE, methodName, params, new SoapListener() {
            @Override
            public void onSuccess(int statusCode, SoapObject object) {
                hideLoading();
                groups.clear();
                children.clear();
                mGroupsPosition.clear();

                List<MaterialDriverMappingCangKu> materialDriverMappingCangKuList = new ArrayList<>();

                if (object.getPropertyCount() == 0) {
                    mEmptyLayout.setVisibility(View.VISIBLE);
                } else {

                    SoapObject driverTaskMaterialResult = (SoapObject) object.getProperty("GetDriverTaskMaterialResult");

                    for (int i = 0; i < driverTaskMaterialResult.getPropertyCount(); i++) {
                        SoapObject materialdrivermappingAndroidJson = (SoapObject) driverTaskMaterialResult.getProperty(i);

                        if (materialdrivermappingAndroidJson.getPropertyCount() == 0) {
                            mEmptyLayout.setVisibility(View.VISIBLE);
                        } else {
                            mEmptyLayout.setVisibility(View.GONE);


                            MaterialDriverMappingCangKu materialDriverMappingCangKu = new MaterialDriverMappingCangKu();

                            materialDriverMappingCangKu.setFromstorename(materialdrivermappingAndroidJson.getPropertyAsString("fromstorename"));
                            materialDriverMappingCangKu.setFromstoreaddress(materialdrivermappingAndroidJson.getPropertyAsString("fromstoreaddress"));
//                    materialDriverMappingCangKu.setTasks(materialdrivermappingCangKuBean.getPropertyAsString("fromstoreaddress"));
                            List<MaterialDriverMappingChanPin> materialDriverMappingChanPinList = new ArrayList<>();

                            SoapObject materialdrivermapping = (SoapObject) materialdrivermappingAndroidJson.getProperty("tasks");

                            for (int i1 = 0; i1 < materialdrivermapping.getPropertyCount(); i1++) {
                                SoapObject materialdrivermappingBean = (SoapObject) materialdrivermapping.getProperty(i1);

                                MaterialDriverMappingChanPin materialDriverMappingChanPin = new MaterialDriverMappingChanPin();
                                materialDriverMappingChanPin.setMaterial(materialdrivermappingBean.getPropertyAsString("material"));
                                materialDriverMappingChanPin.setReceivephone(materialdrivermappingBean.getPropertyAsString("receivephone"));
                                materialDriverMappingChanPin.setReceiveusername(materialdrivermappingBean.getPropertyAsString("receiveusername"));
                                materialDriverMappingChanPin.setTargetproject(materialdrivermappingBean.getPropertyAsString("targetproject"));
                                materialDriverMappingChanPin.setFromspaceaddress(materialdrivermappingBean.getPropertyAsString("fromstoreaddress"));
                                materialDriverMappingChanPin.setFromstorename(materialdrivermappingBean.getPropertyAsString("fromstorename"));
                                materialDriverMappingChanPin.setFromspacename(materialdrivermappingBean.getPropertyAsString("fromspacename"));
                                materialDriverMappingChanPin.setFromspaceaddress(materialdrivermappingBean.getPropertyAsString("fromspaceaddress"));
                                materialDriverMappingChanPin.setTostoreaddress(materialdrivermappingBean.getPropertyAsString("tostoreaddress"));
                                materialDriverMappingChanPin.setDrivername(materialdrivermappingBean.getPropertyAsString("drivername"));
                                materialDriverMappingChanPin.setId(materialdrivermappingBean.getPropertyAsString("id"));
                                materialDriverMappingChanPin.setMid(materialdrivermappingBean.getPropertyAsString("mid"));
                                materialDriverMappingChanPin.setDriverid(materialdrivermappingBean.getPropertyAsString("driverid"));
                                materialDriverMappingChanPin.setIsactive(materialdrivermappingBean.getPropertyAsString("isactive"));
                                materialDriverMappingChanPin.setIsbeginscan(materialdrivermappingBean.getPropertyAsString("isbeginscan"));
                                materialDriverMappingChanPin.setIsendscan(materialdrivermappingBean.getPropertyAsString("isendscan"));
                                materialDriverMappingChanPin.setIsontransport(materialdrivermappingBean.getPropertyAsString("isontransport"));
                                materialDriverMappingChanPin.setNum(materialdrivermappingBean.getPropertyAsString("num"));
                                materialDriverMappingChanPin.setAssigntime(materialdrivermappingBean.getProperty("assigntime") == null ? "" : materialdrivermappingBean.getProperty("assigntime").toString());
                                materialDriverMappingChanPin.setBegintime(materialdrivermappingBean.getProperty("begintime") == null ? "" : materialdrivermappingBean.getProperty("begintime").toString());
                                materialDriverMappingChanPin.setEndtime(materialdrivermappingBean.getProperty("endtime") == null ? "" : materialdrivermappingBean.getProperty("endtime").toString());


                                addressChangeLat(materialDriverMappingChanPin.getTostoreaddress());

                                materialDriverMappingChanPinList.add(materialDriverMappingChanPin);
                            }

                            materialDriverMappingCangKu.setTasks(materialDriverMappingChanPinList);
                            materialDriverMappingCangKuList.add(materialDriverMappingCangKu);
                        }
                    }


                    mAdapter.setDataItems(getSampleItems(materialDriverMappingCangKuList));
                    mAdapter.notifyDataSetChanged();
                    mAdapter.expandAll();
                }


            }

            @Override
            public void onFailure(int statusCode, String content, Throwable error) {
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, SoapFault fault) {
                hideLoading();
            }
        }, getAapiUserName(), getApiPwd());


    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    //地理编码(地址转坐标)
    private void addressChangeLat(String toAddress) {
        GeocodeSearch geocoderSearch = new GeocodeSearch(this);
        geocoderSearch.setOnGeocodeSearchListener(this);
        // name表示地址,第二个参数表示查询城市,中文或者中文全拼,citycode、adcode
        GeocodeQuery query = new GeocodeQuery(toAddress, "");
        geocoderSearch.getFromLocationNameAsyn(query);
    }
//获得结果

    public List<YunShuMainAdapter.OrderListItem> getSampleItems(List<MaterialDriverMappingCangKu> materialDriverMappingCangKuList) {
        List<YunShuMainAdapter.OrderListItem> items = new ArrayList<>();


        for (int i = 0; i < materialDriverMappingCangKuList.size(); i++) {


            int groupPosition = items.size();
            mGroupsPosition.put(groupPosition, i);

            MaterialDriverMappingCangKu materialDriverMappingCangKu = materialDriverMappingCangKuList.get(i);

            groups.add(new MaterialDriverMappingCangKu(groupPosition + "", materialDriverMappingCangKu.getFromstorename(), materialDriverMappingCangKu.getFromstoreaddress()));
            items.add(new YunShuMainAdapter.OrderListItem(materialDriverMappingCangKu));

            List<MaterialDriverMappingChanPin> materialDriverMappingChanPinList = materialDriverMappingCangKu.getTasks();

            List<MaterialDriverMappingChanPin> products = new ArrayList<MaterialDriverMappingChanPin>();


            for (int i1 = 0; i1 < materialDriverMappingChanPinList.size(); i1++) {

                MaterialDriverMappingChanPin materialDriverMappingChanPin = materialDriverMappingChanPinList.get(i1);


                products.add(materialDriverMappingChanPin);

                items.add(new YunShuMainAdapter.OrderListItem(groupPosition, materialDriverMappingChanPin, i1));
            }

            children.put(String.valueOf(groupPosition), products);// 将组元素的一个唯一值，这里取Id，作为子元素List的Key

        }
        items.add(new YunShuMainAdapter.OrderListItem("", "", "", ""));

        return items;
    }

    @Override
    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {

    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {
        if (i == 1000) {
            double latitude = geocodeResult.getGeocodeAddressList().get(0).getLatLonPoint().getLatitude();
            double longitude = geocodeResult.getGeocodeAddressList().get(0).getLatLonPoint().getLongitude();
            String toAddress = geocodeResult.getGeocodeAddressList().get(0).getFormatAddress();

            LatLng latLng = new LatLng(latitude, longitude);//聊城
            poiList.add(new Poi(toAddress, latLng, ""));

            mSharedPreferencesHelper.putListData("poiList", poiList);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 101) {
            Bundle bundle = data.getExtras();
            String mid = bundle.getString("scan_result");
            setIsScan(mid);
            transportScanBegin(mid);
        }
    }

    /**
     * 设置是否扫描
     *
     * @param mid
     */
    private void setIsScan(String mid) {
        for (int k = 0; k < groups.size(); k++) {
            MaterialDriverMappingCangKu group = groups.get(k);
            List<MaterialDriverMappingChanPin> childs = children.get(group.getId());
            for (int i = 0; i < childs.size(); i++) {
                // 不全选中
                if (childs.get(i).getMid().equals(mid)) {
                    childs.get(i).setIsbeginscan("1");
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void transportScanBegin(String mid) {

        showLoading();

        String methodName = "TransportScanBegin";

        SoapUtil soapUtil = SoapUtil.getInstance(this);
        soapUtil.setDotNet(true);
        SoapParams params = new SoapParams();
        params.put("driverId", mSharedPreferencesHelper.getString(BaseConstants.UID));
        params.put("mid", mid);

        /**
         * 请求
         */
        soapUtil.callEntry(getUrl(), NAME_SPACE, methodName, params, new SoapListener() {
            @Override
            public void onSuccess(int statusCode, SoapObject object) {
                hideLoading();

                ArmsUtils.snackbarText("扫描成功");
            }

            @Override
            public void onFailure(int statusCode, String content, Throwable error) {
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, SoapFault fault) {
                hideLoading();
            }
        }, getAapiUserName(), getApiPwd());


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        if (requestCode == 103) {
            scan();
        }
    }

    @OnClick(R.id.btnRight)
    void scan() {
        if (getPermission()) {

            Intent newIntent = new Intent(this, ScanActivity.class);
            mSharedPreferencesHelper.putInt(SCAN_REQUEST_CODE, 101);
            // 开始一个新的 Activity等候返回结果
            startActivityForResult(newIntent, 101);
        }

    }

    private boolean getPermission() {
        if (EasyPermissions.hasPermissions(this, permissions)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(this, "需要获取您的照相使用权限", 103, permissions);
            return false;
        }

    }

    @Override
    public void checkGroup(int groupPosition, boolean isChecked) {


        MaterialDriverMappingCangKu group = groups.get(groupPosition);
        List<MaterialDriverMappingChanPin> childs = children.get(group.getId());
        for (int i = 0; i < childs.size(); i++) {
            childs.get(i).setChoosed(isChecked);
        }
        if (isAllCheck()) {
//            allChekbox.setChecked(true);
        } else {
//            allChekbox.setChecked(false);
        }

        mAdapter.notifyDataSetChanged();
    }

    private boolean isAllCheck() {

        for (MaterialDriverMappingCangKu group : groups) {
            if (!group.isChoosed())
                return false;
        }
        return true;
    }

    @Override
    public void checkChild(int groupPosition, int childPosition, boolean isChecked) {

        boolean allChildSameState = true;// 判断改组下面的所有子元素是否是同一种状态
        MaterialDriverMappingCangKu group = groups.get(mGroupsPosition.get(groupPosition));

        List<MaterialDriverMappingChanPin> childs = children.get(group.getId());
        for (int i = 0; i < childs.size(); i++) {
            // 不全选中
            if (childs.get(i).isChoosed() != isChecked) {
                allChildSameState = false;
                break;
            }
        }
        //获取店铺选中商品的总金额
        if (allChildSameState) {
            group.setChoosed(isChecked);// 如果所有子元素状态相同，那么对应的组元素被设为这种统一状态
        } else {
            group.setChoosed(false);// 否则，组元素一律设置为未选中状态
        }

//        group.setChoosed(true);

        if (isAllCheck() && allChildSameState) {
//            allChekbox.setChecked(true);// 全选
        } else {
//            allChekbox.setChecked(false);// 反选
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void wangCheng() {

        if (!isAllScan()) {
            ArmsUtils.snackbarText("还有物料未扫描！");
            return;
        }

        if (!isAllCheck()) {
            ArmsUtils.snackbarText("还有选项未确认！");
            return;
        }
        transportBegin();
    }

    private boolean isAllScan() {
        for (int k = 0; k < groups.size(); k++) {
            MaterialDriverMappingCangKu group = groups.get(k);
            List<MaterialDriverMappingChanPin> childs = children.get(group.getId());
            for (int i = 0; i < childs.size(); i++) {
                // 不全选中
                if (childs.get(i).getIsbeginscan().equals("0")) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 6.物料都扫描完毕,装车开始运输调用的方法
     */
    private void transportBegin() {

        showLoading();

        String methodName = "TransportBegin";

        SoapUtil soapUtil = SoapUtil.getInstance(this);
        soapUtil.setDotNet(true);
        SoapParams params = new SoapParams();
        params.put("driverId", mSharedPreferencesHelper.getString(BaseConstants.UID));

        /**
         * 请求
         */
        soapUtil.callEntry(getUrl(), NAME_SPACE, methodName, params, new SoapListener() {
            @Override
            public void onSuccess(int statusCode, SoapObject object) {
                hideLoading();
                ArmsUtils.snackbarText("开始运输");
//                setChuKuTrue();
                Utils.navigation(YunShuChuKuMainActivity.this, RouterHub.APP_YUN_SHU_IN_TRANSIT_ACTIVITY);
                killMyself();
            }

            @Override
            public void onFailure(int statusCode, String content, Throwable error) {
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, SoapFault fault) {
                hideLoading();
            }
        }, getAapiUserName(), getApiPwd());


    }

    private void setChuKuTrue() {
        mSharedPreferencesHelper.putBoolean(BaseConstants.IS_CHU_KU, true);
    }

    /**
     * 是否出库，true 已出库 false 未出库
     * 出库之后执行到达目的地之后的方法，反之执行出库相关的方法
     *
     * @return
     */
    private boolean isChuKu() {
        return mSharedPreferencesHelper.getBoolean(BaseConstants.IS_CHU_KU);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

        Toast.makeText(this, "相关权限获取成功", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Toast.makeText(this, "请同意相关权限，否则功能无法使用", Toast.LENGTH_SHORT).show();
    }
}
