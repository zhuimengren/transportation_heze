package com.xiaoxing.yunshu.mvp.contract;

import com.jess.arms.mvp.IModel;
import com.jess.arms.mvp.IView;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadedVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.StartDistibute;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPeiXiangQing;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;

public interface YiFenPeiXiangQingContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        void getYiFenPeiXiangQingDataSuccess(YiFenPeiXiangQing entityList);

        void loadScanVerifySuccess(LoadScanVerify entityList);

        void loadMaterialPhotoVerifySuccess(LoadMaterialPhotoVerify entityList);

        void loadPhotoVerifySuccess(LoadPhotoVerify entityList);

        void loadedVerifySuccess(LoadedVerify entityList);

        void startDistibuteSuccess(StartDistibute entityList);
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        Observable<YiFenPeiXiangQing> getYiFenPeiXiangQingList(Map<String, String> map);

        Observable<LoadScanVerify> loadScanVerify(Map<String, String> map);

        Observable<LoadMaterialPhotoVerify> loadMaterialPhotoVerify(RequestBody info);

        Observable<LoadPhotoVerify> loadPhotoVerify(RequestBody info);

        Observable<LoadedVerify> loadedVerify(Map<String, String> map);

        Observable<StartDistibute> startDistibute(Map<String, String> map);
    }
}
