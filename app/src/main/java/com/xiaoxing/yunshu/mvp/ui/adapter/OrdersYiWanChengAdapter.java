package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.OrdersYiWanCheng;

import java.util.List;


public class OrdersYiWanChengAdapter extends BaseQuickAdapter<OrdersYiWanCheng.DataBean, BaseViewHolder> {

    private Context mContext;

    public OrdersYiWanChengAdapter(Context context, @Nullable List<OrdersYiWanCheng.DataBean> data) {
        super(R.layout.item_orders_yi_wan_cheng, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, OrdersYiWanCheng.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));

        helper.setText(R.id.tv_qiang_dan_dan_hao, "抢单单号：" + item.getGrabNo());
        helper.setText(R.id.tv_jia_ge, "抢单价格：" + item.getRealPrice());
        helper.setText(R.id.tv_si_ji_xing_ming, "司机姓名：" + item.getDriverName());
        helper.setText(R.id.tv_che_pai_hao, "车牌号：" + item.getTruckNumber());
        helper.setText(R.id.tv_bei_zhu, "备注：" + item.getRemark());
    }

}
