package com.xiaoxing.yunshu.mvp.ui.entity;

public class LoginReceiver {


    /**
     * id : 2
     * mobile_phone : 15965561796
     * plate_number : null
     * display_name : 徐星
     * login_name : null
     * login_pwd : null
     * car_type : null
     * car_load : null
     * company : null
     * num : 0
     */

    private int id;
    private String mobile_phone;
    private Object plate_number;
    private String display_name;
    private Object login_name;
    private Object login_pwd;
    private Object car_type;
    private Object car_load;
    private Object company;
    private String apiUserName;
    private String apiPwd;
    private int num;

    public String getApiUserName() {
        return apiUserName == null ? "" : apiUserName;
    }

    public void setApiUserName(String apiUserName) {
        this.apiUserName = apiUserName == null ? "" : apiUserName;
    }

    public String getApiPwd() {
        return apiPwd == null ? "" : apiPwd;
    }

    public void setApiPwd(String apiPwd) {
        this.apiPwd = apiPwd == null ? "" : apiPwd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public Object getPlate_number() {
        return plate_number;
    }

    public void setPlate_number(Object plate_number) {
        this.plate_number = plate_number;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public Object getLogin_name() {
        return login_name;
    }

    public void setLogin_name(Object login_name) {
        this.login_name = login_name;
    }

    public Object getLogin_pwd() {
        return login_pwd;
    }

    public void setLogin_pwd(Object login_pwd) {
        this.login_pwd = login_pwd;
    }

    public Object getCar_type() {
        return car_type;
    }

    public void setCar_type(Object car_type) {
        this.car_type = car_type;
    }

    public Object getCar_load() {
        return car_load;
    }

    public void setCar_load(Object car_load) {
        this.car_load = car_load;
    }

    public Object getCompany() {
        return company;
    }

    public void setCompany(Object company) {
        this.company = company;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
