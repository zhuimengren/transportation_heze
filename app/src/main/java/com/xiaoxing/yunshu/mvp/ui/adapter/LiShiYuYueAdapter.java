package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.support.annotation.Nullable;
import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.mvp.ui.entity.LiShiYuYue;

import java.util.List;

import com.xiaoxing.yunshu.R;


public class LiShiYuYueAdapter extends BaseQuickAdapter<LiShiYuYue, BaseViewHolder> {

    private Context mContext;

    public LiShiYuYueAdapter(Context context, @Nullable List<LiShiYuYue> data) {
        super(R.layout.item_li_shi_yu_yue, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, LiShiYuYue item) {

        helper.setText(R.id.tv_xing_hao, item.getUid());
        helper.setText(R.id.tv_ri_qi, item.getShiJian());
        helper.setText(R.id.tv_shu_liang, item.getRetId());
        helper.setText(R.id.tv_zhuang_tai, item.getZhuangTai().equals("1") ? "未取" : "已取");
    }

}
