package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Poi;
import com.amap.api.navi.AmapNaviPage;
import com.amap.api.navi.AmapNaviParams;
import com.amap.api.navi.AmapNaviType;
import com.amap.api.navi.INaviInfoCallback;
import com.amap.api.navi.model.AMapNaviLocation;
import com.baidu.trace.model.OnTraceListener;
import com.baidu.trace.model.PushMessage;
import com.dds.soap.SoapListener;
import com.dds.soap.SoapParams;
import com.dds.soap.SoapUtil;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.base.BaseConstants;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.jess.arms.utils.LogUtils;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.app.MyApplication;
import com.xiaoxing.yunshu.di.component.DaggerInTransitComponent;
import com.xiaoxing.yunshu.di.module.InTransitModule;
import com.xiaoxing.yunshu.mvp.contract.InTransitContract;
import com.xiaoxing.yunshu.mvp.presenter.InTransitPresenter;

import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;

import java.util.List;

import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;

import static com.jess.arms.base.BaseConstants.NAME_SPACE;
import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.APP_YUN_SHU_IN_TRANSIT_ACTIVITY)
public class InTransitActivity extends BaseActivity<InTransitPresenter> implements InTransitContract.View, INaviInfoCallback {
    public static int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;
    LatLng p1 = new LatLng(36.074670, 120.377450);//深业中心大厦
    LatLng p2 = new LatLng(36.264680, 120.033382);//胶州
    LatLng p3 = new LatLng(36.706860, 119.161760);//潍坊
    LatLng p4 = new LatLng(36.553580, 116.75199);//济南
    LatLng p5 = new LatLng(36.457020, 115.98549);//聊城
    private MyApplication trackApp = null;
    /**
     * 初始化监听器
     */

    // 初始化轨迹服务监听器
    OnTraceListener mTraceListener = new OnTraceListener() {
        @Override
        public void onBindServiceCallback(int i, String s) {

            LogUtils.debugInfo("onBindServiceCallback");
        }

        // 开启服务回调
        @Override
        public void onStartTraceCallback(int status, String message) {
            LogUtils.debugEInfo("onStartTraceCallback");
            // 开启采集
            trackApp.mClient.startGather(mTraceListener);
        }

        // 停止服务回调
        @Override
        public void onStopTraceCallback(int status, String message) {
            LogUtils.debugEInfo("onStopTraceCallback");
        }

        // 开启采集回调
        @Override
        public void onStartGatherCallback(int status, String message) {
            LogUtils.debugEInfo("onStartGatherCallback");
        }

        // 停止采集回调
        @Override
        public void onStopGatherCallback(int status, String message) {
            LogUtils.debugEInfo("onStopGatherCallback");
        }

        // 推送回调
        @Override
        public void onPushCallback(byte messageNo, PushMessage message) {
            LogUtils.debugEInfo("onPushCallback");
        }

        @Override
        public void onInitBOSCallback(int i, String s) {
            LogUtils.debugEInfo("onInitBOSCallback");

        }
    };

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerInTransitComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .inTransitModule(new InTransitModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_in_transit; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {


        trackApp = (MyApplication) MyApplication.getContext();
        Toolbar toolbar = ToolbarUtils.initToolbarTitleNoBack(this, "运输中");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Utils.navigation(InTransitActivity.this, RouterHub.APP_YUN_SHU_TRACING_ACTIVITY);
//            }
//        });
        initTrace();

    }

    private void initTrace() {
//                mSharedPreferencesHelper.getString(BaseConstants.COMPANY)
//                + "_"
//                +
        String entityname = mSharedPreferencesHelper.getString(BaseConstants.DISPLAY_NAME) + "_" + mSharedPreferencesHelper.getString(BaseConstants.MOBILE_PHONE);
        LogUtils.debugEInfo("entityname==" + entityname);
        trackApp.mTrace.setEntityName(entityname);
        //开启轨迹追踪
        // 开启服务
        trackApp.mClient.startTrace(trackApp.mTrace, mTraceListener);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 导航
     */
    @OnClick(R.id.btn_navigation)
    void navigation() {

//        List<Poi> poiList = new ArrayList();
//        poiList.add(new Poi("聊城", p5, ""));
//        poiList.add(new Poi("胶州", p2, ""));
//        poiList.add(new Poi("潍坊", p3, ""));

        List<Poi> poiList = mSharedPreferencesHelper.getListData("poiList", Poi.class);

        AmapNaviParams params = new AmapNaviParams(poiList.get(0), poiList, poiList.get(poiList.size() - 1), AmapNaviType.DRIVER);
        params.setUseInnerVoice(true);
        AmapNaviPage.getInstance().showRouteActivity(getApplicationContext(), params, InTransitActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void setYunShuJieShuTrue() {
        mSharedPreferencesHelper.putBoolean(BaseConstants.YUN_SHU_JIE_SHU, true);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    @OnClick(R.id.btn_que_ding)
    public void onViewClicked() {
        showConfirmMessageDialog();
    }

    private void showConfirmMessageDialog() {
        new QMUIDialog.CheckBoxMessageDialogBuilder(InTransitActivity.this)
                .setTitle("运输结束")
                .setMessage("确认运输结束?")
                .setChecked(true)
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {

                        transportFinish();
                        dialog.dismiss();

                    }
                })
                .create(mCurrentDialogStyle).show();
    }

    /**
     * 7.整车到达目的地,调用此方法
     */
    private void transportFinish() {

        showLoading();

        String methodName = "TransportFinish";

        SoapUtil soapUtil = SoapUtil.getInstance(this);
        soapUtil.setDotNet(true);
        SoapParams params = new SoapParams();
        params.put("driverId", mSharedPreferencesHelper.getString(BaseConstants.UID));

        /**
         * 请求
         */
        soapUtil.callEntry(getUrl(), NAME_SPACE, methodName, params, new SoapListener() {
            @Override
            public void onSuccess(int statusCode, SoapObject object) {
                hideLoading();
                ArmsUtils.snackbarText("运输结束");
                stopYingYan();
//                setYunShuJieShuTrue();
                Utils.navigation(InTransitActivity.this, RouterHub.APP_LOGIN_ACTIVITY);
//                killMyself();
            }

            @Override
            public void onFailure(int statusCode, String content, Throwable error) {
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, SoapFault fault) {
                hideLoading();
            }
        }, getAapiUserName(), getApiPwd());


    }

    @Override
    public void showLoading() {

    }

    private void stopYingYan() {

        stopTrace();
        stopGather();
    }

    private void stopTrace() {
        // 停止服务
        trackApp.mClient.stopTrace(trackApp.mTrace, mTraceListener);
    }

    private void stopGather() {
        // 停止采集
        trackApp.mClient.stopGather(mTraceListener);
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @Override
    public void onInitNaviFailure() {

    }

    @Override
    public void onGetNavigationText(String s) {

    }

    @Override
    public void onLocationChange(AMapNaviLocation aMapNaviLocation) {

    }

    @Override
    public void onArriveDestination(boolean b) {

    }

    @Override
    public void onStartNavi(int i) {

    }

    @Override
    public void onCalculateRouteSuccess(int[] ints) {

    }

    @Override
    public void onCalculateRouteFailure(int i) {

    }

    @Override
    public void onStopSpeaking() {

    }

    @Override
    public void onReCalculateRoute(int i) {

    }

    @Override
    public void onExitPage(int i) {

    }

    @Override
    public void onStrategyChanged(int i) {

    }

    @Override
    public View getCustomNaviBottomView() {
        return null;
    }

    @Override
    public View getCustomNaviView() {
        return null;
    }

    @Override
    public void onArrivedWayPoint(int i) {

    }
}
