package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerForgotPwdComponent;
import com.xiaoxing.yunshu.di.module.ForgotPwdModule;
import com.xiaoxing.yunshu.mvp.contract.ForgotPwdContract;
import com.xiaoxing.yunshu.mvp.presenter.ForgotPwdPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgetPwdSendValidateCode;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgotPwd;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.CountDownButton;
import me.jessyan.armscomponent.commonres.view.XEditText;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_FORGOT_PWD)
public class ForgotPwdActivity extends BaseActivity<ForgotPwdPresenter> implements ForgotPwdContract.View {


    @BindView(R.id.xet_phone)
    XEditText xetPhone;
    @BindView(R.id.xet_verification_code)
    XEditText xetVerificationCode;
    @BindView(R.id.cd_btn)
    CountDownButton cdBtn;
    @BindView(R.id.xet_pwd)
    XEditText xetPwd;
    @BindView(R.id.xet_pwd_again)
    XEditText xetPwdAgain;
    @BindView(R.id.btn_register)
    Button btnRegister;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerForgotPwdComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .forgotPwdModule(new ForgotPwdModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_forgot_pwd; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_forgot_pwd));
        //getForgotPwdData();
        cdBtn.setEnabled(!TextUtils.isEmpty(xetPhone.getText()));

        xetPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cdBtn.setEnabled(!TextUtils.isEmpty(s.toString()));// && !mCountDownButton.isCountDownNow()
                if (!TextUtils.isEmpty(s.toString())) {
                    cdBtn.setBackground(getResources().getDrawable(R.drawable.public_shape_blue_bg_corner_5dp));
                } else {
                    cdBtn.setBackground(getResources().getDrawable(R.drawable.public_shape_gray_bg_corner_5dp));
                }
            }
        });
    }

    @Override
    public void getForgotPwdDataSuccess(ForgotPwd entityData) {

        showMessage(entityData.getMessage());
        if (entityData.getCode() == 200) {

            killMyself();
        }

    }

    @Override
    public void forgetPwdSendValidateCodeSuccess(ForgetPwdSendValidateCode entityData) {
        try {
            if (entityData.getCode() == 200) {
                showMessage("发送成功");
            } else {
                showMessage(entityData.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick({R.id.cd_btn, R.id.btn_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cd_btn:
                onSendCodeViewClicked();
                break;
            case R.id.btn_register:

                if (TextUtils.isEmpty(getPhone())) {
                    showMessage("请输入手机号");
                    return;
                }
                if (TextUtils.isEmpty(getVerificationCode())) {
                    showMessage("请输入验证码");
                    return;
                }

                if (TextUtils.isEmpty(getPwd())) {
                    showMessage("请输入密码");
                    return;
                }
                if (TextUtils.isEmpty(getPwdAgain())) {
                    showMessage("请输入确认密码");
                    return;
                }
                if (!getPwd().equals(getPwdAgain())) {
                    showMessage("两次密码输入不一致");
                    return;
                }

                getForgotPwdData();
                break;
        }
    }

    public void onSendCodeViewClicked() {
        HashMap<String, String> map = new HashMap<>();
        map.put("mobilePhone", getPhone());
        mPresenter.forgetPwdSendValidateCode(map);

    }

    private String getPhone() {
        return xetPhone.getText().toString();
    }

    private String getVerificationCode() {
        return xetVerificationCode.getText().toString();
    }

    private String getPwd() {
        return xetPwd.getText().toString();
    }

    private String getPwdAgain() {
        return xetPwdAgain.getText().toString();
    }

    private void getForgotPwdData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("password", getPwd());
        map.put("mobilePhone", getPhone());
        map.put("validateCode", getVerificationCode());
        mPresenter.getForgotPwdData(map);
    }
}
