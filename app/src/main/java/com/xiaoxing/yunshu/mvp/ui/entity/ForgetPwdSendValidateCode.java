package com.xiaoxing.yunshu.mvp.ui.entity;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2020/2/11 0011 14:36
 */
public class ForgetPwdSendValidateCode {


    /**
     * Code : 200
     * Message : 验证码发送成功！
     */

    private int Code;
    private String Message;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}
