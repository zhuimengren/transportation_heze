package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPeiXiangQing;

import java.util.List;


public class YiFenPeiXiangQingAdapter extends BaseQuickAdapter<YiFenPeiXiangQing.DataBean, BaseViewHolder> {

    private Context mContext;

    public YiFenPeiXiangQingAdapter(Context context, @Nullable List<YiFenPeiXiangQing.DataBean> data) {
        super(R.layout.item_yi_fen_pei_xiang_qing, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, YiFenPeiXiangQing.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
    }

}
