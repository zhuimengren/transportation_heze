package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanDetail;

import java.util.List;


public class QiangDanDetailAdapter extends BaseQuickAdapter<QiangDanDetail.DataBean, BaseViewHolder> {

    private Context mContext;

    public QiangDanDetailAdapter(Context context, @Nullable List<QiangDanDetail.DataBean> data) {
        super(R.layout.item_qiang_dan_detail, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, QiangDanDetail.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
    }

}
