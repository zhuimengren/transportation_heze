package com.xiaoxing.yunshu.mvp.ui.entity;

public class LiShiYuYue {

    private String id;
    private String uid;
    private String retId;
    private String xingHao;
    private String shiJian;
    private String shuLiang;
    private String zhuangTai;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRetId() {
        return retId;
    }

    public void setRetId(String retId) {
        this.retId = retId;
    }

    public String getXingHao() {
        return xingHao;
    }

    public void setXingHao(String xingHao) {
        this.xingHao = xingHao;
    }

    public String getShiJian() {
        return shiJian;
    }

    public void setShiJian(String shiJian) {
        this.shiJian = shiJian;
    }

    public String getShuLiang() {
        return shuLiang;
    }

    public void setShuLiang(String shuLiang) {
        this.shuLiang = shuLiang;
    }

    public String getZhuangTai() {
        return zhuangTai;
    }

    public void setZhuangTai(String zhuangTai) {
        this.zhuangTai = zhuangTai;
    }
}
