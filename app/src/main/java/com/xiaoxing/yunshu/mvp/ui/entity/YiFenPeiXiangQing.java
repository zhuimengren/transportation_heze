package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class YiFenPeiXiangQing {


    /**
     * Data : {"GrabNo":"PG202002060001","RefPrice":700,"Remark":"装卸信息","GrabStatus":4,"GrabStatusDesc":"已分配","DriverCount":1,"DriverId":180,"DistributeDateTime":"2020-02-13 09:48:07","DistributeUserId":174,"DistributeUserName":"配送管理员","PublishUserId":174,"PublishUserName":"配送管理员","PublishDateTime":"2020-02-06 10:37:07","FinishUserId":174,"FinishUserName":"配送管理员","FinishDateTime":"2020-02-13 09:47:11","InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-06 10:35:37","Tasks":[{"TaskNo":"PT202002050005","TruckId":3,"TruckNumber":"1111","DriverId":180,"DriverName":"xx","DriverPhone":"15965561796","DeliverAddress":"1","GetAddress":"延吉路175号南京路仓库","OutStoreId":44,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":4,"IsFromOutStorage":true,"GrabNo":"PG202002060001","InsertUserId":174,"InsertDateTime":"2020-02-05 12:12:23","OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库"},"Items":[{"ItemId":14,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":2000,"MaterialUnit":"件","IsStartVerify":false,"IsEndVerify":false},{"ItemId":15,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,50mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":3000,"MaterialUnit":"件","IsStartVerify":false,"IsEndVerify":false}]},{"TaskNo":"PT202002060001","TruckId":3,"TruckNumber":"1111","DriverId":180,"DriverName":"xx","DriverPhone":"15965561796","DeliverAddress":"1","GetAddress":"延吉路175号南京路仓库","OutStoreId":44,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":4,"IsFromOutStorage":true,"GrabNo":"PG202002060001","InsertUserId":174,"InsertDateTime":"2020-02-06 10:32:01","OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库"},"Items":[{"ItemId":22,"TaskNo":"PT202002060001","MaterialName":"螺杆,等长双头,M18,350mm,热浸锌","MaterialNum":132,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false},{"ItemId":23,"TaskNo":"PT202002060001","MaterialName":"螺杆,等长双头,M20,380mm,热浸锌","MaterialNum":8,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false},{"ItemId":24,"TaskNo":"PT202002060001","MaterialName":"螺杆,等长双头,M20,420mm,热浸锌","MaterialNum":60,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false},{"ItemId":25,"TaskNo":"PT202002060001","MaterialName":"普通螺栓,M18,100mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":528,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false}]}]}
     * Code : 200
     * Message : 获取抢单详细信息成功！
     */

    private DataBean Data;
    private int Code;
    private String Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * GrabNo : PG202002060001
         * RefPrice : 700.0
         * Remark : 装卸信息
         * GrabStatus : 4
         * GrabStatusDesc : 已分配
         * DriverCount : 1
         * DriverId : 180
         * DistributeDateTime : 2020-02-13 09:48:07
         * DistributeUserId : 174
         * DistributeUserName : 配送管理员
         * PublishUserId : 174
         * PublishUserName : 配送管理员
         * PublishDateTime : 2020-02-06 10:37:07
         * FinishUserId : 174
         * FinishUserName : 配送管理员
         * FinishDateTime : 2020-02-13 09:47:11
         * InsertUserId : 174
         * InsertUserName : 配送管理员
         * InsertDateTime : 2020-02-06 10:35:37
         * Tasks : [{"TaskNo":"PT202002050005","TruckId":3,"TruckNumber":"1111","DriverId":180,"DriverName":"xx","DriverPhone":"15965561796","DeliverAddress":"1","GetAddress":"延吉路175号南京路仓库","OutStoreId":44,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":4,"IsFromOutStorage":true,"GrabNo":"PG202002060001","InsertUserId":174,"InsertDateTime":"2020-02-05 12:12:23","OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库"},"Items":[{"ItemId":14,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":2000,"MaterialUnit":"件","IsStartVerify":false,"IsEndVerify":false},{"ItemId":15,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,50mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":3000,"MaterialUnit":"件","IsStartVerify":false,"IsEndVerify":false}]},{"TaskNo":"PT202002060001","TruckId":3,"TruckNumber":"1111","DriverId":180,"DriverName":"xx","DriverPhone":"15965561796","DeliverAddress":"1","GetAddress":"延吉路175号南京路仓库","OutStoreId":44,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":4,"IsFromOutStorage":true,"GrabNo":"PG202002060001","InsertUserId":174,"InsertDateTime":"2020-02-06 10:32:01","OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库"},"Items":[{"ItemId":22,"TaskNo":"PT202002060001","MaterialName":"螺杆,等长双头,M18,350mm,热浸锌","MaterialNum":132,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false},{"ItemId":23,"TaskNo":"PT202002060001","MaterialName":"螺杆,等长双头,M20,380mm,热浸锌","MaterialNum":8,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false},{"ItemId":24,"TaskNo":"PT202002060001","MaterialName":"螺杆,等长双头,M20,420mm,热浸锌","MaterialNum":60,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false},{"ItemId":25,"TaskNo":"PT202002060001","MaterialName":"普通螺栓,M18,100mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":528,"MaterialUnit":"根","IsStartVerify":false,"IsEndVerify":false}]}]
         */

        private String GrabNo;
        private double RefPrice;
        private String Remark;
        private int GrabStatus;
        private String GrabStatusDesc;
        private int DriverCount;
        private int DriverId;
        private String DistributeDateTime;
        private int DistributeUserId;
        private String DistributeUserName;
        private int PublishUserId;
        private String PublishUserName;
        private String PublishDateTime;
        private int FinishUserId;
        private String FinishUserName;
        private String FinishDateTime;
        private int InsertUserId;
        private String InsertUserName;
        private String InsertDateTime;
        private List<TasksBean> Tasks;

        public String getGrabNo() {
            return GrabNo;
        }

        public void setGrabNo(String GrabNo) {
            this.GrabNo = GrabNo;
        }

        public double getRefPrice() {
            return RefPrice;
        }

        public void setRefPrice(double RefPrice) {
            this.RefPrice = RefPrice;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }

        public int getGrabStatus() {
            return GrabStatus;
        }

        public void setGrabStatus(int GrabStatus) {
            this.GrabStatus = GrabStatus;
        }

        public String getGrabStatusDesc() {
            return GrabStatusDesc;
        }

        public void setGrabStatusDesc(String GrabStatusDesc) {
            this.GrabStatusDesc = GrabStatusDesc;
        }

        public int getDriverCount() {
            return DriverCount;
        }

        public void setDriverCount(int DriverCount) {
            this.DriverCount = DriverCount;
        }

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int DriverId) {
            this.DriverId = DriverId;
        }

        public String getDistributeDateTime() {
            return DistributeDateTime;
        }

        public void setDistributeDateTime(String DistributeDateTime) {
            this.DistributeDateTime = DistributeDateTime;
        }

        public int getDistributeUserId() {
            return DistributeUserId;
        }

        public void setDistributeUserId(int DistributeUserId) {
            this.DistributeUserId = DistributeUserId;
        }

        public String getDistributeUserName() {
            return DistributeUserName;
        }

        public void setDistributeUserName(String DistributeUserName) {
            this.DistributeUserName = DistributeUserName;
        }

        public int getPublishUserId() {
            return PublishUserId;
        }

        public void setPublishUserId(int PublishUserId) {
            this.PublishUserId = PublishUserId;
        }

        public String getPublishUserName() {
            return PublishUserName;
        }

        public void setPublishUserName(String PublishUserName) {
            this.PublishUserName = PublishUserName;
        }

        public String getPublishDateTime() {
            return PublishDateTime;
        }

        public void setPublishDateTime(String PublishDateTime) {
            this.PublishDateTime = PublishDateTime;
        }

        public int getFinishUserId() {
            return FinishUserId;
        }

        public void setFinishUserId(int FinishUserId) {
            this.FinishUserId = FinishUserId;
        }

        public String getFinishUserName() {
            return FinishUserName;
        }

        public void setFinishUserName(String FinishUserName) {
            this.FinishUserName = FinishUserName;
        }

        public String getFinishDateTime() {
            return FinishDateTime;
        }

        public void setFinishDateTime(String FinishDateTime) {
            this.FinishDateTime = FinishDateTime;
        }

        public int getInsertUserId() {
            return InsertUserId;
        }

        public void setInsertUserId(int InsertUserId) {
            this.InsertUserId = InsertUserId;
        }

        public String getInsertUserName() {
            return InsertUserName;
        }

        public void setInsertUserName(String InsertUserName) {
            this.InsertUserName = InsertUserName;
        }

        public String getInsertDateTime() {
            return InsertDateTime;
        }

        public void setInsertDateTime(String InsertDateTime) {
            this.InsertDateTime = InsertDateTime;
        }

        public List<TasksBean> getTasks() {
            return Tasks;
        }

        public void setTasks(List<TasksBean> Tasks) {
            this.Tasks = Tasks;
        }

        public static class TasksBean {
            /**
             * TaskNo : PT202002050005
             * TruckId : 3
             * TruckNumber : 1111
             * DriverId : 180
             * DriverName : xx
             * DriverPhone : 15965561796
             * DeliverAddress : 1
             * GetAddress : 延吉路175号南京路仓库
             * OutStoreId : 44
             * ConsigneeName :  1
             * ConsigneePhone : 1
             * DistributeStatus : 4
             * IsFromOutStorage : true
             * GrabNo : PG202002060001
             * InsertUserId : 174
             * InsertDateTime : 2020-02-05 12:12:23
             * OutStore : {"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库"}
             * Items : [{"ItemId":14,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":2000,"MaterialUnit":"件","IsStartVerify":false,"IsEndVerify":false},{"ItemId":15,"TaskNo":"PT202002050005","MaterialName":"普通螺栓,M16,50mm,钢,镀锌,配平垫弹簧垫螺母","MaterialNum":3000,"MaterialUnit":"件","IsStartVerify":false,"IsEndVerify":false}]
             */

            private String id;
            private String TaskNo;
            private int TruckId;
            private String TruckNumber;
            private int DriverId;
            private String DriverName;
            private String DriverPhone;
            private String DeliverAddress;
            private String GetAddress;
            private String GetUser;
            private String GetUserPhone;
            private int OutStoreId;
            private String ConsigneeName;
            private String ConsigneePhone;
            private int DistributeStatus;
            private boolean IsFromOutStorage;
            private boolean isChoosed;
            private String GrabNo;
            private int InsertUserId;
            private String InsertDateTime;
            private String LoadPhotoId;
            private OutStoreBean OutStore;
            private List<ItemsBean> Items;

            public String getGetUser() {
                return GetUser == null ? "" : GetUser;
            }

            public void setGetUser(String getUser) {
                GetUser = getUser == null ? "" : getUser;
            }

            public String getGetUserPhone() {
                return GetUserPhone == null ? "" : GetUserPhone;
            }

            public void setGetUserPhone(String getUserPhone) {
                GetUserPhone = getUserPhone == null ? "" : getUserPhone;
            }

            public String getLoadPhotoId() {
                return LoadPhotoId == null ? "" : LoadPhotoId;
            }

            public void setLoadPhotoId(String loadPhotoId) {
                LoadPhotoId = loadPhotoId == null ? "" : loadPhotoId;
            }

            public boolean isChoosed() {
                return isChoosed;
            }

            public void setChoosed(boolean choosed) {
                isChoosed = choosed;
            }

            public String getId() {
                return id == null ? "" : id;
            }

            public void setId(String id) {
                this.id = id == null ? "" : id;
            }

            public String getTaskNo() {
                return TaskNo;
            }

            public void setTaskNo(String TaskNo) {
                this.TaskNo = TaskNo;
            }

            public int getTruckId() {
                return TruckId;
            }

            public void setTruckId(int TruckId) {
                this.TruckId = TruckId;
            }

            public String getTruckNumber() {
                return TruckNumber;
            }

            public void setTruckNumber(String TruckNumber) {
                this.TruckNumber = TruckNumber;
            }

            public int getDriverId() {
                return DriverId;
            }

            public void setDriverId(int DriverId) {
                this.DriverId = DriverId;
            }

            public String getDriverName() {
                return DriverName;
            }

            public void setDriverName(String DriverName) {
                this.DriverName = DriverName;
            }

            public String getDriverPhone() {
                return DriverPhone;
            }

            public void setDriverPhone(String DriverPhone) {
                this.DriverPhone = DriverPhone;
            }

            public String getDeliverAddress() {
                return DeliverAddress;
            }

            public void setDeliverAddress(String DeliverAddress) {
                this.DeliverAddress = DeliverAddress;
            }

            public String getGetAddress() {
                return GetAddress;
            }

            public void setGetAddress(String GetAddress) {
                this.GetAddress = GetAddress;
            }

            public int getOutStoreId() {
                return OutStoreId;
            }

            public void setOutStoreId(int OutStoreId) {
                this.OutStoreId = OutStoreId;
            }

            public String getConsigneeName() {
                return ConsigneeName;
            }

            public void setConsigneeName(String ConsigneeName) {
                this.ConsigneeName = ConsigneeName;
            }

            public String getConsigneePhone() {
                return ConsigneePhone;
            }

            public void setConsigneePhone(String ConsigneePhone) {
                this.ConsigneePhone = ConsigneePhone;
            }

            public int getDistributeStatus() {
                return DistributeStatus;
            }

            public void setDistributeStatus(int DistributeStatus) {
                this.DistributeStatus = DistributeStatus;
            }

            public boolean isIsFromOutStorage() {
                return IsFromOutStorage;
            }

            public void setIsFromOutStorage(boolean IsFromOutStorage) {
                this.IsFromOutStorage = IsFromOutStorage;
            }

            public String getGrabNo() {
                return GrabNo;
            }

            public void setGrabNo(String GrabNo) {
                this.GrabNo = GrabNo;
            }

            public int getInsertUserId() {
                return InsertUserId;
            }

            public void setInsertUserId(int InsertUserId) {
                this.InsertUserId = InsertUserId;
            }

            public String getInsertDateTime() {
                return InsertDateTime;
            }

            public void setInsertDateTime(String InsertDateTime) {
                this.InsertDateTime = InsertDateTime;
            }

            public OutStoreBean getOutStore() {
                return OutStore;
            }

            public void setOutStore(OutStoreBean OutStore) {
                this.OutStore = OutStore;
            }

            public List<ItemsBean> getItems() {
                return Items;
            }

            public void setItems(List<ItemsBean> Items) {
                this.Items = Items;
            }

            public static class OutStoreBean {
                /**
                 * ID : 44
                 * StoreName : 南京路仓库
                 * StoreAddress : 延吉路175号南京路仓库
                 */

                private int ID;
                private String StoreName;
                private String StoreAddress;

                public int getID() {
                    return ID;
                }

                public void setID(int ID) {
                    this.ID = ID;
                }

                public String getStoreName() {
                    return StoreName;
                }

                public void setStoreName(String StoreName) {
                    this.StoreName = StoreName;
                }

                public String getStoreAddress() {
                    return StoreAddress;
                }

                public void setStoreAddress(String StoreAddress) {
                    this.StoreAddress = StoreAddress;
                }
            }

            public static class ItemsBean {
                /**
                 * ItemId : 14
                 * TaskNo : PT202002050005
                 * MaterialName : 普通螺栓,M16,120mm,钢,镀锌,配平垫弹簧垫螺母
                 * MaterialNum : 2000.0
                 * MaterialUnit : 件
                 * IsStartVerify : false
                 * IsEndVerify : false
                 */

                private int ItemId;
                private String TaskNo;
                private String MaterialName;
                private double MaterialNum;
                private String MaterialUnit;
                private boolean IsStartVerify;
                private boolean IsEndVerify;
                private boolean IsFromOutStorage;
                private boolean isChoosed;
                private String isbeginscan;

                public String getIsbeginscan() {
                    return isbeginscan == null ? "" : isbeginscan;
                }

                public void setIsbeginscan(String isbeginscan) {
                    this.isbeginscan = isbeginscan == null ? "" : isbeginscan;
                }

                public boolean isChoosed() {
                    return isChoosed;
                }

                public void setChoosed(boolean choosed) {
                    isChoosed = choosed;
                }

                public boolean isFromOutStorage() {
                    return IsFromOutStorage;
                }

                public void setFromOutStorage(boolean fromOutStorage) {
                    IsFromOutStorage = fromOutStorage;
                }

                public int getItemId() {
                    return ItemId;
                }

                public void setItemId(int ItemId) {
                    this.ItemId = ItemId;
                }

                public String getTaskNo() {
                    return TaskNo;
                }

                public void setTaskNo(String TaskNo) {
                    this.TaskNo = TaskNo;
                }

                public String getMaterialName() {
                    return MaterialName;
                }

                public void setMaterialName(String MaterialName) {
                    this.MaterialName = MaterialName;
                }

                public double getMaterialNum() {
                    return MaterialNum;
                }

                public void setMaterialNum(double MaterialNum) {
                    this.MaterialNum = MaterialNum;
                }

                public String getMaterialUnit() {
                    return MaterialUnit;
                }

                public void setMaterialUnit(String MaterialUnit) {
                    this.MaterialUnit = MaterialUnit;
                }

                public boolean isIsStartVerify() {
                    return IsStartVerify;
                }

                public void setIsStartVerify(boolean IsStartVerify) {
                    this.IsStartVerify = IsStartVerify;
                }

                public boolean isIsEndVerify() {
                    return IsEndVerify;
                }

                public void setIsEndVerify(boolean IsEndVerify) {
                    this.IsEndVerify = IsEndVerify;
                }
            }
        }
    }
}
