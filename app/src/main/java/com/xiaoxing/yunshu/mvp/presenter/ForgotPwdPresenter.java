package com.xiaoxing.yunshu.mvp.presenter;

import android.app.Application;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;
import com.xiaoxing.yunshu.mvp.contract.ForgotPwdContract;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgetPwdSendValidateCode;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgotPwd;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class ForgotPwdPresenter extends BasePresenter<ForgotPwdContract.Model, ForgotPwdContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public ForgotPwdPresenter(ForgotPwdContract.Model model, ForgotPwdContract.View rootView) {
        super(model, rootView);
    }

    public void getForgotPwdData(Map<String, String> map) {

        mModel.getForgotPwdData(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<ForgotPwd>(mErrorHandler) {
                    @Override
                    public void onNext(ForgotPwd entityData) {
                        mRootView.getForgotPwdDataSuccess(entityData);
                    }
                });
    }

    public void forgetPwdSendValidateCode(Map<String, String> map) {

        mModel.forgetPwdSendValidateCode(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<ForgetPwdSendValidateCode>(mErrorHandler) {
                    @Override
                    public void onNext(ForgetPwdSendValidateCode entityData) {
                        mRootView.forgetPwdSendValidateCodeSuccess(entityData);
                    }
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
