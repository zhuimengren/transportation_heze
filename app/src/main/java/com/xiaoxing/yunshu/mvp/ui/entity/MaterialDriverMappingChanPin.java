package com.xiaoxing.yunshu.mvp.ui.entity;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2018/10/11 0011 16:06
 */
public class MaterialDriverMappingChanPin {
    private String material;
    private String receivephone;
    private String receiveusername;
    private String targetproject;
    private String fromstoreaddress;
    private String fromstorename;
    private String fromspacename;
    private String fromspaceaddress;
    private String tostoreaddress;
    private String drivername;
    private String id;
    private String mid;
    private String driverid;
    private String isactive;
    private String isbeginscan;
    private String isendscan;
    private String isontransport;
    private String num;
    private String assigntime;
    private String begintime;
    private String endtime;
    private boolean isChoosed;
    private boolean isScan;

    public boolean isScan() {
        return isScan;
    }

    public void setScan(boolean scan) {
        isScan = scan;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getReceivephone() {
        return receivephone;
    }

    public void setReceivephone(String receivephone) {
        this.receivephone = receivephone;
    }

    public String getReceiveusername() {
        return receiveusername;
    }

    public void setReceiveusername(String receiveusername) {
        this.receiveusername = receiveusername;
    }

    public String getTargetproject() {
        return targetproject;
    }

    public void setTargetproject(String targetproject) {
        this.targetproject = targetproject;
    }

    public String getFromstoreaddress() {
        return fromstoreaddress;
    }

    public void setFromstoreaddress(String fromstoreaddress) {
        this.fromstoreaddress = fromstoreaddress;
    }

    public String getFromstorename() {
        return fromstorename;
    }

    public void setFromstorename(String fromstorename) {
        this.fromstorename = fromstorename;
    }

    public String getFromspacename() {
        return fromspacename;
    }

    public void setFromspacename(String fromspacename) {
        this.fromspacename = fromspacename;
    }

    public String getFromspaceaddress() {
        return fromspaceaddress;
    }

    public void setFromspaceaddress(String fromspaceaddress) {
        this.fromspaceaddress = fromspaceaddress;
    }

    public String getTostoreaddress() {
        return tostoreaddress;
    }

    public void setTostoreaddress(String tostoreaddress) {
        this.tostoreaddress = tostoreaddress;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getIsbeginscan() {
        return isbeginscan;
    }

    public void setIsbeginscan(String isbeginscan) {
        this.isbeginscan = isbeginscan;
    }

    public String getIsendscan() {
        return isendscan;
    }

    public void setIsendscan(String isendscan) {
        this.isendscan = isendscan;
    }

    public String getIsontransport() {
        return isontransport;
    }

    public void setIsontransport(String isontransport) {
        this.isontransport = isontransport;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getAssigntime() {
        return assigntime;
    }

    public void setAssigntime(String assigntime) {
        this.assigntime = assigntime;
    }

    public String getBegintime() {
        return begintime;
    }

    public void setBegintime(String begintime) {
        this.begintime = begintime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}
