package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.contract.GrabOrdersContract;


import com.xiaoxing.yunshu.mvp.ui.entity.GrabOrders;

import io.reactivex.Observable;


@ActivityScope
public class GrabOrdersModel extends BaseModel implements GrabOrdersContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public GrabOrdersModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<GrabOrders> getGrabOrdersData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getGrabOrdersData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}