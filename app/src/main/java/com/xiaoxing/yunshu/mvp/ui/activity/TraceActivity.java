package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.baidu.trace.LBSTraceClient;
import com.baidu.trace.Trace;
import com.baidu.trace.model.OnTraceListener;
import com.baidu.trace.model.PushMessage;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.jess.arms.utils.LogUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.app.MyApplication;
import com.xiaoxing.yunshu.di.component.DaggerTraceComponent;
import com.xiaoxing.yunshu.di.module.TraceModule;
import com.xiaoxing.yunshu.mvp.contract.TraceContract;
import com.xiaoxing.yunshu.mvp.presenter.TracePresenter;

import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import com.jess.arms.base.BaseConstants;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.APP_TRACE_ACTIVITY)
public class TraceActivity extends BaseActivity<TracePresenter> implements TraceContract.View {

    /**
     * 初始化轨迹服务
     */
    // 轨迹服务ID
//    long serviceId = 205291;
    long serviceId = BaseConstants.SERVICE_ID;
    // 设备标识
    String entityName = "myTrace";
    // 是否需要对象存储服务，默认为：false，关闭对象存储服务。注：鹰眼 Android SDK v3.0以上版本支持随轨迹上传图像等对象数据，若需使用此功能，该参数需设为 true，且需导入bos-android-sdk-1.0.2.jar。
    boolean isNeedObjectStorage = false;
    // 初始化轨迹服务
    Trace mTrace = new Trace(serviceId, entityName, isNeedObjectStorage);
    // 初始化轨迹服务客户端
    LBSTraceClient mTraceClient = new LBSTraceClient(MyApplication.getContext());

    /**
     * 设置定位和回传周期
     */

    // 定位周期(单位:秒)
    int gatherInterval = 5;
    // 打包回传周期(单位:秒)
    int packInterval = 10;


    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerTraceComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .traceModule(new TraceModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_trace; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleNoBack(this, "任务");


        initTrace();

    }

    private void initTrace() {

        // 设置定位和打包周期
        mTraceClient.setInterval(gatherInterval, packInterval);

        //开启轨迹追踪
        // 开启服务
        mTraceClient.startTrace(mTrace, mTraceListener);
        // 开启采集
        mTraceClient.startGather(mTraceListener);
    }

    private void stopTrace() {
        // 停止服务
        mTraceClient.stopTrace(mTrace, mTraceListener);
    }

    private void stopGather() {
        // 停止采集
        mTraceClient.stopGather(mTraceListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopTrace();
        stopGather();
    }

    /**
     * 初始化监听器
     */

    // 初始化轨迹服务监听器
    OnTraceListener mTraceListener = new OnTraceListener() {
        @Override
        public void onBindServiceCallback(int i, String s) {

            ArmsUtils.makeText(TraceActivity.this, "onBindServiceCallback");
            LogUtils.debugInfo("onBindServiceCallback");
        }

        // 开启服务回调
        @Override
        public void onStartTraceCallback(int status, String message) {
            ArmsUtils.makeText(TraceActivity.this, "onBindServiceCallback");
            LogUtils.debugEInfo("onStartTraceCallback");
        }

        // 停止服务回调
        @Override
        public void onStopTraceCallback(int status, String message) {
            ArmsUtils.makeText(TraceActivity.this, "onStopTraceCallback");
            LogUtils.debugEInfo("onStopTraceCallback");
        }

        // 开启采集回调
        @Override
        public void onStartGatherCallback(int status, String message) {
            ArmsUtils.makeText(TraceActivity.this, "onStartGatherCallback");
            LogUtils.debugEInfo("onStartGatherCallback");
        }

        // 停止采集回调
        @Override
        public void onStopGatherCallback(int status, String message) {
            ArmsUtils.makeText(TraceActivity.this, "onStopGatherCallback");
            LogUtils.debugEInfo("onStopGatherCallback");
        }

        // 推送回调
        @Override
        public void onPushCallback(byte messageNo, PushMessage message) {
            ArmsUtils.makeText(TraceActivity.this, "onPushCallback");
            LogUtils.debugEInfo("onPushCallback");
        }

        @Override
        public void onInitBOSCallback(int i, String s) {
            ArmsUtils.makeText(TraceActivity.this, "onInitBOSCallback");
            LogUtils.debugEInfo("onInitBOSCallback");

        }
    };

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }
}
