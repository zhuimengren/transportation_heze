package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class OrdersJinXingZhong {


    /**
     * Data : [{"TaskNo":"PT202002050005","DriverName":"xx","DriverPhone":"15965561796","TruckNumber":"1111","ConsigneeName":" 1","ConsigneePhone":"1","DeliverAddress":"1","GetAddress":"延吉路175号南京路仓库","OutStoreId":44,"OutStoreName":"南京路仓库","DistributeStatus":6,"DistributeStatusName":"配送中","IsFromOutStorage":true,"StartDateTime":"2020-02-14 17:06:15","EndDateTime":null,"InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-05 12:12:23"},{"TaskNo":"PT202002060001","DriverName":"xx","DriverPhone":"15965561796","TruckNumber":"1111","ConsigneeName":" 1","ConsigneePhone":"1","DeliverAddress":"1","GetAddress":"延吉路175号南京路仓库","OutStoreId":44,"OutStoreName":"南京路仓库","DistributeStatus":6,"DistributeStatusName":"配送中","IsFromOutStorage":true,"StartDateTime":"2020-02-14 17:06:15","EndDateTime":null,"InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-06 10:32:01"}]
     * Code : 200
     * Message : 获取配送中记录成功！
     */

    private int Code;
    private String Message;
    private List<DataBean> Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * TaskNo : PT202002050005
         * DriverName : xx
         * DriverPhone : 15965561796
         * TruckNumber : 1111
         * ConsigneeName :  1
         * ConsigneePhone : 1
         * DeliverAddress : 1
         * GetAddress : 延吉路175号南京路仓库
         * OutStoreId : 44
         * OutStoreName : 南京路仓库
         * DistributeStatus : 6
         * DistributeStatusName : 配送中
         * IsFromOutStorage : true
         * StartDateTime : 2020-02-14 17:06:15
         * EndDateTime : null
         * InsertUserId : 174
         * InsertUserName : 配送管理员
         * InsertDateTime : 2020-02-05 12:12:23
         */

        private String TaskNo;
        private String DriverName;
        private String DriverPhone;
        private String TruckNumber;
        private String ConsigneeName;
        private String ConsigneePhone;
        private String DeliverAddress;
        private String GetAddress;
        private int OutStoreId;
        private String OutStoreName;
        private int DistributeStatus;
        private String DistributeStatusName;
        private boolean IsFromOutStorage;
        private String StartDateTime;
        private String EndDateTime;
        private int InsertUserId;
        private String InsertUserName;
        private String InsertDateTime;

        public String getTaskNo() {
            return TaskNo == null ? "" : TaskNo;
        }

        public void setTaskNo(String taskNo) {
            TaskNo = taskNo == null ? "" : taskNo;
        }

        public String getDriverName() {
            return DriverName == null ? "" : DriverName;
        }

        public void setDriverName(String driverName) {
            DriverName = driverName == null ? "" : driverName;
        }

        public String getDriverPhone() {
            return DriverPhone == null ? "" : DriverPhone;
        }

        public void setDriverPhone(String driverPhone) {
            DriverPhone = driverPhone == null ? "" : driverPhone;
        }

        public String getTruckNumber() {
            return TruckNumber == null ? "" : TruckNumber;
        }

        public void setTruckNumber(String truckNumber) {
            TruckNumber = truckNumber == null ? "" : truckNumber;
        }

        public String getConsigneeName() {
            return ConsigneeName == null ? "" : ConsigneeName;
        }

        public void setConsigneeName(String consigneeName) {
            ConsigneeName = consigneeName == null ? "" : consigneeName;
        }

        public String getConsigneePhone() {
            return ConsigneePhone == null ? "" : ConsigneePhone;
        }

        public void setConsigneePhone(String consigneePhone) {
            ConsigneePhone = consigneePhone == null ? "" : consigneePhone;
        }

        public String getDeliverAddress() {
            return DeliverAddress == null ? "" : DeliverAddress;
        }

        public void setDeliverAddress(String deliverAddress) {
            DeliverAddress = deliverAddress == null ? "" : deliverAddress;
        }

        public String getGetAddress() {
            return GetAddress == null ? "" : GetAddress;
        }

        public void setGetAddress(String getAddress) {
            GetAddress = getAddress == null ? "" : getAddress;
        }

        public int getOutStoreId() {
            return OutStoreId;
        }

        public void setOutStoreId(int outStoreId) {
            OutStoreId = outStoreId;
        }

        public String getOutStoreName() {
            return OutStoreName == null ? "" : OutStoreName;
        }

        public void setOutStoreName(String outStoreName) {
            OutStoreName = outStoreName == null ? "" : outStoreName;
        }

        public int getDistributeStatus() {
            return DistributeStatus;
        }

        public void setDistributeStatus(int distributeStatus) {
            DistributeStatus = distributeStatus;
        }

        public String getDistributeStatusName() {
            return DistributeStatusName == null ? "" : DistributeStatusName;
        }

        public void setDistributeStatusName(String distributeStatusName) {
            DistributeStatusName = distributeStatusName == null ? "" : distributeStatusName;
        }

        public boolean isFromOutStorage() {
            return IsFromOutStorage;
        }

        public void setFromOutStorage(boolean fromOutStorage) {
            IsFromOutStorage = fromOutStorage;
        }

        public String getStartDateTime() {
            return StartDateTime == null ? "" : StartDateTime;
        }

        public void setStartDateTime(String startDateTime) {
            StartDateTime = startDateTime == null ? "" : startDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime == null ? "" : EndDateTime;
        }

        public void setEndDateTime(String endDateTime) {
            EndDateTime = endDateTime == null ? "" : endDateTime;
        }

        public int getInsertUserId() {
            return InsertUserId;
        }

        public void setInsertUserId(int insertUserId) {
            InsertUserId = insertUserId;
        }

        public String getInsertUserName() {
            return InsertUserName == null ? "" : InsertUserName;
        }

        public void setInsertUserName(String insertUserName) {
            InsertUserName = insertUserName == null ? "" : insertUserName;
        }

        public String getInsertDateTime() {
            return InsertDateTime == null ? "" : InsertDateTime;
        }

        public void setInsertDateTime(String insertDateTime) {
            InsertDateTime = insertDateTime == null ? "" : insertDateTime;
        }
    }
}
