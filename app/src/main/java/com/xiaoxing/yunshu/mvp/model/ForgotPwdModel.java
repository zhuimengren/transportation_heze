package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.ForgotPwdContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgetPwdSendValidateCode;
import com.xiaoxing.yunshu.mvp.ui.entity.ForgotPwd;
import com.xiaoxing.yunshu.mvp.ui.entity.SendCode;

import io.reactivex.Observable;


@ActivityScope
public class ForgotPwdModel extends BaseModel implements ForgotPwdContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public ForgotPwdModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<ForgotPwd> getForgotPwdData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getForgotPwdData(map);

    }

    @Override
    public Observable<ForgetPwdSendValidateCode> forgetPwdSendValidateCode(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).forgetPwdSendValidateCode(map);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}