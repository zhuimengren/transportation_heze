package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerGrabOrdersComponent;
import com.xiaoxing.yunshu.di.module.GrabOrdersModule;
import com.xiaoxing.yunshu.mvp.contract.GrabOrdersContract;
import com.xiaoxing.yunshu.mvp.presenter.GrabOrdersPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.GrabOrders;
import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersDaiQiangDanFragment;
import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersJinXingZhongFragment;
import com.xiaoxing.yunshu.mvp.ui.fragment.OrdersYiWanChengFragment;

import java.util.ArrayList;
import java.util.HashMap;

import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.activity_grab_orders)
public class GrabOrdersActivity extends BaseActivity<GrabOrdersPresenter> implements GrabOrdersContract.View {
    private final String[] mTitles = {
            "待抢单", "进行中", "已完成"
    };
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerGrabOrdersComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .grabOrdersModule(new GrabOrdersModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_grab_orders; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_grab_orders));

        mFragments.add(OrdersDaiQiangDanFragment.newInstance());
        mFragments.add(OrdersJinXingZhongFragment.newInstance());
        mFragments.add(OrdersYiWanChengFragment.newInstance());

        //getGrabOrdersData();
        ViewPager vp = findViewById(R.id.vp);
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(mAdapter);

        SlidingTabLayout tabLayout_10 = findViewById(R.id.tl_10);
        tabLayout_10.setViewPager(vp);


    }

    @Override
    public void getGrabOrdersDataSuccess(GrabOrders entityData) {


    }

    private void getGrabOrdersData() {
        HashMap<String, String> map = new HashMap<>();

        mPresenter.getGrabOrdersData(map);
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
