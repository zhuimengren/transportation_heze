package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Button;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerXiuGaiMiMaComponent;
import com.xiaoxing.yunshu.di.module.XiuGaiMiMaModule;
import com.xiaoxing.yunshu.mvp.contract.XiuGaiMiMaContract;
import com.xiaoxing.yunshu.mvp.presenter.XiuGaiMiMaPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.XiuGaiMiMa;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonres.view.XEditText;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_XIU_GAI_MI_MA)
public class XiuGaiMiMaActivity extends BaseActivity<XiuGaiMiMaPresenter> implements XiuGaiMiMaContract.View {


    @BindView(R.id.xet_old_pwd)
    XEditText xetOldPwd;
    @BindView(R.id.xet_new_pwd)
    XEditText xetNewPwd;
    @BindView(R.id.xet_new_pwd_again)
    XEditText xetNewPwdAgain;
    @BindView(R.id.btn_save)
    Button btnSave;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerXiuGaiMiMaComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .xiuGaiMiMaModule(new XiuGaiMiMaModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_xiu_gai_mi_ma; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_xiu_gai_mi_ma));
        //getXiuGaiMiMaData();
    }

    @Override
    public void getXiuGaiMiMaDataSuccess(XiuGaiMiMa entityData) {

        showMessage(entityData.getMessage());
        if (entityData.getCode() == 200) {
            killMyself();
        }

    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick(R.id.btn_save)
    public void onViewClicked() {
        if (TextUtils.isEmpty(getOldPwd())) {
            showMessage("请输入旧密码");
            return;
        }
        if (TextUtils.isEmpty(getNewPwd())) {
            showMessage("请输入新密码");
            return;
        }
        if (TextUtils.isEmpty(getNewPwdAgain())) {
            showMessage("请输入确认新密码");
            return;
        }
        if (!getNewPwd().equals(getNewPwdAgain())) {
            showMessage("两次新密码输入不一致");
            return;
        }
        getXiuGaiMiMaData();

    }

    private String getOldPwd() {
        return xetOldPwd.getText().toString();
    }

    private String getNewPwd() {
        return xetNewPwd.getText().toString();
    }

    private String getNewPwdAgain() {
        return xetNewPwdAgain.getText().toString();
    }

    private void getXiuGaiMiMaData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("oldPassword", getOldPwd());
        map.put("newPassword", getNewPwd());
        mPresenter.getXiuGaiMiMaData(map);
    }
}
