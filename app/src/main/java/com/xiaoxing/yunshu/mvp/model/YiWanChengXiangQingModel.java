package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.YiWanChengXiangQingContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.YiWanChengXiangQing;

import io.reactivex.Observable;


@ActivityScope
public class YiWanChengXiangQingModel extends BaseModel implements YiWanChengXiangQingContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public YiWanChengXiangQingModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<YiWanChengXiangQing> getYiWanChengXiangQingList(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getYiWanChengXiangQingList(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}