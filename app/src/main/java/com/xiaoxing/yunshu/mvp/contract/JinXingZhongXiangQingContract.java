package com.xiaoxing.yunshu.mvp.contract;

import com.jess.arms.mvp.IModel;
import com.jess.arms.mvp.IView;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.JinXingZhongXiangQing;
import com.xiaoxing.yunshu.mvp.ui.entity.SendFinishVerifyNotice;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;

public interface JinXingZhongXiangQingContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        void getJinXingZhongXiangQingDataSuccess(JinXingZhongXiangQing entityList);

        void finishScanVerifySuccess(FinishScanVerify entityList);

        void finishMaterialPhotoVerifySuccess(FinishMaterialPhotoVerify entityList);

        void finishPhotoVerifySuccess(FinishPhotoVerify entityList);

        void finishVerifySuccess(FinishVerify entityList);

        void sendFinishVerifyNoticeSuccess(SendFinishVerifyNotice entity);
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        Observable<JinXingZhongXiangQing> getJinXingZhongXiangQingList(Map<String, String> map);

        Observable<FinishScanVerify> finishScanVerify(Map<String, String> map);

        Observable<FinishMaterialPhotoVerify> finishMaterialPhotoVerify(RequestBody map);

        Observable<FinishPhotoVerify> finishPhotoVerify(RequestBody map);

        Observable<FinishVerify> finishVerify(Map<String, String> map);

        Observable<SendFinishVerifyNotice> sendFinishVerifyNotice(Map<String, String> map);
    }
}
