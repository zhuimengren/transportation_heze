package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;
import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.contract.YiFenPeiXiangQingContract;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadedVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.StartDistibute;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPeiXiangQing;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.RequestBody;


@ActivityScope
public class YiFenPeiXiangQingModel extends BaseModel implements YiFenPeiXiangQingContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public YiFenPeiXiangQingModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<YiFenPeiXiangQing> getYiFenPeiXiangQingList(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getYiFenPeiXiangQingList(map);

    }

    @Override
    public Observable<LoadScanVerify> loadScanVerify(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).loadScanVerify(map);

    }

    @Override
    public Observable<LoadMaterialPhotoVerify> loadMaterialPhotoVerify(RequestBody info) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).loadMaterialPhotoVerify(info);

    }

    @Override
    public Observable<LoadPhotoVerify> loadPhotoVerify(RequestBody info) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).loadPhotoVerify(info);

    }

    @Override
    public Observable<LoadedVerify> loadedVerify(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).loadedVerify(map);

    }

    @Override
    public Observable<StartDistibute> startDistibute(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).startDistibute(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}