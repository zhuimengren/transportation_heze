package com.xiaoxing.yunshu.mvp.presenter;

import android.app.Application;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;
import com.xiaoxing.yunshu.mvp.contract.CheLiangXinXiContract;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXi;
import com.xiaoxing.yunshu.mvp.ui.entity.DeleteTruck;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class CheLiangXinXiPresenter extends BasePresenter<CheLiangXinXiContract.Model, CheLiangXinXiContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public CheLiangXinXiPresenter(CheLiangXinXiContract.Model model, CheLiangXinXiContract.View rootView) {
        super(model, rootView);
    }

    public void getCheLiangXinXiList(Map<String, String> map) {

        mModel.getCheLiangXinXiList(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<CheLiangXinXi>(mErrorHandler) {
                    @Override
                    public void onNext(CheLiangXinXi entityList) {
                        mRootView.getCheLiangXinXiDataSuccess(entityList);
                    }
                });
    }

    public void deleteTruck(Map<String, String> map) {

        mModel.deleteTruck(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<DeleteTruck>(mErrorHandler) {
                    @Override
                    public void onNext(DeleteTruck entityList) {
                        mRootView.deleteTruckSuccess(entityList);
                    }
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
