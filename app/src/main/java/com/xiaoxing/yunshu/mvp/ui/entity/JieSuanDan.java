package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class JieSuanDan {


    /**
     * Data : {"StatementNo":"PJ202002170001","DriverId":180,"DriverName":"xx","IDCardNo":"371326198912262419","DriverPhone":"15965561796","TruckNumber":"1111","AccountAmount":650,"StatementStatus":2,"StatementStatusName":"已结算","AccountUserId":174,"AccountUserName":"配送管理员","AccountDateTime":"2020-02-17 10:29:42","GrabNo":"PG202002060001","Remark":"","InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-17 10:29:32","ModifyUserId":null,"ModifyDateTime":null,"Items":[{"StatementNo":"PJ202002170001","TaskNo":"PT202002050005","TaskBill":{"TaskNo":"PT202002050005","StartDateTime":"2020-02-14 17:06:15","EndDateTime":"2020-02-15 12:34:57","TruckId":null,"TruckNumber":null,"DriverId":null,"DriverName":null,"DriverPhone":null,"DeliverCity":null,"DeliverAddress":"1","DeliverAddrLat":null,"DeliverAddrLng":null,"FDeliverAddrLat":null,"FDeliverAddrLng":null,"GetCity":null,"GetAddress":"延吉路175号南京路仓库","GetAddrLat":null,"GetAddrLng":null,"SGetAddrLat":null,"SGetAddrLng":null,"OutStoreId":null,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":0,"DistributeStatusDesc":null,"IsFromOutStorage":true,"GrabNo":null,"LoadPhotoId":null,"FinishPhotoId":null,"LoadPhotoUrl":"fe702345-9c08-4d9b-a3ca-874094cce94c.jpg","FinishPhotoUrl":"a5e46fc9-a48f-4be0-8904-ae85cd53b4c9.jpg","InsertUserId":0,"InsertUserName":null,"InsertDateTime":"0001-01-01 00:00:00","ModifyUserId":null,"ModifyUserName":null,"ModifyDateTime":null,"OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null},"Grab":null,"Items":null,"MItems":null},"Main":null},{"StatementNo":"PJ202002170001","TaskNo":"PT202002060001","TaskBill":{"TaskNo":"PT202002060001","StartDateTime":"2020-02-14 17:06:15","EndDateTime":"2020-02-15 12:49:00","TruckId":null,"TruckNumber":null,"DriverId":null,"DriverName":null,"DriverPhone":null,"DeliverCity":null,"DeliverAddress":"1","DeliverAddrLat":null,"DeliverAddrLng":null,"FDeliverAddrLat":null,"FDeliverAddrLng":null,"GetCity":null,"GetAddress":"延吉路175号南京路仓库","GetAddrLat":null,"GetAddrLng":null,"SGetAddrLat":null,"SGetAddrLng":null,"OutStoreId":null,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":0,"DistributeStatusDesc":null,"IsFromOutStorage":true,"GrabNo":null,"LoadPhotoId":null,"FinishPhotoId":null,"LoadPhotoUrl":"3c6c60de-edbb-4cd0-9854-48f99a34833f.jpg","FinishPhotoUrl":"3e8c74e0-6232-499e-a8dc-1f2100dc386a.jpg","InsertUserId":0,"InsertUserName":null,"InsertDateTime":"0001-01-01 00:00:00","ModifyUserId":null,"ModifyUserName":null,"ModifyDateTime":null,"OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null},"Grab":null,"Items":null,"MItems":null},"Main":null}]}
     * Code : 200
     * Message : 获取结算单详细信息成功！
     */

    private DataBean Data;
    private int Code;
    private String Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * StatementNo : PJ202002170001
         * DriverId : 180
         * DriverName : xx
         * IDCardNo : 371326198912262419
         * DriverPhone : 15965561796
         * TruckNumber : 1111
         * AccountAmount : 650.0
         * StatementStatus : 2
         * StatementStatusName : 已结算
         * AccountUserId : 174
         * AccountUserName : 配送管理员
         * AccountDateTime : 2020-02-17 10:29:42
         * GrabNo : PG202002060001
         * Remark :
         * InsertUserId : 174
         * InsertUserName : 配送管理员
         * InsertDateTime : 2020-02-17 10:29:32
         * ModifyUserId : null
         * ModifyDateTime : null
         * Items : [{"StatementNo":"PJ202002170001","TaskNo":"PT202002050005","TaskBill":{"TaskNo":"PT202002050005","StartDateTime":"2020-02-14 17:06:15","EndDateTime":"2020-02-15 12:34:57","TruckId":null,"TruckNumber":null,"DriverId":null,"DriverName":null,"DriverPhone":null,"DeliverCity":null,"DeliverAddress":"1","DeliverAddrLat":null,"DeliverAddrLng":null,"FDeliverAddrLat":null,"FDeliverAddrLng":null,"GetCity":null,"GetAddress":"延吉路175号南京路仓库","GetAddrLat":null,"GetAddrLng":null,"SGetAddrLat":null,"SGetAddrLng":null,"OutStoreId":null,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":0,"DistributeStatusDesc":null,"IsFromOutStorage":true,"GrabNo":null,"LoadPhotoId":null,"FinishPhotoId":null,"LoadPhotoUrl":"fe702345-9c08-4d9b-a3ca-874094cce94c.jpg","FinishPhotoUrl":"a5e46fc9-a48f-4be0-8904-ae85cd53b4c9.jpg","InsertUserId":0,"InsertUserName":null,"InsertDateTime":"0001-01-01 00:00:00","ModifyUserId":null,"ModifyUserName":null,"ModifyDateTime":null,"OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null},"Grab":null,"Items":null,"MItems":null},"Main":null},{"StatementNo":"PJ202002170001","TaskNo":"PT202002060001","TaskBill":{"TaskNo":"PT202002060001","StartDateTime":"2020-02-14 17:06:15","EndDateTime":"2020-02-15 12:49:00","TruckId":null,"TruckNumber":null,"DriverId":null,"DriverName":null,"DriverPhone":null,"DeliverCity":null,"DeliverAddress":"1","DeliverAddrLat":null,"DeliverAddrLng":null,"FDeliverAddrLat":null,"FDeliverAddrLng":null,"GetCity":null,"GetAddress":"延吉路175号南京路仓库","GetAddrLat":null,"GetAddrLng":null,"SGetAddrLat":null,"SGetAddrLng":null,"OutStoreId":null,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":0,"DistributeStatusDesc":null,"IsFromOutStorage":true,"GrabNo":null,"LoadPhotoId":null,"FinishPhotoId":null,"LoadPhotoUrl":"3c6c60de-edbb-4cd0-9854-48f99a34833f.jpg","FinishPhotoUrl":"3e8c74e0-6232-499e-a8dc-1f2100dc386a.jpg","InsertUserId":0,"InsertUserName":null,"InsertDateTime":"0001-01-01 00:00:00","ModifyUserId":null,"ModifyUserName":null,"ModifyDateTime":null,"OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null},"Grab":null,"Items":null,"MItems":null},"Main":null}]
         */

        private String StatementNo;
        private int DriverId;
        private String DriverName;
        private String IDCardNo;
        private String DriverPhone;
        private String TruckNumber;
        private double AccountAmount;
        private int StatementStatus;
        private String StatementStatusName;
        private int AccountUserId;
        private String AccountUserName;
        private String AccountDateTime;
        private String GrabNo;
        private String Remark;
        private int InsertUserId;
        private String InsertUserName;
        private String InsertDateTime;
        private String ModifyUserId;
        private String ModifyDateTime;
        private List<ItemsBean> Items;

        public String getStatementNo() {
            return StatementNo;
        }

        public void setStatementNo(String StatementNo) {
            this.StatementNo = StatementNo;
        }

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int DriverId) {
            this.DriverId = DriverId;
        }

        public String getDriverName() {
            return DriverName;
        }

        public void setDriverName(String DriverName) {
            this.DriverName = DriverName;
        }

        public String getIDCardNo() {
            return IDCardNo;
        }

        public void setIDCardNo(String IDCardNo) {
            this.IDCardNo = IDCardNo;
        }

        public String getDriverPhone() {
            return DriverPhone;
        }

        public void setDriverPhone(String DriverPhone) {
            this.DriverPhone = DriverPhone;
        }

        public String getTruckNumber() {
            return TruckNumber;
        }

        public void setTruckNumber(String TruckNumber) {
            this.TruckNumber = TruckNumber;
        }

        public double getAccountAmount() {
            return AccountAmount;
        }

        public void setAccountAmount(double AccountAmount) {
            this.AccountAmount = AccountAmount;
        }

        public int getStatementStatus() {
            return StatementStatus;
        }

        public void setStatementStatus(int StatementStatus) {
            this.StatementStatus = StatementStatus;
        }

        public String getStatementStatusName() {
            return StatementStatusName;
        }

        public void setStatementStatusName(String StatementStatusName) {
            this.StatementStatusName = StatementStatusName;
        }

        public int getAccountUserId() {
            return AccountUserId;
        }

        public void setAccountUserId(int AccountUserId) {
            this.AccountUserId = AccountUserId;
        }

        public String getAccountUserName() {
            return AccountUserName;
        }

        public void setAccountUserName(String AccountUserName) {
            this.AccountUserName = AccountUserName;
        }

        public String getAccountDateTime() {
            return AccountDateTime;
        }

        public void setAccountDateTime(String AccountDateTime) {
            this.AccountDateTime = AccountDateTime;
        }

        public String getGrabNo() {
            return GrabNo;
        }

        public void setGrabNo(String GrabNo) {
            this.GrabNo = GrabNo;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }

        public int getInsertUserId() {
            return InsertUserId;
        }

        public void setInsertUserId(int InsertUserId) {
            this.InsertUserId = InsertUserId;
        }

        public String getInsertUserName() {
            return InsertUserName;
        }

        public void setInsertUserName(String InsertUserName) {
            this.InsertUserName = InsertUserName;
        }

        public String getInsertDateTime() {
            return InsertDateTime;
        }

        public void setInsertDateTime(String InsertDateTime) {
            this.InsertDateTime = InsertDateTime;
        }

        public String getModifyUserId() {
            return ModifyUserId;
        }

        public void setModifyUserId(String ModifyUserId) {
            this.ModifyUserId = ModifyUserId;
        }

        public String getModifyDateTime() {
            return ModifyDateTime;
        }

        public void setModifyDateTime(String ModifyDateTime) {
            this.ModifyDateTime = ModifyDateTime;
        }

        public List<ItemsBean> getItems() {
            return Items;
        }

        public void setItems(List<ItemsBean> Items) {
            this.Items = Items;
        }

        public static class ItemsBean {
            /**
             * StatementNo : PJ202002170001
             * TaskNo : PT202002050005
             * TaskBill : {"TaskNo":"PT202002050005","StartDateTime":"2020-02-14 17:06:15","EndDateTime":"2020-02-15 12:34:57","TruckId":null,"TruckNumber":null,"DriverId":null,"DriverName":null,"DriverPhone":null,"DeliverCity":null,"DeliverAddress":"1","DeliverAddrLat":null,"DeliverAddrLng":null,"FDeliverAddrLat":null,"FDeliverAddrLng":null,"GetCity":null,"GetAddress":"延吉路175号南京路仓库","GetAddrLat":null,"GetAddrLng":null,"SGetAddrLat":null,"SGetAddrLng":null,"OutStoreId":null,"ConsigneeName":" 1","ConsigneePhone":"1","DistributeStatus":0,"DistributeStatusDesc":null,"IsFromOutStorage":true,"GrabNo":null,"LoadPhotoId":null,"FinishPhotoId":null,"LoadPhotoUrl":"fe702345-9c08-4d9b-a3ca-874094cce94c.jpg","FinishPhotoUrl":"a5e46fc9-a48f-4be0-8904-ae85cd53b4c9.jpg","InsertUserId":0,"InsertUserName":null,"InsertDateTime":"0001-01-01 00:00:00","ModifyUserId":null,"ModifyUserName":null,"ModifyDateTime":null,"OutStore":{"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null},"Grab":null,"Items":null,"MItems":null}
             * Main : null
             */

            private String StatementNo;
            private String TaskNo;
            private TaskBillBean TaskBill;
            private String Main;

            public String getStatementNo() {
                return StatementNo;
            }

            public void setStatementNo(String StatementNo) {
                this.StatementNo = StatementNo;
            }

            public String getTaskNo() {
                return TaskNo;
            }

            public void setTaskNo(String TaskNo) {
                this.TaskNo = TaskNo;
            }

            public TaskBillBean getTaskBill() {
                return TaskBill;
            }

            public void setTaskBill(TaskBillBean TaskBill) {
                this.TaskBill = TaskBill;
            }

            public String getMain() {
                return Main;
            }

            public void setMain(String Main) {
                this.Main = Main;
            }

            public static class TaskBillBean {
                /**
                 * TaskNo : PT202002050005
                 * StartDateTime : 2020-02-14 17:06:15
                 * EndDateTime : 2020-02-15 12:34:57
                 * TruckId : null
                 * TruckNumber : null
                 * DriverId : null
                 * DriverName : null
                 * DriverPhone : null
                 * DeliverCity : null
                 * DeliverAddress : 1
                 * DeliverAddrLat : null
                 * DeliverAddrLng : null
                 * FDeliverAddrLat : null
                 * FDeliverAddrLng : null
                 * GetCity : null
                 * GetAddress : 延吉路175号南京路仓库
                 * GetAddrLat : null
                 * GetAddrLng : null
                 * SGetAddrLat : null
                 * SGetAddrLng : null
                 * OutStoreId : null
                 * ConsigneeName :  1
                 * ConsigneePhone : 1
                 * DistributeStatus : 0
                 * DistributeStatusDesc : null
                 * IsFromOutStorage : true
                 * GrabNo : null
                 * LoadPhotoId : null
                 * FinishPhotoId : null
                 * LoadPhotoUrl : fe702345-9c08-4d9b-a3ca-874094cce94c.jpg
                 * FinishPhotoUrl : a5e46fc9-a48f-4be0-8904-ae85cd53b4c9.jpg
                 * InsertUserId : 0
                 * InsertUserName : null
                 * InsertDateTime : 0001-01-01 00:00:00
                 * ModifyUserId : null
                 * ModifyUserName : null
                 * ModifyDateTime : null
                 * OutStore : {"ID":44,"StoreName":"南京路仓库","StoreAddress":"延吉路175号南京路仓库","Tasks":null}
                 * Grab : null
                 * Items : null
                 * MItems : null
                 */

                private String TaskNo;
                private String StartDateTime;
                private String EndDateTime;
                private String TruckId;
                private String TruckNumber;
                private String DriverId;
                private String DriverName;
                private String DriverPhone;
                private String DeliverCity;
                private String DeliverAddress;
                private String DeliverAddrLat;
                private String DeliverAddrLng;
                private String FDeliverAddrLat;
                private String FDeliverAddrLng;
                private String GetCity;
                private String GetAddress;
                private String GetAddrLat;
                private String GetAddrLng;
                private String SGetAddrLat;
                private String SGetAddrLng;
                private String OutStoreId;
                private String ConsigneeName;
                private String ConsigneePhone;
                private int DistributeStatus;
                private String DistributeStatusDesc;
                private boolean IsFromOutStorage;
                private String GrabNo;
                private String LoadPhotoId;
                private String FinishPhotoId;
                private String LoadPhotoUrl;
                private String FinishPhotoUrl;
                private int InsertUserId;
                private String InsertUserName;
                private String InsertDateTime;
                private String ModifyUserId;
                private String ModifyUserName;
                private String ModifyDateTime;
                private OutStoreBean OutStore;
                private String Grab;
                private String Items;
                private String MItems;

                public String getTaskNo() {
                    return TaskNo;
                }

                public void setTaskNo(String TaskNo) {
                    this.TaskNo = TaskNo;
                }

                public String getStartDateTime() {
                    return StartDateTime;
                }

                public void setStartDateTime(String StartDateTime) {
                    this.StartDateTime = StartDateTime;
                }

                public String getEndDateTime() {
                    return EndDateTime;
                }

                public void setEndDateTime(String EndDateTime) {
                    this.EndDateTime = EndDateTime;
                }

                public String getTruckId() {
                    return TruckId;
                }

                public void setTruckId(String TruckId) {
                    this.TruckId = TruckId;
                }

                public String getTruckNumber() {
                    return TruckNumber;
                }

                public void setTruckNumber(String TruckNumber) {
                    this.TruckNumber = TruckNumber;
                }

                public String getDriverId() {
                    return DriverId;
                }

                public void setDriverId(String DriverId) {
                    this.DriverId = DriverId;
                }

                public String getDriverName() {
                    return DriverName;
                }

                public void setDriverName(String DriverName) {
                    this.DriverName = DriverName;
                }

                public String getDriverPhone() {
                    return DriverPhone;
                }

                public void setDriverPhone(String DriverPhone) {
                    this.DriverPhone = DriverPhone;
                }

                public String getDeliverCity() {
                    return DeliverCity;
                }

                public void setDeliverCity(String DeliverCity) {
                    this.DeliverCity = DeliverCity;
                }

                public String getDeliverAddress() {
                    return DeliverAddress;
                }

                public void setDeliverAddress(String DeliverAddress) {
                    this.DeliverAddress = DeliverAddress;
                }

                public String getDeliverAddrLat() {
                    return DeliverAddrLat;
                }

                public void setDeliverAddrLat(String DeliverAddrLat) {
                    this.DeliverAddrLat = DeliverAddrLat;
                }

                public String getDeliverAddrLng() {
                    return DeliverAddrLng;
                }

                public void setDeliverAddrLng(String DeliverAddrLng) {
                    this.DeliverAddrLng = DeliverAddrLng;
                }

                public String getFDeliverAddrLat() {
                    return FDeliverAddrLat;
                }

                public void setFDeliverAddrLat(String FDeliverAddrLat) {
                    this.FDeliverAddrLat = FDeliverAddrLat;
                }

                public String getFDeliverAddrLng() {
                    return FDeliverAddrLng;
                }

                public void setFDeliverAddrLng(String FDeliverAddrLng) {
                    this.FDeliverAddrLng = FDeliverAddrLng;
                }

                public String getGetCity() {
                    return GetCity;
                }

                public void setGetCity(String GetCity) {
                    this.GetCity = GetCity;
                }

                public String getGetAddress() {
                    return GetAddress;
                }

                public void setGetAddress(String GetAddress) {
                    this.GetAddress = GetAddress;
                }

                public String getGetAddrLat() {
                    return GetAddrLat;
                }

                public void setGetAddrLat(String GetAddrLat) {
                    this.GetAddrLat = GetAddrLat;
                }

                public String getGetAddrLng() {
                    return GetAddrLng;
                }

                public void setGetAddrLng(String GetAddrLng) {
                    this.GetAddrLng = GetAddrLng;
                }

                public String getSGetAddrLat() {
                    return SGetAddrLat;
                }

                public void setSGetAddrLat(String SGetAddrLat) {
                    this.SGetAddrLat = SGetAddrLat;
                }

                public String getSGetAddrLng() {
                    return SGetAddrLng;
                }

                public void setSGetAddrLng(String SGetAddrLng) {
                    this.SGetAddrLng = SGetAddrLng;
                }

                public String getOutStoreId() {
                    return OutStoreId;
                }

                public void setOutStoreId(String OutStoreId) {
                    this.OutStoreId = OutStoreId;
                }

                public String getConsigneeName() {
                    return ConsigneeName;
                }

                public void setConsigneeName(String ConsigneeName) {
                    this.ConsigneeName = ConsigneeName;
                }

                public String getConsigneePhone() {
                    return ConsigneePhone;
                }

                public void setConsigneePhone(String ConsigneePhone) {
                    this.ConsigneePhone = ConsigneePhone;
                }

                public int getDistributeStatus() {
                    return DistributeStatus;
                }

                public void setDistributeStatus(int DistributeStatus) {
                    this.DistributeStatus = DistributeStatus;
                }

                public String getDistributeStatusDesc() {
                    return DistributeStatusDesc;
                }

                public void setDistributeStatusDesc(String DistributeStatusDesc) {
                    this.DistributeStatusDesc = DistributeStatusDesc;
                }

                public boolean isIsFromOutStorage() {
                    return IsFromOutStorage;
                }

                public void setIsFromOutStorage(boolean IsFromOutStorage) {
                    this.IsFromOutStorage = IsFromOutStorage;
                }

                public String getGrabNo() {
                    return GrabNo;
                }

                public void setGrabNo(String GrabNo) {
                    this.GrabNo = GrabNo;
                }

                public String getLoadPhotoId() {
                    return LoadPhotoId;
                }

                public void setLoadPhotoId(String LoadPhotoId) {
                    this.LoadPhotoId = LoadPhotoId;
                }

                public String getFinishPhotoId() {
                    return FinishPhotoId;
                }

                public void setFinishPhotoId(String FinishPhotoId) {
                    this.FinishPhotoId = FinishPhotoId;
                }

                public String getLoadPhotoUrl() {
                    return LoadPhotoUrl;
                }

                public void setLoadPhotoUrl(String LoadPhotoUrl) {
                    this.LoadPhotoUrl = LoadPhotoUrl;
                }

                public String getFinishPhotoUrl() {
                    return FinishPhotoUrl;
                }

                public void setFinishPhotoUrl(String FinishPhotoUrl) {
                    this.FinishPhotoUrl = FinishPhotoUrl;
                }

                public int getInsertUserId() {
                    return InsertUserId;
                }

                public void setInsertUserId(int InsertUserId) {
                    this.InsertUserId = InsertUserId;
                }

                public String getInsertUserName() {
                    return InsertUserName;
                }

                public void setInsertUserName(String InsertUserName) {
                    this.InsertUserName = InsertUserName;
                }

                public String getInsertDateTime() {
                    return InsertDateTime;
                }

                public void setInsertDateTime(String InsertDateTime) {
                    this.InsertDateTime = InsertDateTime;
                }

                public String getModifyUserId() {
                    return ModifyUserId;
                }

                public void setModifyUserId(String ModifyUserId) {
                    this.ModifyUserId = ModifyUserId;
                }

                public String getModifyUserName() {
                    return ModifyUserName;
                }

                public void setModifyUserName(String ModifyUserName) {
                    this.ModifyUserName = ModifyUserName;
                }

                public String getModifyDateTime() {
                    return ModifyDateTime;
                }

                public void setModifyDateTime(String ModifyDateTime) {
                    this.ModifyDateTime = ModifyDateTime;
                }

                public OutStoreBean getOutStore() {
                    return OutStore;
                }

                public void setOutStore(OutStoreBean OutStore) {
                    this.OutStore = OutStore;
                }

                public String getGrab() {
                    return Grab;
                }

                public void setGrab(String Grab) {
                    this.Grab = Grab;
                }

                public String getItems() {
                    return Items;
                }

                public void setItems(String Items) {
                    this.Items = Items;
                }

                public String getMItems() {
                    return MItems;
                }

                public void setMItems(String MItems) {
                    this.MItems = MItems;
                }

                public static class OutStoreBean {
                    /**
                     * ID : 44
                     * StoreName : 南京路仓库
                     * StoreAddress : 延吉路175号南京路仓库
                     * Tasks : null
                     */

                    private int ID;
                    private String StoreName;
                    private String StoreAddress;
                    private String Tasks;

                    public int getID() {
                        return ID;
                    }

                    public void setID(int ID) {
                        this.ID = ID;
                    }

                    public String getStoreName() {
                        return StoreName;
                    }

                    public void setStoreName(String StoreName) {
                        this.StoreName = StoreName;
                    }

                    public String getStoreAddress() {
                        return StoreAddress;
                    }

                    public void setStoreAddress(String StoreAddress) {
                        this.StoreAddress = StoreAddress;
                    }

                    public String getTasks() {
                        return Tasks;
                    }

                    public void setTasks(String Tasks) {
                        this.Tasks = Tasks;
                    }
                }
            }
        }
    }
}
