package com.xiaoxing.yunshu.mvp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flyco.tablayout.SlidingTabLayout;
import com.jess.arms.base.BaseFragment;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerQiangDanComponent;
import com.xiaoxing.yunshu.di.module.QiangDanModule;
import com.xiaoxing.yunshu.mvp.contract.QiangDanContract;
import com.xiaoxing.yunshu.mvp.presenter.QiangDanPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDan;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class QiangDanFragment extends BaseFragment<QiangDanPresenter> implements QiangDanContract.View {

    private final String[] mTitles = {
            "已分配", "进行中", "已完成"
    };
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private ViewPager vp;
    private SlidingTabLayout tabLayout_10;

    public static QiangDanFragment newInstance() {
        QiangDanFragment fragment = new QiangDanFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerQiangDanComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .qiangDanModule(new QiangDanModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qiang_dan, container, false);
        vp = view.findViewById(R.id.vp);
        tabLayout_10 = view.findViewById(R.id.tl_10);
        ToolbarUtils.initToolbarTitleNoBack(view, this, "配送");

        return view;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        //getQiangDanData();
        mFragments.add(YiFenPeiFragment.newInstance());
        mFragments.add(OrdersJinXingZhongFragment.newInstance());
        mFragments.add(OrdersYiWanChengFragment.newInstance());

        //getGrabOrdersData();
        mAdapter = new MyPagerAdapter(getChildFragmentManager());
        vp.setAdapter(mAdapter);

        tabLayout_10.setViewPager(vp);
    }


    @Subscriber(tag = "setPeiSongCurrentItem", mode = ThreadMode.MAIN)
    public void setPeiSongCurrentItem(int position) {
        vp.setCurrentItem(position);
    }

    /**
     * 通过此方法可以使 Fragment 能够与外界做一些交互和通信, 比如说外部的 Activity 想让自己持有的某个 Fragment 对象执行一些方法,
     * 建议在有多个需要与外界交互的方法时, 统一传 {@link Message}, 通过 what 字段来区分不同的方法, 在 {@link #setData(Object)}
     * 方法中就可以 {@code switch} 做不同的操作, 这样就可以用统一的入口方法做多个不同的操作, 可以起到分发的作用
     * <p>
     * 调用此方法时请注意调用时 Fragment 的生命周期, 如果调用 {@link #setData(Object)} 方法时 {@link Fragment#onCreate(Bundle)} 还没执行
     * 但在 {@link #setData(Object)} 里却调用了 Presenter 的方法, 是会报空的, 因为 Dagger 注入是在 {@link Fragment#onCreate(Bundle)}
     * 方法中执行的
     * 然后才创建的 Presenter, 如果要做一些初始化操作,可以不必让外部调用 {@link #setData(Object)}, 在 {@link #initData(Bundle)} 中初始化就可以了
     *
     * <p>
     * Example usage:
     *
     * <pre>
     * public void setData(@Nullable Object data) {
     *     if (data != null && data instanceof Message) {
     *         switch (((Message) data).what) {
     *             case 0:
     *                 loadData(((Message) data).arg1);
     *                 break;
     *             case 1:
     *                 refreshUI();
     *                 break;
     *             default:
     *                 //do something
     *                 break;
     *         }
     *     }
     * }
     *
     * // call setData(Object):
     * Message data = new Message();
     * data.what = 0;
     * data.arg1 = 1;
     * fragment.setData(data);
     * </pre>
     *
     * @param data 当不需要参数时 {@code data} 可以为 {@code null}
     */
    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void getQiangDanDataSuccess(QiangDan entityData) {


    }

    private void getQiangDanData() {
        HashMap<String, String> map = new HashMap<>();
        mPresenter.getQiangDanData(map);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
