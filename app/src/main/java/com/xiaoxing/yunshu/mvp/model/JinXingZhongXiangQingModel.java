package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;
import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.contract.JinXingZhongXiangQingContract;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.JinXingZhongXiangQing;
import com.xiaoxing.yunshu.mvp.ui.entity.SendFinishVerifyNotice;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.RequestBody;


@ActivityScope
public class JinXingZhongXiangQingModel extends BaseModel implements JinXingZhongXiangQingContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public JinXingZhongXiangQingModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<JinXingZhongXiangQing> getJinXingZhongXiangQingList(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getJinXingZhongXiangQingList(map);

    }

    @Override
    public Observable<FinishScanVerify> finishScanVerify(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).finishScanVerify(map);

    }

    @Override
    public Observable<FinishMaterialPhotoVerify> finishMaterialPhotoVerify(RequestBody map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).finishMaterialPhotoVerify(map);

    }

    @Override
    public Observable<FinishPhotoVerify> finishPhotoVerify(RequestBody map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).finishPhotoVerify(map);

    }

    @Override
    public Observable<FinishVerify> finishVerify(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).finishVerify(map);

    }

    @Override
    public Observable<SendFinishVerifyNotice> sendFinishVerifyNotice(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).sendFinishVerifyNotice(map);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}