package com.xiaoxing.yunshu.mvp.ui.entity;

public class CheLiangXinXiEdit {


    /**
     * Code : 200
     * Message : 车辆信息修改成功！
     */

    private int Code;
    private String Message;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

}
