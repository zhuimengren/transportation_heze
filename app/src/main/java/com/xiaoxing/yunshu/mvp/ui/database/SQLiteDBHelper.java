package com.xiaoxing.yunshu.mvp.ui.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteDBHelper extends SQLiteOpenHelper {

	private final static String DB_NAME = "DYJDB";// 数据库名
	private final static int DB_VISION = 1;// 数据库版本

	public SQLiteDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VISION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String create_OutStorageList = " Create table IF NOT EXISTS OutStorageList (id integer primary key autoincrement,"
				+ " material_id integer,"
				+ " out_storage_estimate varchar(40),"
				+ " out_storage_count varchar(20),"
				+ " material_user_id integer,"
				+ " check_user_id integer,"
				+ " check_status varchar(20),"
				+ " deal_status varchar(20),"
				+ " receive_address varchar(200),"
				+ " receive_username varchar(100),"
				+ " receive_userphone varchar(30),"
				+ " out_storagelist_batchid varchar(100),"
				+ " target_project varchar(200)  " + ")  ";

		db.execSQL(create_OutStorageList);

		// db.close();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		if (oldVersion != newVersion) {

			String drop_OutStorageList = " Drop table IF EXISTS OutStorageList ";

			db.execSQL(drop_OutStorageList);

			onCreate(db);

		}

	}

}
