package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.YiWanChengXiangQing;

import java.util.List;
import java.util.Map;

import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;


public class YiWanChengXiangQing1Adapter extends ExpandableRecyclerAdapter<YiWanChengXiangQing1Adapter.OrderListItem> {
    public static final int QIANG_DAN_DETAIL_HEAD = 1000; //头部
    public static final int QIANG_DAN_DETAIL_MIDDLE = 1001; //中间
    public static final int QIANG_DAN_DETAIL_FOTTER = 1002; //底部

    private List<YiWanChengXiangQing.DataBean.TasksBean> mGroups;
    private Map<String, List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean>> mChildren;
    private Map<Integer, Integer> mGroupsPosition;

    private CheckInterface checkInterface;
    private IYiFenPeiXiangQingInterface mIYiFenPeiXiangQingInterface;

    public YiWanChengXiangQing1Adapter(Context context, List<YiWanChengXiangQing.DataBean.TasksBean> groups, Map<String, List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean>> children, Map<Integer, Integer> groupsPosition) {
        super(context);
        this.mGroups = groups;
        this.mChildren = children;
        this.mGroupsPosition = groupsPosition;
    }

    public void setCheckInterface(CheckInterface checkInterface) {
        this.checkInterface = checkInterface;
    }

    public void setWanChengInterface(IYiFenPeiXiangQingInterface iYiFenPeiXiangQingInterface) {
        this.mIYiFenPeiXiangQingInterface = iYiFenPeiXiangQingInterface;
    }

    public Object getChild(int groupPosition, int childPosition) {
        List<YiWanChengXiangQing.DataBean.TasksBean.ItemsBean> childs = mChildren.get(String.valueOf(groupPosition));
        return childs.get(childPosition);
    }

    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case QIANG_DAN_DETAIL_HEAD:
                return new ShangJiaNameViewHolder(inflate(R.layout.item_yi_wan_cheng_head, parent));
            case QIANG_DAN_DETAIL_FOTTER:
                return new FooterViewHolder(inflate(R.layout.item_yi_wan_cheng_fotter, parent));
            case QIANG_DAN_DETAIL_MIDDLE:
            default:
                return new ProductsViewHolder(inflate(R.layout.item_yi_wan_cheng_middle, parent));
        }
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case QIANG_DAN_DETAIL_HEAD:
                ((ShangJiaNameViewHolder) holder).bind(position);
                break;
            case QIANG_DAN_DETAIL_FOTTER:
                ((FooterViewHolder) holder).bind(position);
                break;
            case QIANG_DAN_DETAIL_MIDDLE:
            default:
                ((ProductsViewHolder) holder).bind(position);
                break;
        }
    }

    public void setDataItems(List<OrderListItem> orderListItems) {
        setItems(orderListItems);
    }

    /**
     * 复选框接口
     */
    public interface CheckInterface {
        /**
         * 组选框状态改变触发的事件
         *
         * @param groupPosition 组元素位置
         * @param isChecked     组元素选中与否
         */
        void checkGroup(int groupPosition, boolean isChecked);

        /**
         * 子选框状态改变时触发的事件
         *
         * @param groupPosition 组元素位置
         * @param childPosition 子元素位置
         * @param isChecked     子元素选中与否
         */
        void checkChild(int groupPosition, int childPosition, boolean isChecked);
    }

    public interface IYiFenPeiXiangQingInterface {
        void heDui(String taskNo);

        void saoMiaoErWeiMa(YiWanChengXiangQing.DataBean.TasksBean.ItemsBean itemsBean);

        void paiZhao(YiWanChengXiangQing.DataBean.TasksBean.ItemsBean itemsBean);

        void zhuangChePaiZhao(YiWanChengXiangQing.DataBean.TasksBean tasksBean);
    }

    public static class OrderListItem extends ExpandableRecyclerAdapter.ListItem {
        public YiWanChengXiangQing.DataBean.TasksBean mTasksBean;
        public YiWanChengXiangQing.DataBean.TasksBean.ItemsBean mItemsBean;
        public int mGroupPosition;
        public int mChildPosition;
        public String mGrabNo;
        public String mPrice;
        public YiWanChengXiangQing.DataBean.TasksBean mFooterTasksBean;

        public OrderListItem(YiWanChengXiangQing.DataBean.TasksBean tasksBean) {
            super(QIANG_DAN_DETAIL_HEAD);
            mTasksBean = tasksBean;
        }

        public OrderListItem(int groupPosition, YiWanChengXiangQing.DataBean.TasksBean.ItemsBean itemsBean, int childPosition) {
            super(QIANG_DAN_DETAIL_MIDDLE);
            mItemsBean = itemsBean;
            mGroupPosition = groupPosition;
            mChildPosition = childPosition;
        }

        public OrderListItem(String first, String last, YiWanChengXiangQing.DataBean.TasksBean footerTasksBean, String type) {
            super(QIANG_DAN_DETAIL_FOTTER);
            mGrabNo = first;
            mPrice = last;
            mFooterTasksBean = footerTasksBean;
        }
    }

    public class ShangJiaNameViewHolder extends ViewHolder {

        private TextView tv_name;
        private CheckBox cb_cang_ku_name;

        public ShangJiaNameViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_che_pai_hao);
            cb_cang_ku_name = view.findViewById(R.id.cb_cang_ku_name);
        }

        public void bind(int position) {
            YiWanChengXiangQing.DataBean.TasksBean tasksBean = visibleItems.get(position).mTasksBean;
            tv_name.setText("任务单号：" + tasksBean.getTaskNo());

            final YiWanChengXiangQing.DataBean.TasksBean group = (YiWanChengXiangQing.DataBean.TasksBean) getGroup(mGroupsPosition.get(position));
            cb_cang_ku_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    group.setChoosed(((CheckBox) v).isChecked());
                    checkInterface.checkGroup(mGroupsPosition.get(position), ((CheckBox) v).isChecked());// 暴露组选接口
                }
            });

            cb_cang_ku_name.setChecked(group.isChoosed());

        }
    }

    public class ProductsViewHolder extends ViewHolder {

        private TextView tv_name;
        private TextView tv_num;
        private TextView tv_sao_mao;
        private Button btn_sao_mao;
        private Button btn_pai_zhao;
        private CheckBox cb_product_name;

        public ProductsViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_che_pai_hao);
            tv_num = view.findViewById(R.id.tv_num);
            cb_product_name = view.findViewById(R.id.cb_product_name);
            tv_sao_mao = view.findViewById(R.id.tv_sao_mao);
            btn_sao_mao = view.findViewById(R.id.btn_sao_mao);
            btn_pai_zhao = view.findViewById(R.id.btn_pai_zhao);
        }

        public void bind(int position) {

            YiWanChengXiangQing.DataBean.TasksBean.ItemsBean materialDriverMappingChanPin = visibleItems.get(position).mItemsBean;
            tv_name.setText(materialDriverMappingChanPin.getMaterialName());
//            tv_num.setText("共" + materialDriverMappingChanPin.getNum() + "件");

            int groupPosition = visibleItems.get(position).mGroupPosition;
            int childPosition = visibleItems.get(position).mChildPosition;

            final YiWanChengXiangQing.DataBean.TasksBean.ItemsBean itemsBean = (YiWanChengXiangQing.DataBean.TasksBean.ItemsBean) getChild(groupPosition, childPosition);

            cb_product_name.setChecked(itemsBean.isChoosed());

//            if (itemsBean.getIsbeginscan().equals("1")) {
//                tv_sao_mao.setText("已扫描");
//            } else {
//                tv_sao_mao.setText("未扫描");
//            }

            cb_product_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    itemsBean.setChoosed(((CheckBox) v).isChecked());
                    cb_product_name.setChecked(((CheckBox) v).isChecked());
                    checkInterface.checkChild(groupPosition, position, ((CheckBox) v).isChecked());// 暴露子选接口

                }
            });

            if (materialDriverMappingChanPin.isIsStartVerify()) {
                btn_sao_mao.setText("已核对(扫描)");
                btn_pai_zhao.setText("已核对(拍照)");
            } else {
                btn_sao_mao.setText("未核对(扫描)");
                btn_pai_zhao.setText("未核对(拍照)");
            }

//            if (materialDriverMappingChanPin.isFromOutStorage()) {
//                btn_sao_mao.setVisibility(View.VISIBLE);
//                btn_pai_zhao.setVisibility(View.GONE);
//            } else {
//                btn_sao_mao.setVisibility(View.GONE);
//                btn_pai_zhao.setVisibility(View.VISIBLE);
//            }
            btn_sao_mao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mIYiFenPeiXiangQingInterface.saoMiaoErWeiMa(itemsBean);
                }
            });
            btn_pai_zhao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mIYiFenPeiXiangQingInterface.paiZhao(itemsBean);

                }
            });

        }
    }

    public class FooterViewHolder extends ViewHolder {

        private Button btn_que_ding;
        private Button btn_zhang_che_zhao_pian;

        public FooterViewHolder(View view) {
            super(view);
            btn_que_ding = view.findViewById(R.id.btn_que_ding);
            btn_zhang_che_zhao_pian = view.findViewById(R.id.btn_zhang_che_zhao_pian);
        }

        public void bind(int position) {


            YiWanChengXiangQing.DataBean.TasksBean tasksBean = visibleItems.get(position).mFooterTasksBean;

            if (!TextUtils.isEmpty(tasksBean.getLoadPhotoId())) {
                btn_zhang_che_zhao_pian.setText("装车照片(已拍照)");
            } else {
                btn_zhang_che_zhao_pian.setText("装车照片(未拍照)");
            }

            if (tasksBean.getDistributeStatus() == 8) {
                btn_que_ding.setText("已结算(查看结算单)");
//                btn_que_ding.setClickable(false);
            } else {
                btn_que_ding.setText("未结算");
//                btn_que_ding.setClickable(true);

            }

            btn_que_ding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    String grabNo = visibleItems.get(position).mGrabNo;
//                    String price = visibleItems.get(position).mPrice;
//                    mIYiFenPeiXiangQingInterface.heDui(tasksBean.getTaskNo());
                    if (tasksBean.getDistributeStatus() == 8) {//已结算
                        Bundle bundle = new Bundle();
                        bundle.putString("grabNo", tasksBean.getGrabNo());
                        Utils.navigation(mContext, RouterHub.ACTIVITY_JIE_SUAN_DAN, bundle);
                    } else {
                        ArmsUtils.makeText(mContext, "未结算，不能查看详情");
                    }


                }
            });
            btn_zhang_che_zhao_pian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIYiFenPeiXiangQingInterface.zhuangChePaiZhao(tasksBean);
                }
            });
        }
    }
}
