package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.WoDeContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.WoDe;

import io.reactivex.Observable;


@FragmentScope
public class WoDeModel extends BaseModel implements WoDeContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public WoDeModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<WoDe> getWoDeData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getWoDeData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}