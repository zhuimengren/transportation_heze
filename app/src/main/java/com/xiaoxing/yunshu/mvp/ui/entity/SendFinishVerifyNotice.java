package com.xiaoxing.yunshu.mvp.ui.entity;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2020/4/28 0028 15:59
 */
public class SendFinishVerifyNotice {


    /**
     * Code : 200
     * Message : 收货验证短信发送成功！
     */

    private int Code;
    private String Message;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}
