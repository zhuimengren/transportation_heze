package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanDetail;

import java.util.List;
import java.util.Map;


public class QiangDanDetail1Adapter extends ExpandableRecyclerAdapter<QiangDanDetail1Adapter.OrderListItem> {
    public static final int QIANG_DAN_DETAIL_HEAD = 1000; //头部
    public static final int QIANG_DAN_DETAIL_MIDDLE = 1001; //中间
    public static final int QIANG_DAN_DETAIL_FOTTER = 1002; //底部

    private List<QiangDanDetail.DataBean.TasksBean> mGroups;
    private Map<String, List<QiangDanDetail.DataBean.TasksBean.ItemsBean>> mChildren;
    private Map<Integer, Integer> mGroupsPosition;

    private CheckInterface checkInterface;
    private IQiangDanQueDingInterface mWanCheng;

    public QiangDanDetail1Adapter(Context context, List<QiangDanDetail.DataBean.TasksBean> groups, Map<String, List<QiangDanDetail.DataBean.TasksBean.ItemsBean>> children, Map<Integer, Integer> groupsPosition) {
        super(context);
        this.mGroups = groups;
        this.mChildren = children;
        this.mGroupsPosition = groupsPosition;
    }

    public void setCheckInterface(CheckInterface checkInterface) {
        this.checkInterface = checkInterface;
    }

    public void setWanChengInterface(IQiangDanQueDingInterface wanChengInterface) {
        this.mWanCheng = wanChengInterface;
    }

    public Object getChild(int groupPosition, int childPosition) {
        List<QiangDanDetail.DataBean.TasksBean.ItemsBean> childs = mChildren.get(String.valueOf(groupPosition));
        return childs.get(childPosition);
    }

    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case QIANG_DAN_DETAIL_HEAD:
                return new ShangJiaNameViewHolder(inflate(R.layout.item_qiang_dan_detail_head, parent));
            case QIANG_DAN_DETAIL_FOTTER:
                return new FooterViewHolder(inflate(R.layout.item_qiang_dan_detail_fotter, parent));
            case QIANG_DAN_DETAIL_MIDDLE:
            default:
                return new ProductsViewHolder(inflate(R.layout.item_qiang_dan_detail_middle, parent));
        }
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case QIANG_DAN_DETAIL_HEAD:
                ((ShangJiaNameViewHolder) holder).bind(position);
                break;
            case QIANG_DAN_DETAIL_FOTTER:
                ((FooterViewHolder) holder).bind(position);
                break;
            case QIANG_DAN_DETAIL_MIDDLE:
            default:
                ((ProductsViewHolder) holder).bind(position);
                break;
        }
    }

    public void setDataItems(List<OrderListItem> orderListItems) {
        setItems(orderListItems);
    }

    /**
     * 复选框接口
     */
    public interface CheckInterface {
        /**
         * 组选框状态改变触发的事件
         *
         * @param groupPosition 组元素位置
         * @param isChecked     组元素选中与否
         */
        void checkGroup(int groupPosition, boolean isChecked);

        /**
         * 子选框状态改变时触发的事件
         *
         * @param groupPosition 组元素位置
         * @param childPosition 子元素位置
         * @param isChecked     子元素选中与否
         */
        void checkChild(int groupPosition, int childPosition, boolean isChecked);
    }

    public interface IQiangDanQueDingInterface {
        void queDing(String grabNo, String price);
    }

    public static class OrderListItem extends ExpandableRecyclerAdapter.ListItem {
        public QiangDanDetail.DataBean.TasksBean mTasksBean;
        public QiangDanDetail.DataBean.TasksBean.ItemsBean mItemsBean;
        public int mGroupPosition;
        public int mChildPosition;
        public String mGrabNo;
        public String mPrice;

        public OrderListItem(QiangDanDetail.DataBean.TasksBean tasksBean) {
            super(QIANG_DAN_DETAIL_HEAD);
            mTasksBean = tasksBean;
        }

        public OrderListItem(int groupPosition, QiangDanDetail.DataBean.TasksBean.ItemsBean itemsBean, int childPosition) {
            super(QIANG_DAN_DETAIL_MIDDLE);
            mItemsBean = itemsBean;
            mGroupPosition = groupPosition;
            mChildPosition = childPosition;
        }

        public OrderListItem(String first, String last, String footer, String type) {
            super(QIANG_DAN_DETAIL_FOTTER);
            mGrabNo = first;
            mPrice = last;
        }
    }

    public class ShangJiaNameViewHolder extends ViewHolder {

        private TextView tv_name;
        private TextView tv_qu_huo_di_zhi;
        private TextView tv_song_huo_di_zhi;
        private CheckBox cb_cang_ku_name;

        public ShangJiaNameViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_che_pai_hao);
            tv_qu_huo_di_zhi = view.findViewById(R.id.tv_qu_huo_di_zhi);
            tv_song_huo_di_zhi = view.findViewById(R.id.tv_song_huo_di_zhi);
            cb_cang_ku_name = view.findViewById(R.id.cb_cang_ku_name);
        }

        public void bind(int position) {
            QiangDanDetail.DataBean.TasksBean tasksBean = visibleItems.get(position).mTasksBean;
            tv_name.setText("任务单号：" + tasksBean.getTaskNo());
            tv_qu_huo_di_zhi.setText("取货地址：" + tasksBean.getGetAddress());
            tv_song_huo_di_zhi.setText("送货地址：" + tasksBean.getDeliverAddress());

            final QiangDanDetail.DataBean.TasksBean group = (QiangDanDetail.DataBean.TasksBean) getGroup(mGroupsPosition.get(position));
            cb_cang_ku_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    group.setChoosed(((CheckBox) v).isChecked());
                    checkInterface.checkGroup(mGroupsPosition.get(position), ((CheckBox) v).isChecked());// 暴露组选接口
                }
            });

//            cb_cang_ku_name.setChecked(group.isChoosed());

        }
    }

    public class ProductsViewHolder extends ViewHolder {

        private TextView tv_name;
        private TextView tv_num;
        private TextView tv_dan_wei;
        private TextView tv_sao_mao;
        private CheckBox cb_product_name;

        public ProductsViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_che_pai_hao);
            tv_num = view.findViewById(R.id.tv_num);
            tv_dan_wei = view.findViewById(R.id.tv_dan_wei);
            cb_product_name = view.findViewById(R.id.cb_product_name);
            tv_sao_mao = view.findViewById(R.id.tv_sao_mao);
        }

        public void bind(int position) {
            QiangDanDetail.DataBean.TasksBean.ItemsBean materialDriverMappingChanPin = visibleItems.get(position).mItemsBean;
            tv_name.setText(materialDriverMappingChanPin.getMaterialName());
            tv_num.setText("数量：" + materialDriverMappingChanPin.getMaterialNum());
            tv_dan_wei.setText("单位：" + materialDriverMappingChanPin.getMaterialUnit());

            int groupPosition = visibleItems.get(position).mGroupPosition;
            int childPosition = visibleItems.get(position).mChildPosition;

            final QiangDanDetail.DataBean.TasksBean.ItemsBean goodsInfo2 = (QiangDanDetail.DataBean.TasksBean.ItemsBean) getChild(groupPosition, childPosition);

//            cb_product_name.setChecked(goodsInfo2.isChoosed());

//            if (goodsInfo2.getIsbeginscan().equals("1")) {
//                tv_sao_mao.setText("已扫描");
//            } else {
//                tv_sao_mao.setText("未扫描");
//            }

            cb_product_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    goodsInfo2.setChoosed(((CheckBox) v).isChecked());
                    cb_product_name.setChecked(((CheckBox) v).isChecked());
                    checkInterface.checkChild(groupPosition, position, ((CheckBox) v).isChecked());// 暴露子选接口

                }
            });

        }
    }

    public class FooterViewHolder extends ViewHolder {

        private Button btn_que_ding;

        public FooterViewHolder(View view) {
            super(view);
            btn_que_ding = view.findViewById(R.id.btn_que_ding);
        }

        public void bind(int position) {
            btn_que_ding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String grabNo = visibleItems.get(position).mGrabNo;
                    String price = visibleItems.get(position).mPrice;
                    mWanCheng.queDing(grabNo, price);

                }
            });
        }
    }
}
