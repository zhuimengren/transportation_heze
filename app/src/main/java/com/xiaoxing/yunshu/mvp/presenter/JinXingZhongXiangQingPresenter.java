package com.xiaoxing.yunshu.mvp.presenter;

import android.app.Application;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;
import com.xiaoxing.yunshu.mvp.contract.JinXingZhongXiangQingContract;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.FinishVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.JinXingZhongXiangQing;
import com.xiaoxing.yunshu.mvp.ui.entity.SendFinishVerifyNotice;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import okhttp3.RequestBody;


@ActivityScope
public class JinXingZhongXiangQingPresenter extends BasePresenter<JinXingZhongXiangQingContract.Model, JinXingZhongXiangQingContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public JinXingZhongXiangQingPresenter(JinXingZhongXiangQingContract.Model model, JinXingZhongXiangQingContract.View rootView) {
        super(model, rootView);
    }

    public void getJinXingZhongXiangQingList(Map<String, String> map) {

        mModel.getJinXingZhongXiangQingList(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<JinXingZhongXiangQing>(mErrorHandler) {
                    @Override
                    public void onNext(JinXingZhongXiangQing entityList) {
                        mRootView.getJinXingZhongXiangQingDataSuccess(entityList);
                    }
                });
    }

    public void sendFinishVerifyNotice(Map<String, String> map) {

        mModel.sendFinishVerifyNotice(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<SendFinishVerifyNotice>(mErrorHandler) {
                    @Override
                    public void onNext(SendFinishVerifyNotice entityList) {
                        mRootView.sendFinishVerifyNoticeSuccess(entityList);
                    }
                });
    }

    public void finishScanVerify(Map<String, String> map) {

        mModel.finishScanVerify(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<FinishScanVerify>(mErrorHandler) {
                    @Override
                    public void onNext(FinishScanVerify entityList) {
                        mRootView.finishScanVerifySuccess(entityList);
                    }
                });
    }

    public void finishMaterialPhotoVerify(RequestBody map) {

        mModel.finishMaterialPhotoVerify(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<FinishMaterialPhotoVerify>(mErrorHandler) {
                    @Override
                    public void onNext(FinishMaterialPhotoVerify entityList) {
                        mRootView.finishMaterialPhotoVerifySuccess(entityList);
                    }
                });
    }

    public void finishPhotoVerify(RequestBody map) {

        mModel.finishPhotoVerify(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<FinishPhotoVerify>(mErrorHandler) {
                    @Override
                    public void onNext(FinishPhotoVerify entityList) {
                        mRootView.finishPhotoVerifySuccess(entityList);
                    }
                });
    }

    public void finishVerify(Map<String, String> map) {

        mModel.finishVerify(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<FinishVerify>(mErrorHandler) {
                    @Override
                    public void onNext(FinishVerify entityList) {
                        mRootView.finishVerifySuccess(entityList);
                    }
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
