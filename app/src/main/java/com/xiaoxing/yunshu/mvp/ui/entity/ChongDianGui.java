package com.xiaoxing.yunshu.mvp.ui.entity;

public class ChongDianGui {

    private String id;
    private Object imgPath;
    private String xingHao;
    private String keYongShuLiang;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getImgPath() {
        return imgPath;
    }

    public void setImgPath(Object imgPath) {
        this.imgPath = imgPath;
    }

    public String getXingHao() {
        return xingHao;
    }

    public void setXingHao(String xingHao) {
        this.xingHao = xingHao;
    }

    public String getKeYongShuLiang() {
        return keYongShuLiang;
    }

    public void setKeYongShuLiang(String keYongShuLiang) {
        this.keYongShuLiang = keYongShuLiang;
    }
}
