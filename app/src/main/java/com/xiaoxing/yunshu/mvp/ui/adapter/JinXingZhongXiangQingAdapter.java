package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.JinXingZhongXiangQing;

import java.util.List;


public class JinXingZhongXiangQingAdapter extends BaseQuickAdapter<JinXingZhongXiangQing.DataBean.ItemsBean, BaseViewHolder> {

    private Context mContext;
    private IJinXingZHongXiangQingInterface mIJinXingZHongXiangQingInterface;

    public JinXingZhongXiangQingAdapter(Context context, @Nullable List<JinXingZhongXiangQing.DataBean.ItemsBean> data) {
        super(R.layout.item_jin_xing_zhong_xiang_qing, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, JinXingZhongXiangQing.DataBean.ItemsBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));
        helper.setText(R.id.tv_wu_liao_ming_cheng, item.getMaterialName());
        helper.setText(R.id.tv_shu_liang, "数量：" + item.getMaterialNum());
        helper.setText(R.id.tv_dan_wei, "单位：" + item.getMaterialUnit());
        Button btn_sao_mao = helper.getView(R.id.btn_sao_mao);
        Button btn_pai_zhao = helper.getView(R.id.btn_pai_zhao);

//        if (item.isIsEndVerify()) {
//            btn_sao_mao.setText("已核对(扫描)");
//            btn_pai_zhao.setText("已核对(拍照)");
//        } else {
//            btn_sao_mao.setText("未核对(扫描)");
//            btn_pai_zhao.setText("未核对(拍照)");
//        }
//
//        if (item.isFromOutStorage()) {
//            btn_sao_mao.setVisibility(View.VISIBLE);
//            btn_pai_zhao.setVisibility(View.GONE);
//        } else {
//            btn_sao_mao.setVisibility(View.GONE);
//            btn_pai_zhao.setVisibility(View.VISIBLE);
//        }
        btn_sao_mao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mIJinXingZHongXiangQingInterface.saoMiaoErWeiMa(item);
            }
        });
        btn_pai_zhao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mIJinXingZHongXiangQingInterface.paiZhao(item);

            }
        });
    }

    public void setCheckInterface(IJinXingZHongXiangQingInterface checkInterface) {
        this.mIJinXingZHongXiangQingInterface = checkInterface;
    }


    public interface IJinXingZHongXiangQingInterface {

        void saoMiaoErWeiMa(JinXingZhongXiangQing.DataBean.ItemsBean itemsBean);

        void paiZhao(JinXingZhongXiangQing.DataBean.ItemsBean itemsBean);

    }
}
