package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.QiangDanSubmitContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.QiangDanSubmit;

import io.reactivex.Observable;
import okhttp3.RequestBody;


@ActivityScope
public class QiangDanSubmitModel extends BaseModel implements QiangDanSubmitContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public QiangDanSubmitModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<QiangDanSubmit> getQiangDanSubmitData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getQiangDanSubmitData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}