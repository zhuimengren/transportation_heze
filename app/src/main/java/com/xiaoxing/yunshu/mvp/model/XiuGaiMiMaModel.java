package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.XiuGaiMiMaContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.XiuGaiMiMa;

import io.reactivex.Observable;


@ActivityScope
public class XiuGaiMiMaModel extends BaseModel implements XiuGaiMiMaContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public XiuGaiMiMaModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<XiuGaiMiMa> getXiuGaiMiMaData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getXiuGaiMiMaData(map);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}