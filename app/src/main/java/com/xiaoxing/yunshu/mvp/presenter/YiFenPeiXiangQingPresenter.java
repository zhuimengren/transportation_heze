package com.xiaoxing.yunshu.mvp.presenter;

import android.app.Application;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;
import com.xiaoxing.yunshu.mvp.contract.YiFenPeiXiangQingContract;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadedVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.StartDistibute;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPeiXiangQing;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import okhttp3.RequestBody;


@ActivityScope
public class YiFenPeiXiangQingPresenter extends BasePresenter<YiFenPeiXiangQingContract.Model, YiFenPeiXiangQingContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public YiFenPeiXiangQingPresenter(YiFenPeiXiangQingContract.Model model, YiFenPeiXiangQingContract.View rootView) {
        super(model, rootView);
    }

    public void getYiFenPeiXiangQingList(Map<String, String> map) {

        mModel.getYiFenPeiXiangQingList(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<YiFenPeiXiangQing>(mErrorHandler) {
                    @Override
                    public void onNext(YiFenPeiXiangQing entityList) {
                        mRootView.getYiFenPeiXiangQingDataSuccess(entityList);
                    }
                });
    }

    public void loadScanVerify(Map<String, String> map) {

        mModel.loadScanVerify(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<LoadScanVerify>(mErrorHandler) {
                    @Override
                    public void onNext(LoadScanVerify entityList) {
                        mRootView.loadScanVerifySuccess(entityList);
                    }
                });
    }

    public void loadMaterialPhotoVerify(RequestBody info) {

        mModel.loadMaterialPhotoVerify(info).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<LoadMaterialPhotoVerify>(mErrorHandler) {
                    @Override
                    public void onNext(LoadMaterialPhotoVerify entityList) {
                        mRootView.loadMaterialPhotoVerifySuccess(entityList);
                    }
                });
    }

    public void loadPhotoVerify(RequestBody info) {

        mModel.loadPhotoVerify(info).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<LoadPhotoVerify>(mErrorHandler) {
                    @Override
                    public void onNext(LoadPhotoVerify entityList) {
                        mRootView.loadPhotoVerifySuccess(entityList);
                    }
                });
    }

    public void loadedVerify(Map<String, String> map) {

        mModel.loadedVerify(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<LoadedVerify>(mErrorHandler) {
                    @Override
                    public void onNext(LoadedVerify entityList) {
                        mRootView.loadedVerifySuccess(entityList);
                    }
                });
    }

    public void startDistibute(Map<String, String> map) {

        mModel.startDistibute(map).subscribeOn(Schedulers.io())
                //                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<StartDistibute>(mErrorHandler) {
                    @Override
                    public void onNext(StartDistibute entityList) {
                        mRootView.startDistibuteSuccess(entityList);
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
