package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerJieSuanDanComponent;
import com.xiaoxing.yunshu.di.module.JieSuanDanModule;
import com.xiaoxing.yunshu.mvp.contract.JieSuanDanContract;
import com.xiaoxing.yunshu.mvp.presenter.JieSuanDanPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.JieSuanDan;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_JIE_SUAN_DAN)
public class JieSuanDanActivity extends BaseActivity<JieSuanDanPresenter> implements JieSuanDanContract.View {


    @BindView(R.id.tv_jie_suan_dan_hao)
    TextView tvJieSuanDanHao;
    @BindView(R.id.tv_jie_suan_jin_e)
    TextView tvJieSuanJinE;
    @BindView(R.id.tv_pei_song_yuan)
    TextView tvPeiSongYuan;
    @BindView(R.id.tv_shen_fen_zheng_hao)
    TextView tvShenFenZhengHao;
    @BindView(R.id.tv_shou_ji)
    TextView tvShouJi;
    @BindView(R.id.tv_che_pai_hao)
    TextView tvChePaiHao;
    @BindView(R.id.tv_jie_suan_ren)
    TextView tvJieSuanRen;
    @BindView(R.id.tv_jie_suan_shi_jian)
    TextView tvJieSuanShiJian;
    private String grabNo;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerJieSuanDanComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .jieSuanDanModule(new JieSuanDanModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_jie_suan_dan; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_jie_suan_dan));
        grabNo = getIntent().getExtras().getString("grabNo");
        getJieSuanDanData();
    }

    private void getJieSuanDanData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("grabNo", grabNo);
        mPresenter.getJieSuanDanData(map);
    }

    @Override
    public void getJieSuanDanDataSuccess(JieSuanDan entityData) {


        if (entityData != null && entityData.getData() != null) {
            JieSuanDan.DataBean dataBean = entityData.getData();
            tvJieSuanDanHao.setText(dataBean.getStatementNo());
            tvJieSuanJinE.setText(String.valueOf(dataBean.getAccountAmount()));
            tvPeiSongYuan.setText(dataBean.getDriverName());
            tvShenFenZhengHao.setText(dataBean.getIDCardNo());
            tvShouJi.setText(dataBean.getDriverPhone());
            tvChePaiHao.setText(dataBean.getTruckNumber());
            tvJieSuanRen.setText(dataBean.getAccountUserName());
            tvJieSuanShiJian.setText(dataBean.getAccountDateTime());

        }
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }


}
