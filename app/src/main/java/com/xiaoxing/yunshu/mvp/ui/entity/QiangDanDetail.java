package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class QiangDanDetail {


    /**
     * Code : 200
     * Message : 获取抢单详细信息成功！
     * Data : {"GrabNo":"PG202002050002","RefPrice":2000,"Remark":"dsfdsfdsfdsf\nsfdsfdsfdsfdsf","GrabStatus":2,"GrabStatusDesc":"已发布","DriverCount":0,"PublishUserId":174,"PublishUserName":"配送管理员","PublishDateTime":"2020-02-05 12:04:07","InsertUserId":174,"InsertUserName":"配送管理员","InsertDateTime":"2020-02-05 12:03:56","Tasks":[{"TaskNo":"PT202002050002","DeliverAddress":"禹城","GetAddress":"测试地址","OutStoreId":45,"ConsigneeName":"liu","ConsigneePhone":"1010","DistributeStatus":3,"IsFromOutStorage":true,"GrabNo":"PG202002050002","InsertUserId":174,"InsertDateTime":"2020-02-05 10:10:45","OutStore":{"ID":45,"StoreName":"测试仓库","StoreAddress":"测试地址"},"Items":[{"ItemId":5,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,150mm2,双孔","MaterialNum":60,"MaterialUnit":"只"},{"ItemId":6,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,50mm2,单孔","MaterialNum":44,"MaterialUnit":"只"},{"ItemId":7,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,70mm2,单孔","MaterialNum":100,"MaterialUnit":"只"}]},{"TaskNo":"PT202002050003","DeliverAddress":"北关","GetAddress":"测试地址","OutStoreId":45,"ConsigneeName":"地方","ConsigneePhone":"2456445456","DistributeStatus":3,"IsFromOutStorage":true,"GrabNo":"PG202002050002","InsertUserId":174,"InsertDateTime":"2020-02-05 12:03:09","OutStore":{"ID":45,"StoreName":"测试仓库","StoreAddress":"测试地址"},"Items":[{"ItemId":8,"TaskNo":"PT202002050003","MaterialName":"挂板PD-12100","MaterialNum":24,"MaterialUnit":"套"},{"ItemId":9,"TaskNo":"PT202002050003","MaterialName":"联板L-12-70/400","MaterialNum":24,"MaterialUnit":"套"},{"ItemId":10,"TaskNo":"PT202002050003","MaterialName":"耐张线夹NLL-4","MaterialNum":24,"MaterialUnit":"套"},{"ItemId":11,"TaskNo":"PT202002050003","MaterialName":"直角挂板BDZ-1290","MaterialNum":24,"MaterialUnit":"套"}]}]}
     */

    private int Code;
    private String Message;
    private DataBean Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * GrabNo : PG202002050002
         * RefPrice : 2000.0
         * Remark : dsfdsfdsfdsf
         * sfdsfdsfdsfdsf
         * GrabStatus : 2
         * GrabStatusDesc : 已发布
         * DriverCount : 0
         * PublishUserId : 174
         * PublishUserName : 配送管理员
         * PublishDateTime : 2020-02-05 12:04:07
         * InsertUserId : 174
         * InsertUserName : 配送管理员
         * InsertDateTime : 2020-02-05 12:03:56
         * Tasks : [{"TaskNo":"PT202002050002","DeliverAddress":"禹城","GetAddress":"测试地址","OutStoreId":45,"ConsigneeName":"liu","ConsigneePhone":"1010","DistributeStatus":3,"IsFromOutStorage":true,"GrabNo":"PG202002050002","InsertUserId":174,"InsertDateTime":"2020-02-05 10:10:45","OutStore":{"ID":45,"StoreName":"测试仓库","StoreAddress":"测试地址"},"Items":[{"ItemId":5,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,150mm2,双孔","MaterialNum":60,"MaterialUnit":"只"},{"ItemId":6,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,50mm2,单孔","MaterialNum":44,"MaterialUnit":"只"},{"ItemId":7,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,70mm2,单孔","MaterialNum":100,"MaterialUnit":"只"}]},{"TaskNo":"PT202002050003","DeliverAddress":"北关","GetAddress":"测试地址","OutStoreId":45,"ConsigneeName":"地方","ConsigneePhone":"2456445456","DistributeStatus":3,"IsFromOutStorage":true,"GrabNo":"PG202002050002","InsertUserId":174,"InsertDateTime":"2020-02-05 12:03:09","OutStore":{"ID":45,"StoreName":"测试仓库","StoreAddress":"测试地址"},"Items":[{"ItemId":8,"TaskNo":"PT202002050003","MaterialName":"挂板PD-12100","MaterialNum":24,"MaterialUnit":"套"},{"ItemId":9,"TaskNo":"PT202002050003","MaterialName":"联板L-12-70/400","MaterialNum":24,"MaterialUnit":"套"},{"ItemId":10,"TaskNo":"PT202002050003","MaterialName":"耐张线夹NLL-4","MaterialNum":24,"MaterialUnit":"套"},{"ItemId":11,"TaskNo":"PT202002050003","MaterialName":"直角挂板BDZ-1290","MaterialNum":24,"MaterialUnit":"套"}]}]
         */

        private String GrabNo;
        private double RefPrice;
        private String Remark;
        private int GrabStatus;
        private String GrabStatusDesc;
        private int DriverCount;
        private int PublishUserId;
        private String PublishUserName;
        private String PublishDateTime;
        private int InsertUserId;
        private String InsertUserName;
        private String InsertDateTime;
        private List<TasksBean> Tasks;

        public String getGrabNo() {
            return GrabNo;
        }

        public void setGrabNo(String GrabNo) {
            this.GrabNo = GrabNo;
        }

        public double getRefPrice() {
            return RefPrice;
        }

        public void setRefPrice(double RefPrice) {
            this.RefPrice = RefPrice;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }

        public int getGrabStatus() {
            return GrabStatus;
        }

        public void setGrabStatus(int GrabStatus) {
            this.GrabStatus = GrabStatus;
        }

        public String getGrabStatusDesc() {
            return GrabStatusDesc;
        }

        public void setGrabStatusDesc(String GrabStatusDesc) {
            this.GrabStatusDesc = GrabStatusDesc;
        }

        public int getDriverCount() {
            return DriverCount;
        }

        public void setDriverCount(int DriverCount) {
            this.DriverCount = DriverCount;
        }

        public int getPublishUserId() {
            return PublishUserId;
        }

        public void setPublishUserId(int PublishUserId) {
            this.PublishUserId = PublishUserId;
        }

        public String getPublishUserName() {
            return PublishUserName;
        }

        public void setPublishUserName(String PublishUserName) {
            this.PublishUserName = PublishUserName;
        }

        public String getPublishDateTime() {
            return PublishDateTime;
        }

        public void setPublishDateTime(String PublishDateTime) {
            this.PublishDateTime = PublishDateTime;
        }

        public int getInsertUserId() {
            return InsertUserId;
        }

        public void setInsertUserId(int InsertUserId) {
            this.InsertUserId = InsertUserId;
        }

        public String getInsertUserName() {
            return InsertUserName;
        }

        public void setInsertUserName(String InsertUserName) {
            this.InsertUserName = InsertUserName;
        }

        public String getInsertDateTime() {
            return InsertDateTime;
        }

        public void setInsertDateTime(String InsertDateTime) {
            this.InsertDateTime = InsertDateTime;
        }

        public List<TasksBean> getTasks() {
            return Tasks;
        }

        public void setTasks(List<TasksBean> Tasks) {
            this.Tasks = Tasks;
        }

        public static class TasksBean {
            /**
             * TaskNo : PT202002050002
             * DeliverAddress : 禹城
             * GetAddress : 测试地址
             * OutStoreId : 45
             * ConsigneeName : liu
             * ConsigneePhone : 1010
             * DistributeStatus : 3
             * IsFromOutStorage : true
             * GrabNo : PG202002050002
             * InsertUserId : 174
             * InsertDateTime : 2020-02-05 10:10:45
             * OutStore : {"ID":45,"StoreName":"测试仓库","StoreAddress":"测试地址"}
             * Items : [{"ItemId":5,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,150mm2,双孔","MaterialNum":60,"MaterialUnit":"只"},{"ItemId":6,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,50mm2,单孔","MaterialNum":44,"MaterialUnit":"只"},{"ItemId":7,"TaskNo":"PT202002050002","MaterialName":"电缆接线端子,铜,70mm2,单孔","MaterialNum":100,"MaterialUnit":"只"}]
             */

            private String TaskNo;
            private String DeliverAddress;
            private String GetAddress;
            private int OutStoreId;
            private String ConsigneeName;
            private String ConsigneePhone;
            private int DistributeStatus;
            private boolean IsFromOutStorage;
            private String GrabNo;
            private int InsertUserId;
            private String InsertDateTime;
            private OutStoreBean OutStore;
            private List<ItemsBean> Items;

            public String getTaskNo() {
                return TaskNo;
            }

            public void setTaskNo(String TaskNo) {
                this.TaskNo = TaskNo;
            }

            public String getDeliverAddress() {
                return DeliverAddress;
            }

            public void setDeliverAddress(String DeliverAddress) {
                this.DeliverAddress = DeliverAddress;
            }

            public String getGetAddress() {
                return GetAddress;
            }

            public void setGetAddress(String GetAddress) {
                this.GetAddress = GetAddress;
            }

            public int getOutStoreId() {
                return OutStoreId;
            }

            public void setOutStoreId(int OutStoreId) {
                this.OutStoreId = OutStoreId;
            }

            public String getConsigneeName() {
                return ConsigneeName;
            }

            public void setConsigneeName(String ConsigneeName) {
                this.ConsigneeName = ConsigneeName;
            }

            public String getConsigneePhone() {
                return ConsigneePhone;
            }

            public void setConsigneePhone(String ConsigneePhone) {
                this.ConsigneePhone = ConsigneePhone;
            }

            public int getDistributeStatus() {
                return DistributeStatus;
            }

            public void setDistributeStatus(int DistributeStatus) {
                this.DistributeStatus = DistributeStatus;
            }

            public boolean isIsFromOutStorage() {
                return IsFromOutStorage;
            }

            public void setIsFromOutStorage(boolean IsFromOutStorage) {
                this.IsFromOutStorage = IsFromOutStorage;
            }

            public String getGrabNo() {
                return GrabNo;
            }

            public void setGrabNo(String GrabNo) {
                this.GrabNo = GrabNo;
            }

            public int getInsertUserId() {
                return InsertUserId;
            }

            public void setInsertUserId(int InsertUserId) {
                this.InsertUserId = InsertUserId;
            }

            public String getInsertDateTime() {
                return InsertDateTime;
            }

            public void setInsertDateTime(String InsertDateTime) {
                this.InsertDateTime = InsertDateTime;
            }

            public OutStoreBean getOutStore() {
                return OutStore;
            }

            public void setOutStore(OutStoreBean OutStore) {
                this.OutStore = OutStore;
            }

            public List<ItemsBean> getItems() {
                return Items;
            }

            public void setItems(List<ItemsBean> Items) {
                this.Items = Items;
            }

            public static class OutStoreBean {
                /**
                 * ID : 45
                 * StoreName : 测试仓库
                 * StoreAddress : 测试地址
                 */

                private int ID;
                private String StoreName;
                private String StoreAddress;

                public int getID() {
                    return ID;
                }

                public void setID(int ID) {
                    this.ID = ID;
                }

                public String getStoreName() {
                    return StoreName;
                }

                public void setStoreName(String StoreName) {
                    this.StoreName = StoreName;
                }

                public String getStoreAddress() {
                    return StoreAddress;
                }

                public void setStoreAddress(String StoreAddress) {
                    this.StoreAddress = StoreAddress;
                }
            }

            public static class ItemsBean {
                /**
                 * ItemId : 5
                 * TaskNo : PT202002050002
                 * MaterialName : 电缆接线端子,铜,150mm2,双孔
                 * MaterialNum : 60.0
                 * MaterialUnit : 只
                 */

                private int ItemId;
                private String TaskNo;
                private String MaterialName;
                private double MaterialNum;
                private String MaterialUnit;

                public int getItemId() {
                    return ItemId;
                }

                public void setItemId(int ItemId) {
                    this.ItemId = ItemId;
                }

                public String getTaskNo() {
                    return TaskNo;
                }

                public void setTaskNo(String TaskNo) {
                    this.TaskNo = TaskNo;
                }

                public String getMaterialName() {
                    return MaterialName;
                }

                public void setMaterialName(String MaterialName) {
                    this.MaterialName = MaterialName;
                }

                public double getMaterialNum() {
                    return MaterialNum;
                }

                public void setMaterialNum(double MaterialNum) {
                    this.MaterialNum = MaterialNum;
                }

                public String getMaterialUnit() {
                    return MaterialUnit;
                }

                public void setMaterialUnit(String MaterialUnit) {
                    this.MaterialUnit = MaterialUnit;
                }
            }
        }
    }
}
