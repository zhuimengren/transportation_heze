package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.JieChuJiLu;

import java.util.List;


public class JieChuJiLuAdapter extends BaseQuickAdapter<JieChuJiLu, BaseViewHolder> {

    private Context mContext;

    public JieChuJiLuAdapter(Context context, @Nullable List<JieChuJiLu> data) {
        super(R.layout.item_jie_chu_ji_lu, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, JieChuJiLu item) {
        helper.setText(R.id.tv_xing_hao, item.getXingHao());
        helper.setText(R.id.tv_ri_qi, item.getShiJian());
        helper.setText(R.id.tv_jian_shu, "借出" + item.getJianShu() + "件");
    }

}
