package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerWanChengComponent;
import com.xiaoxing.yunshu.di.module.WanChengModule;
import com.xiaoxing.yunshu.mvp.contract.WanChengContract;
import com.xiaoxing.yunshu.mvp.presenter.WanChengPresenter;

import butterknife.OnClick;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;

import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.APP_YUN_SHU_WAN_CHENG_MAIN_ACTIVITY)
public class WanChengActivity extends BaseActivity
        <WanChengPresenter> implements WanChengContract.View

{


    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerWanChengComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .wanChengModule(new WanChengModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_wan_cheng; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, "配送完成");


    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }


    @OnClick(R.id.btn_que_ding)
    public void onViewClicked() {
        ArmsUtils.snackbarText("配送完成");
    }
}
