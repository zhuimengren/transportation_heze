package com.xiaoxing.yunshu.mvp.ui.entity;

public class GeRenXinXi {


    /**
     * Code : 200
     * Message : 更新成功！
     * Data : null
     */

    private int Code;
    private String Message;
    private Object Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object Data) {
        this.Data = Data;
    }
}
