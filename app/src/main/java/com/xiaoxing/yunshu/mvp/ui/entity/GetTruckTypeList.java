package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2020/2/10 0010 20:51
 */
public class GetTruckTypeList {


    /**
     * Code : 200
     * Message : 获取车辆类型列表成功！
     * Data : [{"BaseInformationNo":"TruckType","BaseInformationId":1,"BaseInformationContent":"重型货车"},{"BaseInformationNo":"TruckType","BaseInformationId":2,"BaseInformationContent":"中型货车"},{"BaseInformationNo":"TruckType","BaseInformationId":3,"BaseInformationContent":"轻型货车"},{"BaseInformationNo":"TruckType","BaseInformationId":4,"BaseInformationContent":"微型货车"}]
     */

    private int Code;
    private String Message;
    private List<DataBean> Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * BaseInformationNo : TruckType
         * BaseInformationId : 1
         * BaseInformationContent : 重型货车
         */

        private String BaseInformationNo;
        private int BaseInformationId;
        private String BaseInformationContent;

        public String getBaseInformationNo() {
            return BaseInformationNo;
        }

        public void setBaseInformationNo(String BaseInformationNo) {
            this.BaseInformationNo = BaseInformationNo;
        }

        public int getBaseInformationId() {
            return BaseInformationId;
        }

        public void setBaseInformationId(int BaseInformationId) {
            this.BaseInformationId = BaseInformationId;
        }

        public String getBaseInformationContent() {
            return BaseInformationContent;
        }

        public void setBaseInformationContent(String BaseInformationContent) {
            this.BaseInformationContent = BaseInformationContent;
        }
    }
}
