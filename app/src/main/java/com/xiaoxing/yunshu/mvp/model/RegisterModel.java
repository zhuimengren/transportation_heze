package com.xiaoxing.yunshu.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.Map;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;

import com.xiaoxing.yunshu.mvp.contract.RegisterContract;


import com.xiaoxing.yunshu.mvp.api.ApiService;
import com.xiaoxing.yunshu.mvp.ui.entity.Register;
import com.xiaoxing.yunshu.mvp.ui.entity.RegisterSendValidateCode;
import com.xiaoxing.yunshu.mvp.ui.entity.SendCode;

import io.reactivex.Observable;


@ActivityScope
public class RegisterModel extends BaseModel implements RegisterContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public RegisterModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<Register> getRegisterData(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).getRegisterData(map);

    }

    @Override
    public Observable<RegisterSendValidateCode> registerSendValidateCode(Map<String, String> map) {
        return mRepositoryManager.obtainRetrofitService(ApiService.class).registerSendValidateCode(map);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }
}