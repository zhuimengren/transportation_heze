package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.dds.soap.SoapListener;
import com.dds.soap.SoapParams;
import com.dds.soap.SoapUtil;
import com.google.gson.Gson;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.base.BaseConstants;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;

import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerOrdersComponent;
import com.xiaoxing.yunshu.di.module.OrdersModule;
import com.xiaoxing.yunshu.mvp.contract.OrdersContract;
import com.xiaoxing.yunshu.mvp.presenter.OrdersPresenter;
import com.xiaoxing.yunshu.mvp.ui.entity.NotiExtras;
import com.xw.repo.XEditText;

import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import me.jessyan.armscomponent.commonsdk.utils.Utils;

import static com.jess.arms.base.BaseConstants.NAME_SPACE;
import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.APP_YUN_SHU_ORDERS_ACTIVITY)
public class OrdersActivity extends BaseActivity
        <OrdersPresenter> implements OrdersContract.View {

    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.btn_hulue)
    Button btnHulue;
    @BindView(R.id.btn_qiang_dan)
    Button btnQiangDan;

    @BindView(R.id.xet_bidmoney)
    XEditText xetBidmoney;

    private String batchId;
    private String address;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerOrdersComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .ordersModule(new OrdersModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_orders; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, "抢单");


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            String extraExtra = bundle.getString(JPushInterface.EXTRA_EXTRA);
            Gson gson = new Gson();
            NotiExtras notiExtras = gson.fromJson(extraExtra, NotiExtras.class);
            batchId = notiExtras.getBatchId();
            address = notiExtras.getAddress();
            String title = bundle.getString("cn.jpush.android.NOTIFICATION_CONTENT_TITLE");
            String content = bundle.getString("cn.jpush.android.ALERT");

            if (!TextUtils.isEmpty(content)) {
                tvDesc.setText(content);
            } else {
                tvDesc.setText("");
            }
        }
        if (TextUtils.isEmpty(mSharedPreferencesHelper.getString(BaseConstants.UID))) {
            ArmsUtils.makeText(this, "请先登录");
            ARouter.getInstance().build(RouterHub.APP_LOGIN_ACTIVITY).withString("orders", "1").navigation();
            return;
        }


    }

    @OnClick({R.id.btn_hulue, R.id.btn_qiang_dan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_hulue:
                if (TextUtils.isEmpty(mSharedPreferencesHelper.getString(BaseConstants.SHE_ZHI_IP)) || TextUtils.isEmpty(mSharedPreferencesHelper.getString(BaseConstants.SHE_ZHI_PORT))) {
                    Utils.navigation(OrdersActivity.this, RouterHub.APP_SHE_ZHI_ACTIVITY);
                    return;
                }
                if (TextUtils.isEmpty(getBidmoney())) {
                    ArmsUtils.makeText(this, "请输入金额");
                    return;
                }

                callWebService();
                break;
            case R.id.btn_qiang_dan:
                killMyself();
                break;
        }
    }

    @NonNull
    private String getBidmoney() {
        return xetBidmoney.getText().toString().trim();
    }

    private void callWebService() {

        showLoading();

        String methodName = "BidForTransportTask";

        SoapUtil soapUtil = SoapUtil.getInstance(this);
        soapUtil.setDotNet(true);
        SoapParams params = new SoapParams();
        params.put("batchId", batchId);
        params.put("driverId", mSharedPreferencesHelper.getString(BaseConstants.UID));
        params.put("bidmoney", getBidmoney());

        /**
         * 请求
         */
        soapUtil.callEntry(getUrl(), NAME_SPACE, methodName, params, new SoapListener() {
            @Override
            public void onSuccess(int statusCode, SoapObject object) {
                hideLoading();
                ArmsUtils.makeText(OrdersActivity.this, "抢单成功，等待审核中，审核通过后会短信通知到您");
                Utils.navigation(OrdersActivity.this, RouterHub.APP_LOGIN_ACTIVITY);
                killMyself();
            }

            @Override
            public void onFailure(int statusCode, String content, Throwable error) {
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, SoapFault fault) {
                hideLoading();
            }
        }, getAapiUserName(), getApiPwd());


    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }


}
