package com.xiaoxing.yunshu.mvp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.hss01248.frescopicker.FrescoIniter;
import com.hss01248.photoouter.PhotoCallback;
import com.hss01248.photoouter.PhotoUtil;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.jess.arms.utils.LogUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.di.component.DaggerYiFenPeiXiangQingComponent;
import com.xiaoxing.yunshu.di.module.YiFenPeiXiangQingModule;
import com.xiaoxing.yunshu.mvp.contract.YiFenPeiXiangQingContract;
import com.xiaoxing.yunshu.mvp.presenter.YiFenPeiXiangQingPresenter;
import com.xiaoxing.yunshu.mvp.ui.adapter.YiFenPeiXiangQing1Adapter;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadMaterialPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadPhotoVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadScanVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.LoadedVerify;
import com.xiaoxing.yunshu.mvp.ui.entity.StartDistibute;
import com.xiaoxing.yunshu.mvp.ui.entity.YiFenPeiXiangQing;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import me.iwf.photopicker.utils.ProxyTools;
import me.jessyan.armscomponent.commonres.utils.BottomDialogUtil;
import me.jessyan.armscomponent.commonres.utils.CProgressDialogUtils;
import me.jessyan.armscomponent.commonres.utils.ToolbarUtils;
import me.jessyan.armscomponent.commonsdk.core.RouterHub;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;

import static com.jess.arms.base.BaseConstants.SCAN_REQUEST_CODE;
import static com.jess.arms.utils.Preconditions.checkNotNull;


@Route(path = RouterHub.ACTIVITY_YI_FEN_PEI_XIANG_QING)
public class YiFenPeiXiangQingActivity extends BaseActivity<YiFenPeiXiangQingPresenter> implements YiFenPeiXiangQingContract.View, OnRefreshListener, YiFenPeiXiangQing1Adapter.IYiFenPeiXiangQingInterface, YiFenPeiXiangQing1Adapter.CheckInterface {


    private static boolean mIsNeedDemo = true;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.empty_image)
    ImageView empty_image;
    @BindView(R.id.empty)
    View mEmptyLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    RefreshLayout mRefreshLayout;
    @BindView(R.id.btn_kai_shi_pei_song)
    Button btnKaiShiPeiSong;
    private YiFenPeiXiangQing1Adapter mAdapter;
    private List<YiFenPeiXiangQing.DataBean> mDataBeanList = new ArrayList<>();
    private String mGrabNo;
    //获得结果
    private Map<String, List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean>> children = new HashMap<String, List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean>>();// 子元素数据列表
    private Map<Integer, Integer> mGroupsPosition = new HashMap<>();
    private List<YiFenPeiXiangQing.DataBean.TasksBean> groups = new ArrayList<YiFenPeiXiangQing.DataBean.TasksBean>();// 组元素数据列表
    private BottomDialogUtil mBottomDialogUtilShare;
    private String mScanTaskNo;
    private String mMaterialName;
    private String mTaskNo;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerYiFenPeiXiangQingComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .yiFenPeiXiangQingModule(new YiFenPeiXiangQingModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_yi_fen_pei_xiang_qing; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        ToolbarUtils.initToolbarTitleBack(this, getString(R.string.activity_yi_fen_pei_xiang_qing));
        mGrabNo = getIntent().getExtras().getString("grabNo");
        PhotoUtil.init(this, new FrescoIniter());//第二个参数根据具体依赖库而定

        initRefreshLayout();
        initRecyclerView();
        initEmpty();

    }

    private void initRefreshLayout() {
        mRefreshLayout.setRefreshHeader(new
                ClassicsHeader(this).setSpinnerStyle(SpinnerStyle.FixedBehind).setPrimaryColorId(R.color.public_colorPrimary).setAccentColorId(android.R.color.white));
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.autoRefresh();
        mRefreshLayout.setEnableLoadMore(false);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(YiFenPeiXiangQingActivity.this));
        mRecyclerView.setAdapter(mAdapter = new YiFenPeiXiangQing1Adapter(YiFenPeiXiangQingActivity.this, groups, children, mGroupsPosition));

//        mRecyclerView.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, 10, Color.parseColor("#EEEEEE")));
//
//        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setAdapter(mAdapter = new YiFenPeiXiangQingAdapter(YiFenPeiXiangQingActivity.this, mDataBeanList = loadModels()));
//
//        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//            }
//        });
        mAdapter.setCheckInterface(this);// 关键步骤1,设置复选框接口
        mAdapter.setWanChengInterface(this);// 设置完成
    }

    private void initEmpty() {
        empty_image.setImageResource(R.drawable.ic_empty);
        empty_text.setTextColor(Color.parseColor("#CFCFCF"));
        empty_text.setText("暂无数据下拉刷新");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 101) {
            Bundle bundle = data.getExtras();
            String mid = bundle.getString("scan_result");
//            setIsScan(mid);
            if (!TextUtils.isEmpty(mid)) {

                HashMap<String, String> map = new HashMap<>();
                map.put("taskNo", mScanTaskNo);
                map.put("materialName", mMaterialName);
                map.put("materialId", mid);

                LogUtils.debugEInfo("taskNo=" + mScanTaskNo + "   materialName=" + mMaterialName + " materialId=" + mid);
                mPresenter.loadScanVerify(map);
            }
        } else {
            //onActivityResult里一行代码回调
            if (resultCode != 0)
                PhotoUtil.onActivityResult(this, requestCode, resultCode, data);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

    /**
     * 设置是否扫描
     *
     * @param mid
     */
    private void setIsScan(String mid) {
        for (int k = 0; k < groups.size(); k++) {
            YiFenPeiXiangQing.DataBean.TasksBean group = groups.get(k);
            List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean> childs = children.get(group.getId());
            for (int i = 0; i < childs.size(); i++) {
                // 不全选中
                if (String.valueOf(childs.get(i).getItemId()).equals(mid)) {
                    childs.get(i).setIsbeginscan("1");
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private List<YiFenPeiXiangQing.DataBean> loadModels() {
        List<YiFenPeiXiangQing.DataBean> lists = new ArrayList<>();

        for (int i = 0; i < 5; i++) {

            YiFenPeiXiangQing.DataBean dataBean = new YiFenPeiXiangQing.DataBean();
            lists.add(dataBean);
        }

        return lists;
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {

        getYiFenPeiXiangQingList();
        mRefreshLayout.finishRefresh();
//        mEmptyLayout.setVisibility(View.GONE);

    }

    private void getYiFenPeiXiangQingList() {
        HashMap<String, String> map = new HashMap<>();
        map.put("grabNo", mGrabNo);
        mPresenter.getYiFenPeiXiangQingList(map);
    }

    @Override
    public void getYiFenPeiXiangQingDataSuccess(YiFenPeiXiangQing entityList) {

        if (entityList != null && entityList.getData() != null) {
//            mDataBeanList.clear();
//            mDataBeanList.addAll(entityList.getData());
//            mAdapter.notifyDataSetChanged();


            List<YiFenPeiXiangQing.DataBean.TasksBean> tasksBeans = entityList.getData().getTasks();

            mAdapter.setDataItems(getSampleItems(tasksBeans));
            mAdapter.notifyDataSetChanged();
            mAdapter.expandAll();

            mEmptyLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        }

    }

    @Override
    public void loadScanVerifySuccess(LoadScanVerify entityList) {

        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {
            getYiFenPeiXiangQingList();
        }


    }

    @Override
    public void loadMaterialPhotoVerifySuccess(LoadMaterialPhotoVerify entityList) {
        showMessage(entityList.getMessage());

        if (entityList.getCode() == 200) {

            getYiFenPeiXiangQingList();
        }

    }

    @Override
    public void loadPhotoVerifySuccess(LoadPhotoVerify entityList) {
        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {

            try {
                if (!TextUtils.isEmpty(mTaskNo)) {
                    LogUtils.debugEInfo("mTaskNo222==" + mTaskNo);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("taskNo", mTaskNo);
                    map.put("addrLng", String.valueOf(mSharedPreferencesHelper.getString("longitude")));
                    map.put("addrLat", String.valueOf(mSharedPreferencesHelper.getString("latitude")));

                    mPresenter.loadedVerify(map);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


//            getYiFenPeiXiangQingList();
        }
    }

    @Override
    public void loadedVerifySuccess(LoadedVerify entityList) {
        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {
            mTaskNo = "";
            getYiFenPeiXiangQingList();
        }
    }

    @Override
    public void startDistibuteSuccess(StartDistibute entityList) {

        showMessage(entityList.getMessage());
        if (entityList.getCode() == 200) {
            btnKaiShiPeiSong.setText("配送进行中");
            btnKaiShiPeiSong.setEnabled(false);
            EventBus.getDefault().post("", "startYunShu");
            EventBus.getDefault().post("", "updateYiFenPeiList");
            EventBus.getDefault().post(1, "setPeiSongCurrentItem");
            EventBus.getDefault().post("", "updateOrdersJinXingZhongData");
            killMyself();
        }

    }

    public List<YiFenPeiXiangQing1Adapter.OrderListItem> getSampleItems(List<YiFenPeiXiangQing.DataBean.TasksBean> tasksBeans) {
        List<YiFenPeiXiangQing1Adapter.OrderListItem> items = new ArrayList<>();


        for (int i = 0; i < tasksBeans.size(); i++) {


            int groupPosition = items.size();
            mGroupsPosition.put(groupPosition, i);

            YiFenPeiXiangQing.DataBean.TasksBean tasksBean = tasksBeans.get(i);
            tasksBean.setId(String.valueOf(groupPosition));
            groups.add(tasksBeans.get(i));
            items.add(new YiFenPeiXiangQing1Adapter.OrderListItem(tasksBean));

            List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean> tasksBeanItems = tasksBean.getItems();

            List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean> products = new ArrayList<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean>();


            for (int i1 = 0; i1 < tasksBeanItems.size(); i1++) {

                YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean itemsBean = tasksBeanItems.get(i1);

                if (tasksBeans.get(i).isIsFromOutStorage()) {
                    itemsBean.setFromOutStorage(true);
                } else {
                    itemsBean.setFromOutStorage(false);

                }
                products.add(itemsBean);

                items.add(new YiFenPeiXiangQing1Adapter.OrderListItem(groupPosition, itemsBean, i1));
            }

            children.put(String.valueOf(groupPosition), products);// 将组元素的一个唯一值，这里取Id，作为子元素List的Key

            items.add(new YiFenPeiXiangQing1Adapter.OrderListItem(tasksBean.getGrabNo(), "", tasksBean, ""));
        }

        return items;
    }

    @Override
    public void showLoading() {
        CProgressDialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        CProgressDialogUtils.cancelProgressDialog(this);
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.makeText(this, message);

    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @Override
    public void checkGroup(int groupPosition, boolean isChecked) {


        YiFenPeiXiangQing.DataBean.TasksBean group = groups.get(groupPosition);
        List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean> childs = children.get(group.getId());
        for (int i = 0; i < childs.size(); i++) {
            childs.get(i).setChoosed(isChecked);
        }
        if (isAllCheck()) {
//            allChekbox.setChecked(true);
        } else {
//            allChekbox.setChecked(false);
        }

        mAdapter.notifyDataSetChanged();
    }

    private boolean isAllCheck() {

        for (YiFenPeiXiangQing.DataBean.TasksBean group : groups) {
            if (!group.isChoosed())
                return false;
        }
        return true;
    }

    @Override
    public void checkChild(int groupPosition, int childPosition, boolean isChecked) {

        boolean allChildSameState = true;// 判断改组下面的所有子元素是否是同一种状态
        YiFenPeiXiangQing.DataBean.TasksBean group = groups.get(mGroupsPosition.get(groupPosition));

        List<YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean> childs = children.get(group.getId());
        for (int i = 0; i < childs.size(); i++) {
            // 不全选中
            if (childs.get(i).isChoosed() != isChecked) {
                allChildSameState = false;
                break;
            }
        }
        //获取店铺选中商品的总金额
        if (allChildSameState) {
            group.setChoosed(isChecked);// 如果所有子元素状态相同，那么对应的组元素被设为这种统一状态
        } else {
            group.setChoosed(false);// 否则，组元素一律设置为未选中状态
        }

//        group.setChoosed(true);

        if (isAllCheck() && allChildSameState) {
//            allChekbox.setChecked(true);// 全选
        } else {
//            allChekbox.setChecked(false);// 反选
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void heDui(String taskNo) {
        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("taskNo", taskNo);

            map.put("addrLng", String.valueOf(mSharedPreferencesHelper.getString("longitude")));
            map.put("addrLat", String.valueOf(mSharedPreferencesHelper.getString("latitude")));

            mPresenter.loadedVerify(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saoMiaoErWeiMa(YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean itemsBean) {

        mScanTaskNo = itemsBean.getTaskNo();
        mMaterialName = itemsBean.getMaterialName();
        toScanQRCodeActivity();
    }

    @Override
    public void paiZhao(YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean itemsBean) {
        paiZhaoShangChuan(itemsBean);
    }

    private void paiZhaoShangChuan(YiFenPeiXiangQing.DataBean.TasksBean.ItemsBean itemsBean) {
        LinearLayout root = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.sales_client_bottom_dialog_pai_zhao, null);
        Button btn_xiang_ce = root.findViewById(me.iwf.photopicker.R.id.btn_xiang_ce);
        Button ben_xiang_ji = root.findViewById(me.iwf.photopicker.R.id.ben_xiang_ji);
        Button btn_qu_xiao = root.findViewById(me.iwf.photopicker.R.id.btn_qu_xiao);
        btn_qu_xiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();
            }
        });
        btn_xiang_ce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.begin()
                        .setNeedCropWhenOne(true)
                        .setNeedCompress(true)
                        .setMaxSelectCount(1)
                        .setCropMuskOval()
//                        .setSelectGif()
                        /*.setFromCamera(false)
                        .setMaxSelectCount(5)
                        .setNeedCropWhenOne(false)
                        .setNeedCompress(true)
                        .setCropRatio(16,9)*/
                        .start(YiFenPeiXiangQingActivity.this, 33, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                                LogUtils.debugEInfo("onFail");
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));
                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);
                                    String jsonData = "{\"TaskNo\":\"" + itemsBean.getTaskNo() + "\",\"TaskItemId\":" + itemsBean.getItemId() + ",\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.loadMaterialPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
//                                refresh1(originalPaths);
                                LogUtils.debugEInfo("onSuccessMulti");

                            }

                            @Override
                            public void onCancel(int requestCode) {
                                LogUtils.debugEInfo("onCancel");

                            }
                        }));
            }
        });
        ben_xiang_ji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.cropAvatar(true)
                        .start(YiFenPeiXiangQingActivity.this, 23, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                                LogUtils.debugEInfo("onCancel");

                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));

                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);
                                    String jsonData = "{\"TaskNo\":\"" + itemsBean.getTaskNo() + "\",\"TaskItemId\":" + itemsBean.getItemId() + ",\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.loadMaterialPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
                                LogUtils.debugEInfo("onSuccessMulti");

                            }

                            @Override
                            public void onCancel(int requestCode) {
                                LogUtils.debugEInfo("onCancel");

                            }
                        }));
            }
        });

        mBottomDialogUtilShare = new BottomDialogUtil(YiFenPeiXiangQingActivity.this, root);

        mBottomDialogUtilShare.showDiaolog();


    }

    @Override
    public void zhuangChePaiZhao(YiFenPeiXiangQing.DataBean.TasksBean tasksBean) {
        zhuangCheTuPian(tasksBean);
    }

    private void zhuangCheTuPian(YiFenPeiXiangQing.DataBean.TasksBean tasksBean) {
        LinearLayout root = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.sales_client_bottom_dialog_pai_zhao, null);
        Button btn_xiang_ce = root.findViewById(me.iwf.photopicker.R.id.btn_xiang_ce);
        Button ben_xiang_ji = root.findViewById(me.iwf.photopicker.R.id.ben_xiang_ji);
        Button btn_qu_xiao = root.findViewById(me.iwf.photopicker.R.id.btn_qu_xiao);
        btn_qu_xiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();
            }
        });
        btn_xiang_ce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.begin()
                        .setNeedCropWhenOne(true)
                        .setNeedCompress(true)
                        .setMaxSelectCount(1)
                        .setCropMuskOval()
                        .setSelectGif()
                        /*.setFromCamera(false)
                        .setMaxSelectCount(5)
                        .setNeedCropWhenOne(false)
                        .setNeedCompress(true)
                        .setCropRatio(16,9)*/
                        .start(YiFenPeiXiangQingActivity.this, 33, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));
                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);
                                    mTaskNo = tasksBean.getTaskNo();

                                    String jsonData = "{\"TaskNo\":\"" + tasksBean.getTaskNo() + "\",\"TaskItemId\":0,\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.loadPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
//                                refresh1(originalPaths);
                            }

                            @Override
                            public void onCancel(int requestCode) {
                            }
                        }));
            }
        });
        ben_xiang_ji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomDialogUtilShare != null)
                    mBottomDialogUtilShare.dismissDiaolog();

                PhotoUtil.cropAvatar(true)
                        .start(YiFenPeiXiangQingActivity.this, 23, ProxyTools.getShowMethodInfoProxy(new PhotoCallback() {
                            @Override
                            public void onFail(String msg, Throwable r, int requestCode) {
                            }

                            @Override
                            public void onSuccessSingle(String originalPath, String compressedPath, int requestCode) {
                                try {
//                                    transportScanArrive(mid, ArmsUtils.fileToBase64(compressedPath));

                                    String imgStr = ArmsUtils.fileToBase64(compressedPath);
                                    mTaskNo = tasksBean.getTaskNo();

                                    String jsonData = "{\"TaskNo\":\"" + tasksBean.getTaskNo() + "\",\"TaskItemId\":0,\"PotoExtension\":\"jpg\",\"PotoBase64Code\":\"" + imgStr + "\"}";
                                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonData);
                                    mPresenter.loadPhotoVerify(body);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccessMulti(List<String> originalPaths, List<String> compressedPaths, int requestCode) {
                            }

                            @Override
                            public void onCancel(int requestCode) {
                            }
                        }));
            }
        });

        mBottomDialogUtilShare = new BottomDialogUtil(YiFenPeiXiangQingActivity.this, root);

        mBottomDialogUtilShare.showDiaolog();


    }

    private void toScanQRCodeActivity() {
        Intent newIntent = new Intent(this, ScanActivity.class);
        mSharedPreferencesHelper.putInt(SCAN_REQUEST_CODE, 101);
        // 开始一个新的 Activity等候返回结果
        startActivityForResult(newIntent, 101);
    }

    @OnClick(R.id.btn_kai_shi_pei_song)
    public void kaiShiPeiSong() {
//        EventBus.getDefault().post("", "startYunShu");
        if (ArmsUtils.isNetworkAvailable(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("grabNo", mGrabNo);
            mPresenter.startDistibute(map);
        } else {
            showMessage("当前网络不可用");
        }
    }

    @OnClick(R.id.btn_jie_shu_pei_song)
    public void jieShuPeiSong() {
        EventBus.getDefault().post("", "endYunShu");
    }


}
