package com.xiaoxing.yunshu.mvp.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.xiaoxing.yunshu.R;
import com.xiaoxing.yunshu.mvp.ui.entity.CheLiangXinXi;
import com.xiaoxing.yunshu.mvp.ui.inter.ICheLiangXinXi;

import java.util.List;


public class CheLiangXinXiAdapter extends BaseQuickAdapter<CheLiangXinXi.DataBean, BaseViewHolder> {

    private Context mContext;
    private ICheLiangXinXi mICheLiangXinXi;

    private int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;

    public CheLiangXinXiAdapter(Context context, @Nullable List<CheLiangXinXi.DataBean> data, ICheLiangXinXi iCheLiangXinXi) {
        super(R.layout.item_che_liang_xin_xi, data);
        this.mContext = context;
        this.mICheLiangXinXi = iCheLiangXinXi;
    }

    @Override
    protected void convert(BaseViewHolder helper, CheLiangXinXi.DataBean item) {
        //GlideUtil.loadImage(mContext,item.getImg(),helper.getView(R.id.img_head));

        helper.setText(R.id.tv_che_pai_hao, "车牌号：" + item.getTruckNumber());
        helper.setText(R.id.tv_che_liang_lei_xing, "车辆类型：" + item.getTruckTypeDesc());
        helper.setText(R.id.tv_zai_zhong, "载重：" + item.getLoadWeight());
        helper.setText(R.id.tv_chang_kuan_gao, "长：" + item.getLoadLength() + "  宽：" + item.getLoadWidth() + "  高：" + item.getLoadHeight());
        helper.setText(R.id.tv_che_liang_suo_shu, "车辆所属：" + item.getTruckBelongToDesc());
        helper.setText(R.id.tv_suo_shu_qi_ye, "所属企业：" + item.getBelongToOrg());
        TextView tv_mo_ren = helper.getView(R.id.tv_mo_ren);

        if (item.isIsDefault()) {
            tv_mo_ren.setVisibility(View.VISIBLE);
        } else {
            tv_mo_ren.setVisibility(View.GONE);
        }

        helper.setOnClickListener(R.id.tv_edit, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mICheLiangXinXi.cheLiangXinXiEdit(item);

            }
        });
        helper.setOnClickListener(R.id.tv_delete, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showMessageNegativeDialog(item);
            }
        });
    }

    private void showMessageNegativeDialog(CheLiangXinXi.DataBean item) {
        new QMUIDialog.MessageDialogBuilder(mContext)
                .setTitle("刪除车辆信息")
                .setMessage("確定要刪除吗？")
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction(0, "确定", QMUIDialogAction.ACTION_PROP_NEGATIVE, new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        mICheLiangXinXi.cheLiangXinXiDel(item);
                        dialog.dismiss();
                    }
                })
                .create(mCurrentDialogStyle).show();
    }
}
