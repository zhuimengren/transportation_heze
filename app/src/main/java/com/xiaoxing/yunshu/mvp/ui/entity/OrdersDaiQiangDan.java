package com.xiaoxing.yunshu.mvp.ui.entity;

import java.util.List;

public class OrdersDaiQiangDan {


    /**
     * Code : 200
     * Message : 获取最新抢单记录成功！
     * Data : [{"GrabNo":"PG202002050002","RefPrice":2000,"Remark":"dsfdsfdsfdsf\nsfdsfdsfdsfdsf","GrabStatus":2,"DriverCount":0,"PublishDateTime":"2020-02-05 12:04:07"},{"GrabNo":"PG202002050001","RefPrice":1500,"Remark":"sddsdsfdsfd\nsfdsfdsfdsfdsf","GrabStatus":2,"DriverCount":0,"PublishDateTime":"2020-02-05 10:08:45"}]
     */

    private int Code;
    private String Message;
    private List<DataBean> Data;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * GrabNo : PG202002050002
         * RefPrice : 2000.0
         * Remark : dsfdsfdsfdsf
         sfdsfdsfdsfdsf
         * GrabStatus : 2
         * DriverCount : 0
         * PublishDateTime : 2020-02-05 12:04:07
         */

        private String GrabNo;
        private double RefPrice;
        private String Remark;
        private int GrabStatus;
        private int DriverCount;
        private String PublishDateTime;

        public String getGrabNo() {
            return GrabNo;
        }

        public void setGrabNo(String GrabNo) {
            this.GrabNo = GrabNo;
        }

        public double getRefPrice() {
            return RefPrice;
        }

        public void setRefPrice(double RefPrice) {
            this.RefPrice = RefPrice;
        }

        public String getRemark() {
            return Remark;
        }

        public void setRemark(String Remark) {
            this.Remark = Remark;
        }

        public int getGrabStatus() {
            return GrabStatus;
        }

        public void setGrabStatus(int GrabStatus) {
            this.GrabStatus = GrabStatus;
        }

        public int getDriverCount() {
            return DriverCount;
        }

        public void setDriverCount(int DriverCount) {
            this.DriverCount = DriverCount;
        }

        public String getPublishDateTime() {
            return PublishDateTime;
        }

        public void setPublishDateTime(String PublishDateTime) {
            this.PublishDateTime = PublishDateTime;
        }
    }
}
