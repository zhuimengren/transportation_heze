package com.dds.soap;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2019/9/10 0010 8:42
 */
public class EncryptUtil {


    /**
     * @param apiUserName
     * @param apiPwd
     * @param key1        时间戳
     * @param key2        apiUserName+apiPwd+key1 加密之后的数据
     */
    public static void checkKey(String apiUserName, String apiPwd, String key1, String key2) {


    }

    public static String getKey2(String apiUserName, String apiPwd, long key1) {
        //获取当前时间戳
        String strEncry = "";
        if (key1 % 2 == 0) {
            strEncry = apiUserName + apiPwd + key1;
        } else {
            strEncry = apiPwd + apiUserName + key1;
        }

        return md5Decode(strEncry);
    }

    /**
     * 32位MD5加密
     *
     * @param content -- 待加密内容
     * @return
     */
    public static String md5Decode(String content) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(content.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("NoSuchAlgorithmException", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UnsupportedEncodingException", e);
        }
        //对生成的16字节数组进行补零操作
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString().toUpperCase();
    }

    public static void main(String[] args) {

        System.out.println(md5Decode("123456"));
    }
}
