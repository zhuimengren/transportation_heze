package com.jess.arms.utils;

import android.content.Context;

/**
 * @author 小星 QQ:753940262
 * @class describe
 * @time 2020/2/22 0022 18:42
 */
public class PxUtil {
    public static float dpToPx(Context context, int dp) {
        //获取屏蔽的像素密度系数
        float density = context.getResources().getDisplayMetrics().density;
        return dp * density;
    }

    public static float pxTodp(Context context, int px) {
        //获取屏蔽的像素密度系数
        float density = context.getResources().getDisplayMetrics().density;
        return px / density;
    }
}