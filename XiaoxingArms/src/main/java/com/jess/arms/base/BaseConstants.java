/*
 * Copyright 2018 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jess.arms.base;

/**
 * ================================================
 * CommonSDK 的 Constants 可以定义公用的常量, 比如关于业务的常量或者正则表达式, 每个组件的 Constants 可以定义组件自己的私有常量
 * <p>
 * Created by JessYan on 30/03/2018 17:32
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public class BaseConstants {
    //电话号码正则
    public static final String TOKEN = "token";
    public static final String IS_LOGIN = "is_login"; //是否已经登录
    public static final String UID = "uid";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static final String USERNAME_AUTHORIZATION = "username_authorization";
    public static final String PASSWORD_AUTHORIZATION = "password_authorization";

    public static final String IS_STAFF = "is_staff";
    public static final String SHOP_ID = "shop_id";
    public static final boolean AUTO_LOGIN = true;
    public static final String USER_TYPE = "user_type";
    public static final String JI_ZHU_MI_MA = "ji_zhu_mi_ma";
    public static final String SHE_ZHI_IP = "she_zhi_ip";
    public static final String SHE_ZHI_PORT = "she_zhi_port";
    public static final String SCAN_RESULT_SUB = "scan_result_sub";
    public static final String SCAN_REQUEST_CODE = "SCAN_REQUEST_CODE";


    public static final String NAME_SPACE = "http://dyjapp.org/";
    public static final String SHE_ZHI_IP_VALUE = "yunshutestapi.xywuhe.com";
    public static final String SHE_ZHI_PORT_VALUE = "80";
//    public static final String BASE_URL = "http://" + SHE_ZHI_IP_VALUE + ":" + SHE_ZHI_PORT_VALUE + "/StoreService.asmx";
    public static final String IS_CHU_KU = "is_chu_ku"; //是否出库
    public static final String IS_RU_KU = "is_ru_ku"; //是否入库
    public static final int REQUEST_CODE_ER_WEI_MA = 101;
    public static final int SERVICE_ID = 204743;
    public static final String COMPANY = "company";
    public static final String DISPLAY_NAME = "display_name";
    public static final String MOBILE_PHONE = "mobile_phone";
    public static final String YUN_SHU_JIE_SHU = "yun_shu_jie_shu";
    public static final String API_USER_NAME = "api_user_name";
    public static final String API_PWD = "api_pwd";
    public static final String NAME = "name";
    public static final String CARD_NO = "card_no";
    public static final String BIRTH = "birth";
    public static final String SEX = "sex";
    public static final String DRIVING_AGE = "driving_age";
    public static final String YUN_XING_QUAN_XIAN = "yun_xing_quan_xian";
}
