
package me.jessyan.armscomponent.commonservice.gold.service;

import com.alibaba.android.arouter.facade.template.IProvider;

import me.jessyan.armscomponent.commonservice.gold.bean.GoldInfo;


public interface GoldInfoService extends IProvider {
    GoldInfo getInfo();
}
