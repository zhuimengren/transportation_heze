
package me.jessyan.armscomponent.commonservice.zhihu.service;

import com.alibaba.android.arouter.facade.template.IProvider;

import me.jessyan.armscomponent.commonservice.zhihu.bean.ZhihuInfo;


public interface ZhihuInfoService extends IProvider {
    ZhihuInfo getInfo();
}
